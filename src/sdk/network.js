/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const netelm = require('./link');
const Complex = require('complex.js');
const mathjs = require('mathjs');
const utils = require('./utils.js');

// Config DEBUG
const VERBOSE = false;

/**
 * Network object
 */

const Network = function (name) {
    const network = {
        name: '',
        links: {
            posSeq: [],
            zeroSeq: [],
            posSeqForYbus: [],
        },
        nodes: [],
        matrix: {
            zBusPosSeq: [],
            zBusZeroSeq: [],
            zBusForYBus: [],
            yBus: [],
            connectivity: mathjs.matrix(),
        },
        messages: [],
        options: {
            calcIk1: false,
            calcIk2: false,
            calcIk3: true,
            calcIk2EE: false,
            calcLoadflow: false,
            cMax: 1.05,
            cMin: 0.95,
            maxLoadflowIterations: 250,
            gsAccelerationFactor: 1.5,
            respectQLimits: true,
            convergenceLimit: 1e-5,
        },

        sBase: 100,
    };

    network.name = name;
    network.addLinkImpedance = addLinkImpedance();
    network.addNode = addNode();
    network.clear = clear();
    network.sortByNode = sortByNode();

    network.addMessage = function (source, message, value, type = 'message') {
        this.messages.push({
            source,
            message,
            value,
            type,
        });
        return this;
    };

    network.getErrorMessages = function () {
        const msgArray = [];
        this.messages.forEach((msg) => {
            if (msg.type === 'error') {
                msgArray.push(msg.message);
            }
        });
        return msgArray;
    };

    return network;
};

/**
 * addLinkImpedance
 * @param {object} link - Impedance of link element to add to array
 */
const addLinkImpedance = () => {
    return function (link) {
        const empty = { ...netelm.complex };
        let linkZ0 = { ...link };
        const linkY = { ...link };

        // If z0 is defined, we need to modify link_z0 before pushing to z0 array
        if (linkZ0.z0.re !== empty.re && linkZ0.z0.im !== empty.im) {
            utils.debugLog(VERBOSE, "Link has defined z0, setting zm for zero sequence to 'infinite'. z0=", link.z0);
            linkZ0.zm = { ...empty };
            linkZ0.from = linkZ0.fromZeroSeq;
            linkZ0.to = linkZ0.toZeroSeq;
        }

        link = sortLinkFromTo(link);
        linkZ0 = sortLinkFromTo(linkZ0);

        // Add links to ref nodes first in array
        if (link.from === 0) {
            this.links.posSeq.unshift(link);
        } else {
            this.links.posSeq.push(link);
        }

        // Add links_z0 to ref nodes first in array
        if (linkZ0.fromZeroSeq === 0) {
            this.links.zeroSeq.unshift(linkZ0);
        } else {
            this.links.zeroSeq.push(linkZ0);
        }

        // Push version of zm onto posSeqForYbus, with empty impedances to ref node.
        if (link.from === 0) {
            linkY.zm = { ...empty };
            this.links.posSeqForYbus.unshift(linkY);
        } else {
            this.links.posSeqForYbus.push(link);
        }

        // Update connectivity matrix and check parallel connections
        if (link.from > 0 && link.to > 0) {
            // get existing noOfConnections from matrix
            const noOfConnections = 1 + this.matrix.connectivity.subset(mathjs.index(link.from - 1, link.to - 1));

            // update matrix with new noOfConnections
            this.matrix.connectivity.subset(mathjs.index(link.from - 1, link.to - 1), noOfConnections);
            this.matrix.connectivity.subset(mathjs.index(link.to - 1, link.from - 1), noOfConnections);

            // Check if impedances are equal for parallel lines - throws error if not
            if (noOfConnections > 1) {
                this.links.posSeqForYbus.map((linkB) => {
                    checkParallelImpedance(link, linkB);
                });
            }
        }

        return this;
    };
};

/**
 * addNode
 * @param {object} node - node to add
 */
const addNode = () => {
    return function (node) {
        utils.debugLog(VERBOSE, '[Network] Adding node', node.name);

        this.nodes.push(node);
        this.nodes = this.nodes.sort(this.sortByNode);
        const size = this.nodes.length;
        this.matrix.connectivity.resize([size, size]);
        return this;
    };
};

/**
 * Sort link so from > to and returns new link object
 *
 * @param {object} - Link
 * @returns {object} - Link
 */
const sortLinkFromTo = (link) => {
    const returnLink = { ...link };

    if (link.from > link.to) {
        returnLink.from = link.to;
        returnLink.to = link.from;
    }

    return returnLink;
};

/**
 * Checks if linkA and linkB are parallel lines with equal impedance
 *
 * @param {object} - linkA
 * @param {object} - linkB
 * @returns {boolean} - Returns true is check is ok
 */
const checkParallelImpedance = (linkA, linkB) => {
    if (Math.min(linkA.from, linkA.to) !== Math.min(linkB.from, linkB.to)) {
        // Lines are not parallel
        return false;
    }

    if (Math.max(linkA.from, linkA.to) !== Math.max(linkB.from, linkB.to)) {
        // Lines are not parallel
        return false;
    }

    if (!mathjs.equal(utils.anyToComplex(linkA.zm), utils.anyToComplex(linkB.zm))) {
        // throw error
        throw new Error('All parallel links must have same impedance');
    }

    return true;
};

/**
 * Clear all arrays in network, and returns new network
 */
const clear = () => {
    return function () {
        this.links = {
            posSeq: [],
            zeroSeq: [],
            posSeqForYbus: [],
        };

        this.nodes = [];

        this.matrix = {
            zBusPosSeq: [],
            zBusZeroSeq: [],
            zBusForYBus: [],
            yBus: [],
            connectivity: mathjs.matrix(),
        };

        this.messages = [];

        return this;
    };
};

/**
 * Helper function for Array-sorting, used by addNode()
 */
const sortByNode = () => {
    return function (a, b) {
        // a should come before b in the sorted order
        if (a.node < b.node) {
            return -1;
            // a should come after b in the sorted order
        } else if (a.node > b.node) {
            return 1;
            // a and b are the same
        } else {
            return 0;
        }
    };
};

/**
 * Checks if link is string and remove whitespace, replace , with ., replace j with i.
 * If link is not a string, return argument unmodified
 *
 * @param {link} - Link object
 * @returns {link} - Link object
 */
const cleanupLinkString = (link) => (typeof link === 'string' ? new Complex(link.replace(/ /g, '').replace(/,/g, '.').replace(/j/g, 'i')) : link);

module.exports.Network = Network;
