/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const utils = require('./utils');

// Config
const VERBOSE = false;

/**
 * Electrical-current object for shortcircuits
 */
const shortcircuit = {
    ik3: 0,
    ik2: 0,
    ik1: 0,
    ik2EE: 0,
    ikmin: 0,
    ikmax: 0,
    cMax: 1,
    cMin: 1,
};

const loadflow = {
    type: 2,
    P: 0,
    Q: 0,
    Psch: 0,
    Qsch: 0,
    Pgen: 0,
    Qgen: 0,
    Pload: 0,
    Qload: 0,
    Umismatch: 0,
    Pmismatch: 0,
    Qmismatch: 0,
    U: 1.0,
    Uln: 1 / Math.sqrt(3),
    uFactor: 1,
    QlimitMax: 0,
    QlimitMin: 0,
    singlePhase: false,
};

const typeEnum = {
    slackbus: 1,
    PQ: 2,
    PV: 3,
};

const NodeFactory = function (name) {
    const node = {
        node: null,
        id: '',
        sc: { ...shortcircuit },
        lf: { ...loadflow },
        Un: 1.0,
        sBase: 0,
        vBase: 0,
        iBase: 0,
        zBase: 0,
    };

    node.name = name;
    node.setBase = setBase();
    node.setNode = setNode();
    node.setId = setId();
    node.setUFactor = setUFactor();
    node.setNodeType = setNodeType();
    node.getNodeTypeName = getNodeTypeName();
    node.addLoadPower = addLoadPower();
    node.addGeneratorPower = addGeneratorPower();
    node.updateScheduled = updateScheduled();
    node.setSinglePhase = setSinglePhase();
    node.setCFactor = setCFactor();

    return node;
};

const setBase = (sBase = 1, vBase = 1) => {
    return function (sb, vb) {
        this.sBase = sb;
        this.vBase = vb;
        this.iBase = sb / (Math.sqrt(3) * vb);
        this.zBase = vb ** 2 / sb;
        return this;
    };
};

const setNode = (node) => {
    return function (n) {
        this.node = n;
        return this;
    };
};

const setSinglePhase = () => {
    return function (v) {
        this.lf.singlePhase = v === true;
        return this;
    };
};

const setId = (id) => {
    return function (n) {
        this.id = n;
        return this;
    };
};

const setUFactor = (num) => {
    return function (n) {
        this.lf.uFactor = n;

        // Maybe uFactor is set after setNodeType, so we must correct this.U
        if (this.lf.type === typeEnum.slackbus) {
            this.lf.U = this.Un * this.lf.uFactor;
        } else if (this.lf.type === typeEnum.PV) {
            this.lf.U = this.Un * this.lf.uFactor;
        }

        return this;
    };
};

const setCFactor = () => {
    return function (cmax, cmin) {
        this.sc.cMax = cmax;
        this.sc.cMin = cmin;
        return this;
    };
};

const setNodeType = () => {
    return function (num) {
        this.lf.type = num;
        if (num === typeEnum.slackbus) {
            this.lf.U = this.Un * this.lf.uFactor;
        } else if (num === typeEnum.PV) {
            this.lf.U = this.Un * this.lf.uFactor;
        } else if (num === typeEnum.PQ) {
            this.lf.U = this.Un;
        } else {
            throw new Error(`setNodeType(${num}): Unknown nodetype ${num}, cannot set type`);
        }

        return this;
    };
};

const getNodeTypeName = () => {
    return function () {
        let str;

        if (this.lf.type === typeEnum.slackbus) {
            str = 'slackbus';
        } else if (this.lf.type === typeEnum.PQ) {
            str = 'PQ';
        } else if (this.lf.type === typeEnum.PV) {
            str = 'PV';
        } else {
            /* istanbul ignore next */
            throw new Error(`Nodetype ${this.lf.type} not known, cannot return nodetype name`);
        }

        return str;
    };
};

const addLoadPower = () => {
    return function (load) {
        const P = this.lf.singlePhase ? load.P * 3 : load.P;
        const Q = this.lf.singlePhase ? load.Q * 3 : load.Q;

        if (P >= 0) {
            utils.debugLog(VERBOSE, '[node]', this.name);
            utils.debugLog(VERBOSE, '... adding load', load.name);
            utils.debugLog(VERBOSE, '... P', P);
            utils.debugLog(VERBOSE, '... Q', Q);

            this.lf.Pload += P;
            this.lf.Qload += Q;
            this.updateScheduled();
        } else {
            throw new Error(`addLoadPower, value of load.P should be >= 0. Argument=${JSON.stringify(load)}`);
        }

        return this;
    };
};

const addGeneratorPower = () => {
    return function (generator) {
        if (generator.P >= 0) {
            utils.debugLog(VERBOSE, '[node]', this.name);
            utils.debugLog(VERBOSE, '... adding generator', generator.name);
            utils.debugLog(VERBOSE, '... P', generator.P);
            utils.debugLog(VERBOSE, '... Sn', generator.Sn);
            utils.debugLog(VERBOSE, '... pf', generator.pf);
            utils.debugLog(VERBOSE, '... Qmax', generator.QlimitMax);
            utils.debugLog(VERBOSE, '... Qmin', generator.QlimitMin);

            // this.uFactor = uFactor;
            if (this.lf.type === typeEnum.PQ) {
                this.setNodeType(typeEnum.PV);

                utils.debugLog(VERBOSE, '[node]', this.name);
                utils.debugLog(VERBOSE, '... changed type to', this.lf.type);
            }

            this.lf.Pgen = generator.P;
            this.lf.QlimitMax = generator.QlimitMax;
            this.lf.QlimitMin = generator.QlimitMin;
            this.updateScheduled();
        } else {
            throw new Error(`addGeneratorPower, value of generator.P must be >= 0. Argument=${JSON.stringify(generator)}`);
        }
        return this;
    };
};

const updateScheduled = () => {
    return function () {
        this.lf.Psch = this.lf.Pgen - this.lf.Pload;
        this.lf.Qsch = this.lf.Qgen - this.lf.Qload;

        return this;
    };
};

module.exports.NodeFactory = NodeFactory;
module.exports.typeEnum = typeEnum;
