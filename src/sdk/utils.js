/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Utilities and helper functions

'use strict';

const mathjs = require('mathjs');

// Config DEBUG
const DEBUG = false;
const VERBOSE = false;

/**
 * Debug log
 *
 * @param {boolean} enabled - Print the log message. Use this to disable logmessages from debug, verbose etc.
 * @param {string} msg - Message
 * @param {string} value - Any other value to show, after message eg. numbers, objects etc.
 */
/* istanbul ignore next */
const debugLog = (enabled, msg, value) => enabled && console.log(`${msg}=${value}`);

/**
 * Returns a two-dimensional Array of javascript objects
 *
 * @param {matrix} m - Two-dimensional Math.js matrix of complex numbers
 */
function complexMatrixToArray(m) {
    /* istanbul ignore next */
    debugLog(DEBUG, '[complexMatrixToArray] Receiving matrix', m.valueOf());

    // Throw error if argument is not a matrix
    if (mathjs.typeOf(m) !== 'Matrix') {
        throw new Error('complexMatrixToArray not passed a Matrix argument');
    }

    const r = m.clone().valueOf();

    m.forEach(function (value, index, matrix) {
        r[index[0]][index[1]] = {
            re: mathjs.subset(m, mathjs.index(index[0], index[1])).re,
            im: mathjs.subset(m, mathjs.index(index[0], index[1])).im,
        };
    });

    /* istanbul ignore next */
    debugLog(DEBUG, '[complexMatrixToArray] Returning matrix', r);

    return r;
}

/**
 * Output calculation-results to console
 *
 * @param {*} network
 */
/* istanbul ignore next */
const resultsToConsole = (network, decimals = 3) => {
    console.log('API returned the follow messages:');
    console.log('---------------------------------');

    network.messages.forEach((msg) => {
        const value = msg.value && ` = ${JSON.stringify(msg.value)}`;
        msg.type === 'message' && console.log(`[${msg.source}] ${msg.message}${value || ''}`);
    });

    const results = getResultsActualUnits(network);

    console.log('');
    // Output to console
    console.log(`Node\tVoltage\tAngle\tPgen\tQgen\tPload\tQload\tType\tIk''3\tIk''2\tIk''1\tIk max\tIk min\tPhases`);
    console.log(`-----\t-------\t-----\t------\t------\t------\t------\t-----\t-----\t-----\t-----\t-----\t-------`);

    network.nodes.map((node, index) => {
        const name = node.name;
        const voltage = node.lf.singlePhase ? mathjs.round(results.nodes[index].lf.Uln, decimals) : mathjs.round(results.nodes[index].lf.U, decimals);
        const angle = mathjs.round(results.nodes[index].lf.angle, decimals);
        const pgen = mathjs.round(results.nodes[index].lf.Pgen, decimals);
        const qgen = mathjs.round(results.nodes[index].lf.Qgen, decimals);
        const pload = mathjs.round(results.nodes[index].lf.Pload, decimals);
        const qload = mathjs.round(results.nodes[index].lf.Qload, decimals);
        const type = results.nodes[index].type === 3 ? 'PV' : results.nodes[index].type === 1 ? 'Slack' : 'PQ';
        const ik3 = mathjs.round(results.nodes[index].sc.ik3, decimals);
        const ik2 = mathjs.round(results.nodes[index].sc.ik2, decimals);
        const ik1 = mathjs.round(results.nodes[index].sc.ik1, decimals);
        const ikmin = mathjs.round(results.nodes[index].sc.ikmin, decimals);
        const ikmax = mathjs.round(results.nodes[index].sc.ikmax, decimals);

        const phases = results.nodes[index].lf.singlePhase ? 'LN' : 'LLL';
        console.log(
            `${name}\t${voltage}\t${angle}\t${pgen}\t${qgen}\t${pload}\t${qload}\t${type}\t${ik3}\t${ik2}\t${ik1}\t${ikmax}\t${ikmin}\t${phases}`
        );
    });

    console.log(``);
    console.log(`Line\t\t\tPline\tQline\tPloss\tQloss\tIb\t\tdU`);
    console.log(`-----\t\t\t------\t------\t------\t------\t--------\t----`);

    network.links.posSeqForYbus.map((link, index) => {
        const name = link.name;
        const pline = mathjs.round(results.links[index].P, decimals);
        const qline = mathjs.round(results.links[index].Q, 3);
        const ploss = mathjs.round(results.links[index].Ploss, decimals);
        const qloss = mathjs.round(results.links[index].Qloss, 3);
        const ib = results.links[index].Ib.toFixed(2);
        const du = results.links[index].dU.toFixed(1);

        console.log(`${name}\t\t\t${pline}\t${qline}\t${ploss}\t${qloss}\t${ib} A\t\t${du} %`);
    });
};

/**
 * Converts an impedance to per-unit, given voltage-base and power-base
 * @param {*} vBase - Voltage base
 * @param {*} sBase - Power base
 * @param {*} str - String containing actual resistance and reactance as complex number 'r+xi'
 * @returns {*} per-unit complex impedance, returned as string
 */
function convertToPerUnit(vBase, sBase, str) {
    try {
        const z = anyToComplex(str);
        const zBase = vBase ** 2 / sBase;

        /* istanbul ignore next */
        debugLog(VERBOSE, '[convertToPerUnit] converting', vBase);
        debugLog(VERBOSE, '... str', str);
        debugLog(VERBOSE, '... vBase', vBase);
        debugLog(VERBOSE, '... sBase', sBase);
        debugLog(VERBOSE, '... zBase', zBase);
        debugLog(VERBOSE, '... to', z.div(zBase).toString());

        return z.div(zBase).toString();
    } catch (error) {
        throw new Error(`convertToPerUnit cannot convert ${str} to a complex number`);
    }
}

/**
 * Returns network, where nodes.U is converted to mathjs.Complex objects
 *
 * @param {object} network - Network object
 * @returns {object} net - Updated network object
 */
function convertNodeUtoComplex(network) {
    const net = { ...network };

    net.nodes.map((elm) => {
        if (typeof elm.lf.U === 'number') {
            elm.lf.U = mathjs.complex(elm.lf.U, 0);
        } else if (typeof elm.lf.U.re === 'number') {
            elm.lf.U = mathjs.complex(elm.lf.U.re, elm.lf.U.im);
        } else {
            throw new Error(`Cannot accept node L-L-L voltage ${JSON.stringify(elm.lf.U)} with typeof ${typeof elm.lf.U} `);
        }
    });

    net.nodes.map((elm) => {
        if (typeof elm.lf.Uln === 'number') {
            elm.lf.Uln = mathjs.complex(elm.lf.Uln, 0);
        } else if (typeof elm.lf.Uln.re === 'number') {
            elm.lf.Uln = mathjs.complex(elm.lf.Uln.re, elm.lf.Uln.im);
        } else {
            throw new Error(`Cannot accept node L-N voltage ${JSON.stringify(elm.lf.Uln)} with typeof ${typeof elm.lf.Uln} `);
        }
    });

    return net;
}

/**
 * Returns network, where links zm, z0 and shunt are converted to mathjs.Complex objects
 *
 * @param {object} links - links array
 * @returns {object} - Updated links array
 */
function convertLinksToComplex(links) {
    if (!Array.isArray(links)) throw new Error('convertLinksToComplex not passed an Array argument');

    const array = [].concat(links);

    array.map((link) => {
        link.zm = anyToComplex(link.zm);
        link.z0 = anyToComplex(link.z0);
        link.shunt = anyToComplex(link.shunt);
    });

    return array;
}

/**
 * Returns network, where links zm, z0 are multiplied linksfault (conductor heating factor)
 *
 * @param {object} links - links array
 * @returns {object} - Updated links array
 */
function applyConductorHeating(links) {
    if (!Array.isArray(links)) throw new Error('applyConductorHeating not passed an Array argument');

    const array = [].concat(links);

    array.map((link) => {
        link.zm.re = link.zm.re * link.Rfault;
        link.z0.re = link.z0.re * link.Rfault;
    });

    return array;
}

/**
 * Converts any object to mathjs.complex, or throws error
 *
 * @param {*} any
 * @returns {object} Complex
 */
function anyToComplex(any) {
    let complex;

    if (typeof any === 'string') {
        const str = any.replace(/ /g, '').replace(/j/g, 'i').replace(/,/g, '.');
        complex = mathjs.complex(str);
    } else if (typeof any === 'number') {
        complex = mathjs.complex(any, 0);
    } else if (typeof any.re === 'number') {
        complex = mathjs.complex(any.re, any.im);
    } else {
        throw new Error('anyToComplex cannot convert argument to Complex');
    }

    return complex;
}

/**
 * Returns true if network has error messages
 *
 * @param {object} network
 * @returns {boolean}
 */
const networkHasError = function (network) {
    return network.messages.some((msg) => msg.type === 'error');
};

/**
 * Returns true if 'a' and 'b' are within 'tolerance' % of each other
 * @param {Number} a
 * @param {Number} b
 * @param {Number} tolerance %
 * @returns {Boolean}
 */
const withinTolerance = (a, b, tolerance) => {
    return a / b <= 1 + tolerance / 100 && a / b >= 1 - tolerance / 100;
};

/**
 * Returns results object converted to actual units
 */
const getResultsActualUnits = (network) => {
    // const obj = {
    //     nodes: [...network.nodes],
    //     links: [...network.links.posSeq],
    // };
    const obj = {
        nodes: JSON.parse(JSON.stringify(network.nodes)),
        links: JSON.parse(JSON.stringify(network.links.posSeq)),
        options: JSON.parse(JSON.stringify(network.options)),
    };

    network.nodes.map((node, index) => {
        obj.nodes[index].Un = node.Un * node.vBase;
        obj.nodes[index].lf.angle = (node.lf.U.arg() * 180) / Math.PI;
        obj.nodes[index].lf.Uln = node.lf.Uln.abs() * node.vBase;
        obj.nodes[index].lf.U = node.lf.U.abs() * node.vBase;
        obj.nodes[index].lf.Pgen = node.lf.Pgen * node.sBase;
        obj.nodes[index].lf.Qgen = node.lf.Qgen * node.sBase;
        obj.nodes[index].lf.Pload = node.lf.Pload * node.sBase;
        obj.nodes[index].lf.Qload = node.lf.Qload * node.sBase;
        obj.nodes[index].sc.ik3 = node.lf.singlePhase ? 0 : node.sc.ik3 * node.iBase;
        obj.nodes[index].sc.ik2 = node.lf.singlePhase ? 0 : node.sc.ik2 * node.iBase;
        obj.nodes[index].sc.ik2EE = node.lf.singlePhase ? 0 : node.sc.ik2EE * node.iBase;
        obj.nodes[index].sc.ik1 = node.sc.ik1 * node.iBase;
        obj.nodes[index].sc.ikmin = node.sc.ikmin * node.iBase;
        obj.nodes[index].sc.ikmax = node.sc.ikmax * node.iBase;
    });

    network.links.posSeq.map((link, index) => {
        obj.links[index].P = link.P * link.sBase;
        obj.links[index].Q = link.Q * link.sBase;
        obj.links[index].Ploss = link.Ploss * link.sBase;
        obj.links[index].Qloss = link.Qloss * link.sBase;
        obj.links[index].Ib = ((link.Ib * link.sBase) / link.vBase / Math.sqrt(3)) * 1000;
        obj.links[index].dU = link.dU;
    });

    return obj;
};

module.exports.complexMatrixToArray = complexMatrixToArray;
module.exports.resultsToConsole = resultsToConsole;
module.exports.convertToPerUnit = convertToPerUnit;
module.exports.debugLog = debugLog;
module.exports.convertNodeUtoComplex = convertNodeUtoComplex;
module.exports.networkHasError = networkHasError;
module.exports.convertLinksToComplex = convertLinksToComplex;
module.exports.applyConductorHeating = applyConductorHeating;
module.exports.anyToComplex = anyToComplex;
module.exports.withinTolerance = withinTolerance;
module.exports.getResultsActualUnits = getResultsActualUnits;
