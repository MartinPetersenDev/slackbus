/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

/**
 * Complex number object.
 *
 * re and im defaults to 1e9. Values higher than this suffers from IEEE 754 floating point rounding errors.
 *
 */
const complex = {
    re: 1e9,
    im: 1e9,
};

/**
 * Complex number object, with values of zero.
 */
const complexZero = {
    re: 0,
    im: 0,
};

/** Common object for netelements. Should not be instantiated on its own */
const NetElement = function (name) {
    const netelement = {
        id: '',
        name: '',
        zm: { ...complex },
        z0: { ...complex },
        shunt: { ...complex },
        U: 0,
        P: 0,
        Q: 0,
    };

    netelement.setBase = setBase();
    netelement.setZm = setImpedance('zm');
    netelement.setZ0 = setImpedance('z0');
    netelement.setShunt = setImpedance('shunt');
    netelement.setId = setId();

    netelement.name = name;
    return netelement;
};

/** Link */
const LinkFactory = function (name) {
    const link = { ...NetElement(name) };

    link.Qlimit = 0;
    link.Ploss = 0;
    link.Qloss = 0;
    link.zm = { ...complex };
    link.z0 = { ...complex };
    link.shunt = { ...complexZero };
    link.Ib = 0;
    link.dU = 0;
    link.singlePhase = false;
    link.Rfault = 1;
    link.Kt = 1;

    link.setFrom = setFrom();
    link.setTo = setTo();
    link.setFromZeroSeq = setFromZeroSeq();
    link.setToZeroSeq = setToZeroSeq();
    link.setSinglePhase = setSinglePhase();
    link.setRfault = setRfault();
    link.setKt = setKt();

    return link;
};

/** Load */
const LoadFactory = function (name) {
    const load = { ...NetElement(name) };

    load.setP = setP();
    load.setQ = setQ();

    return load;
};

/** Generator */
const GeneratorFactory = function (name) {
    const generator = { ...LinkFactory(name) };

    generator.Sn = 1;
    generator.pf = 1.0;
    generator.QlimitMax = 0;
    generator.QlimitMin = 0;

    generator.setSn = setSn();
    generator.setPf = setPf();
    generator.updatePV = updatePV();

    return generator;
};

const setId = (id) => {
    return function (n) {
        this.id = n;
        return this;
    };
};

const setBase = (sBase = 1, vBase = 1) => {
    return function (sb, vb) {
        this.sBase = typeof sb === 'string' ? parseFloat(sb.replace(/,/g, '.')) : sb;
        this.vBase = typeof vb === 'string' ? parseFloat(vb.replace(/,/g, '.')) : vb;
        return this;
    };
};

// No type cast on impedance, because it is done by addLinkImpedance in Network object
const setImpedance = (type) => {
    return function (val) {
        if (type === 'zm') {
            this.zm = val;
        } else if (type === 'z0') {
            this.z0 = val;
        } else if (type === 'shunt') {
            this.shunt = val;
        }
        return this;
    };
};

const setFrom = () => {
    return function (n) {
        this.from = parseInt(n);
        this.setFromZeroSeq(this.from);
        return this;
    };
};

const setTo = () => {
    return function (n) {
        this.to = parseInt(n);
        this.setToZeroSeq(this.to);
        return this;
    };
};

const setFromZeroSeq = () => {
    return function (n) {
        this.fromZeroSeq = parseInt(n);
        return this;
    };
};

const setToZeroSeq = () => {
    return function (n) {
        this.toZeroSeq = parseInt(n);
        return this;
    };
};

const setSinglePhase = () => {
    return function (v) {
        this.singlePhase = v === true;
        return this;
    };
};

const setP = () => {
    return function (v) {
        this.P = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        return this;
    };
};

const setQ = () => {
    return function (v) {
        this.Q = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        return this;
    };
};

const setSn = () => {
    return function (v) {
        const value = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        if (value < 0) {
            throw new Error('setSn cannot be called with a negative number as argument');
        }
        this.Sn = value;
        this.updatePV();
        return this;
    };
};

const setPf = () => {
    return function (v) {
        const value = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        if (value < -1 || value > 1) {
            throw new Error('setPf must be between -1 and 1');
        }
        this.pf = value;
        this.updatePV();
        return this;
    };
};

const updatePV = () => {
    return function () {
        this.QlimitMax = Math.sin(Math.acos(this.pf)) * this.Sn;
        this.QlimitMin = (-1 * this.QlimitMax) / 3;
        this.P = this.Sn * this.pf;
        return this;
    };
};

const setRfault = () => {
    return function (v) {
        this.Rfault = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        return this;
    };
};

const setKt = () => {
    return function (v) {
        this.Kt = typeof v === 'string' ? parseFloat(v.replace(/,/g, '.')) : v;
        return this;
    };
};

module.exports.complex = complex;
module.exports.LinkFactory = LinkFactory;
module.exports.LoadFactory = LoadFactory;
module.exports.GeneratorFactory = GeneratorFactory;
