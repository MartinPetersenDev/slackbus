/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const sdkUtils = require('../sdk/utils');

/**
 * Generates a pdf report of network calculation results.
 * @param {*} graph
 * @param {object} network - Network, assumed to be of type object.
 *
 * @returns {object} pdf
 */

function HTMLReport(g, data) {
    const network = sdkUtils.getResultsActualUnits(data);

    // Set display units
    const voltUnit = network.options.slackbus.displayUnits === 1 ? 'kV' : 'V';
    const wattUnit = network.options.slackbus.displayUnits === 1 ? 'MW' : 'kW';
    const vaUnit = network.options.slackbus.displayUnits === 1 ? 'MVA' : 'kVA';
    const varUnit = network.options.slackbus.displayUnits === 1 ? 'MVAr' : 'kVAr';
    const lengthUnit = network.options.slackbus.displayUnits === 1 ? 'km' : 'm';
    const roundToPower = network.options.slackbus.displayUnits === 1 ? 3 : 1;
    const roundToVoltage = network.options.slackbus.displayUnits === 1 ? 3 : 1;
    const roundToShortCircuit = 2;

    // HTML document
    const doc = document.implementation.createHTMLDocument('Network calculation report');

    const divTop = document.createElement('div');
    const divMiddle = document.createElement('div');
    const divBottom = document.createElement('div');

    // TOP
    divTop.style = 'display: flex;  align-items: center;  font-family: Helvetica,Arial,sans-serif; font-size: 24px;';
    divMiddle.style = 'font-family: Helvetica,Arial,sans-serif; font-size: 14px;';
    divBottom.style = 'font-family: Helvetica,Arial,sans-serif; font-size: 12px;';

    const logo = document.createElement('img');
    logo.src = 'http://localhost:8000/app/slackbus/img/logo.png';
    logo.width = 32;
    logo.height = 32;
    logo.style = 'padding: 0px 20px 0px 0px';

    const title = document.createElement('p');
    title.innerHTML = 'Calculation Report';

    divTop.appendChild(logo);
    divTop.appendChild(title);

    // MIDDLE
    // Nodes
    const tableBus = document.createElement('table');
    tableBus.style = 'padding: 0px 0px 15px 0px;';

    const busHeader = document.createElement('tr');
    busHeader.style = 'padding: 5px';

    const busHeaders = ['Node', 'Voltage', 'Load', "Ik''3", "Ik''2", "Ik''1", 'Ik max', 'Ik min'];

    busHeaders.map((elm) => {
        const th = document.createElement('th');
        th.style = 'padding: 5px 20px 5px 20px; margin: 5px;';
        th.innerHTML = elm;
        busHeader.appendChild(th);
    });

    tableBus.appendChild(busHeader);

    network.nodes.map((node, index) => {
        // Generate node rows
        const row = document.createElement('tr');
        row.style =
            index % 2 === 0
                ? 'padding: 5px 20px 5px 20px; margin: 5px;'
                : 'padding: 5px 20px 5px 20px; margin: 5px; background-color: #dcdcdc; valign: center;';

        const bus = document.createElement('td');
        bus.style = 'text-align: center';
        bus.innerHTML = node.name;
        row.appendChild(bus);

        const Ull = node.lf.U.toFixed(roundToVoltage);
        const Uln = node.lf.Uln.toFixed(roundToVoltage);

        const voltage = document.createElement('td');
        voltage.style = 'text-align: center';
        voltage.innerHTML = `${node.lf.singlePhase ? '---' : Ull} / ${Uln} <i>${voltUnit}</i>`;
        row.appendChild(voltage);

        const P = node.lf.Pload;
        const Q = node.lf.Qload;
        const _P = node.lf.singlePhase ? (P / 3).toFixed(roundToPower) : P.toFixed(roundToPower);
        const _Q = node.lf.singlePhase ? (Q / 3).toFixed(roundToPower) : Q.toFixed(roundToPower);
        const power = document.createElement('td');
        power.style = 'text-align: center';
        power.innerHTML = `${_P} <i>${wattUnit}</i> / ${_Q} <i>${varUnit}</i>`;
        row.appendChild(power);

        const ik3 = document.createElement('td');
        ik3.style = 'text-align: center';
        ik3.innerHTML = node.lf.singlePhase ? '---' : node.sc.ik3.toFixed(roundToShortCircuit) + ` <i>kA</i>`;
        row.appendChild(ik3);

        const ik2 = document.createElement('td');
        ik2.style = 'text-align: center';
        ik2.innerHTML = node.lf.singlePhase ? '---' : node.sc.ik2.toFixed(roundToShortCircuit) + ` <i>kA</i>`;
        row.appendChild(ik2);

        const ik1 = document.createElement('td');
        ik1.style = 'text-align: center';
        ik1.innerHTML = node.sc.ik1.toFixed(roundToShortCircuit) + ` <i>kA</i>`;
        row.appendChild(ik1);

        const ikmax = document.createElement('td');
        ikmax.style = 'text-align: center';
        ikmax.innerHTML = node.lf.singlePhase ? ik1.innerHTML : node.sc.ikmax.toFixed(roundToShortCircuit) + ` <i>kA</i>`;
        row.appendChild(ikmax);

        const ikmin = document.createElement('td');
        ikmin.style = 'text-align: center';
        ikmin.innerHTML = node.sc.ikmin.toFixed(roundToShortCircuit) + ` <i>kA</i>`;
        row.appendChild(ikmin);

        tableBus.appendChild(row);
    });

    // Links
    const defaultParent = g.getDefaultParent(); // Returns cell
    const cells = g.getChildCells(defaultParent);

    const tableLink = document.createElement('table');
    tableLink.style = 'padding: 0px 0px 15px 0px';

    const linkHeader = document.createElement('tr');
    const linkHeaders = ['Line', 'Type', 'Length', 'Phases', 'Ib', 'dU'];
    linkHeaders.map((elm) => {
        const td = document.createElement('th');
        td.style = 'padding: 5px 20px 5px 20px; margin: 5px;';
        td.innerHTML = elm;
        linkHeader.appendChild(td);
    });

    tableLink.appendChild(linkHeader);

    network.links.map((link, index) => {
        // Get additional info from graph
        let length = 0;
        let description = '';
        let isLine;

        cells.forEach((c) => {
            if (c.getId() === link.id) {
                isLine = c.getAttribute('type', '').toLowerCase() === 'line';
                length = isLine ? c.getAttribute('Length', '') + ` <i>${lengthUnit}</i>` : '-';
                description = c.getAttribute('Description', '-');
            }
        });

        if (isLine) {
            // Get single phase flag
            const isSinglePhase = link.singlePhase;

            // Generate link rows
            const row = document.createElement('tr');
            row.style =
                index % 2 === 0 ? 'padding: 5px 20px 5px 20px; margin: 5px;' : 'padding: 5px 20px 5px 20px; margin: 5px; background-color: #dcdcdc;';

            const line = document.createElement('td');
            line.style = 'text-align: center';
            line.innerHTML = link.name;
            row.appendChild(line);

            const type = document.createElement('td');
            type.style = 'text-align: center';
            type.innerHTML = description;
            row.appendChild(type);

            const len = document.createElement('td');
            len.style = 'text-align: center';
            len.innerHTML = length;
            row.appendChild(len);

            const phases = document.createElement('td');
            phases.style = 'text-align: center';
            phases.innerHTML = isSinglePhase ? `1-phase` : `3-phase`;
            row.appendChild(phases);

            const Ib = link.Ib;
            const current = document.createElement('td');
            current.style = 'text-align: center';
            current.innerHTML = `${Ib.toFixed(1)} <i>A</i>`;
            row.appendChild(current);

            const du = document.createElement('td');
            du.style = 'text-align: center';
            du.innerHTML = `${link.dU.toFixed(1)} <i>%</i>`;
            row.appendChild(du);

            tableLink.appendChild(row);
        }
    });

    divMiddle.appendChild(tableBus);
    divMiddle.appendChild(tableLink);

    // BOTTOM
    const footer = document.createElement('p');
    footer.innerHTML =
        '<p>This calculation report was created with the free demo version of SlackBUS, and comes without warranties and liabilities of any kind.</p><p>SlackBUS - Copyright (c) 2020 - http://www.slackbus.net - Build version: @@CI_COMMIT_SHORT_SHA</p>';
    // footer.innerText =
    divBottom.appendChild(footer);

    // Doc
    doc.body.appendChild(divTop);
    doc.body.appendChild(divMiddle);
    doc.body.appendChild(divBottom);

    return doc.body.innerHTML;
}

module.exports.HTMLReport = HTMLReport;

module.exports.submit = async (event, context, callback) => {
    const returnPdf = HTMLReport(event.body);

    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': 'http://slackbus.net',
            'Content-type': 'application/pdf',
        },
        body: JSON.stringify({
            message: 'Go-go SlackBUS.net API! Your report was generated successfully!',
            data: returnPdf,
        }),
    };

    callback(null, response);
};
