/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const mathjs = require('mathjs');
const utils = require('../sdk/utils');

// Config
const DEBUG = false;
const VERBOSE = false;

/**
 * Builds Z-bus matrix from network object supplied as parameter.
 *
 * @param {object} network - Object must contain fields 'from', 'to', 'impedance.[zm|z0].re' and 'impedance.[zm|z0].im' where [zm|z0] is selectable with parameter selectZ.
 * @param {string} selectZ - Selects if zm or z0 is used for calculation, with zm as default.
 *
 * @returns {object} Z-bus
 */
function zBusBuilder(network, selectZ = 'zm') {
    // Helper variables
    const links = network.links;
    const noOfLinks = network.links.length;
    const size = parseInt(network.size);

    const scannedNodes = [];
    const isSelectZ0 = selectZ === 'z0';

    utils.debugLog(DEBUG, '[zBusBuilder]', '');
    utils.debugLog(DEBUG, '... isSelectZ0', isSelectZ0);
    utils.debugLog(DEBUG, '... _size', size);
    utils.debugLog(DEBUG, '... type', typeof size);
    utils.debugLog(DEBUG, '... _noOfLinks', noOfLinks);
    utils.debugLog(DEBUG, '... called with links', JSON.stringify(network));

    // Initialize _size x _size matrix
    let zBus = mathjs.zeros(size, size);

    // Iterate network
    for (let m = 0; m < noOfLinks; m++) {
        const nodes = [links[m].from, links[m].to];
        const type = getType(nodes, scannedNodes);
        const impedance = isSelectZ0 ? links[m].z0 : links[m].zm;

        utils.debugLog(DEBUG, '[zBusBuilder]', '');
        utils.debugLog(DEBUG, '... Adding', selectZ);
        utils.debugLog(DEBUG, '... type', type);
        utils.debugLog(DEBUG, '... from', links[m].from);
        utils.debugLog(DEBUG, '... to', links[m].to);
        utils.debugLog(DEBUG, '... impedance', JSON.stringify(impedance));

        if (type === 1) {
            zBus = addType1(zBus, impedance, scannedNodes);
        } else if (type === 2) {
            zBus = addType2(zBus, impedance, scannedNodes, nodes);
        } else if (type === 3) {
            zBus = addType3(zBus, impedance, nodes);
        } else if (type === 4) {
            zBus = addType4(zBus, impedance, scannedNodes, nodes);
        }

        utils.debugLog(DEBUG, '[zBusBuilder] current bus', JSON.stringify(zBus));
    }

    utils.debugLog(DEBUG, '[zBusBuilder]  returning z-bus', JSON.stringify(zBus));

    return zBus;
}

/**
 * Add type 4 branch
 */
function addType4(zBus, impedance, scannedNodes, nodes) {
    const i = nodes[0] - 1;
    const j = nodes[1] - 1;
    const _zBus = mathjs.clone(zBus);

    utils.debugLog(VERBOSE, '[zBusBuilder] Type 4', '');
    utils.debugLog(VERBOSE, '... i', i);
    utils.debugLog(VERBOSE, '... j', j);
    utils.debugLog(VERBOSE, '...  nodes[0]', nodes[0]);
    utils.debugLog(VERBOSE, '...  nodes[1]', nodes[1]);
    utils.debugLog(VERBOSE, '...  scannedNodes', scannedNodes);

    // MATH version 2
    // Remember matrix multiplication is not commutative
    // Z = Z_old - a*b*b^T
    // a = (impedance + Zii - 2*Zji + Zjj)^-1
    // b = Zi - Zj
    //
    let a = mathjs.add(impedance, _zBus.subset(mathjs.index(i, i)));
    a = mathjs.subtract(a, mathjs.multiply(2, _zBus.subset(mathjs.index(j, i))));
    a = mathjs.add(a, _zBus.subset(mathjs.index(j, j)));
    a = mathjs.pow(a, -1);

    const b = mathjs.subtract(mathjs.column(_zBus, i), mathjs.column(_zBus, j));

    let temp = mathjs.multiply(a, b);
    temp = mathjs.multiply(temp, mathjs.transpose(b));

    utils.debugLog(DEBUG, '[zBusBuilder] Added type 4, z-bus', JSON.stringify(_zBus));

    return mathjs.subtract(_zBus, temp);
}

/**
 * Add type 3 branch
 */
function addType3(zBus, impedance, nodes) {
    const i = nodes[1] - 1;
    const _zBus = mathjs.clone(zBus);

    // MATH version 2
    //
    // Z = Z - Zi * (Zii + impedance)^-1 * Zi^T
    // Z = Z - c
    // a = Zi
    // b = (Zii + impedance)^-1
    // temp = a*b*a^T
    const a = mathjs.column(_zBus, i);
    let b = mathjs.add(_zBus.subset(mathjs.index(i, i)), impedance);
    b = mathjs.pow(b, -1);
    let temp = mathjs.multiply(a, b);
    temp = mathjs.multiply(temp, mathjs.transpose(a));

    return mathjs.subtract(_zBus, temp);
}

/**
 * Add type 2 branch to zbus
 */
function addType2(zBus, impedance, scannedNodes, nodes) {
    const lastNode = scannedNodes[scannedNodes.length - 2];
    const i = nodes[1] - 1;
    const j = nodes[0] - 1;
    const _zBus = mathjs.clone(zBus);

    utils.debugLog(VERBOSE, '[zBusBuilder] Type 2', '');
    utils.debugLog(VERBOSE, '... lastNode', lastNode);
    utils.debugLog(VERBOSE, '... i', i);
    utils.debugLog(VERBOSE, '... j', j);
    utils.debugLog(VERBOSE, '... nodes[0]', nodes[0]);
    utils.debugLog(VERBOSE, '... nodes[1]', nodes[1]);
    utils.debugLog(VERBOSE, '... scannedNodes', scannedNodes);

    for (let g = 0; g < i; g++) {
        _zBus.subset(mathjs.index(i, g), _zBus.subset(mathjs.index(j, g)));
        _zBus.subset(mathjs.index(g, i), _zBus.subset(mathjs.index(g, j)));
    }

    utils.debugLog(DEBUG, '[zBusBuilder] Added type 2, z-bus', JSON.stringify(_zBus));
    return mathjs.subset(_zBus, mathjs.index(i, i), mathjs.add(_zBus.subset(mathjs.index(j, j)), impedance));
}

/**
 * Add type 1 branch to zbus
 */
function addType1(zBus, impedance, _scannedNodes) {
    const _lastNode = _scannedNodes[_scannedNodes.length - 1] - 1;

    utils.debugLog(DEBUG, '[zBusBuilder] Added type 1, z-bus', zBus);
    return mathjs.subset(zBus, mathjs.index(_lastNode, _lastNode), impedance);
}

/**
 * Get type of branch
 */
function getType(link, scannedNodes) {
    // Cast all links as integers
    const _link = link.map((node) => (node = parseInt(node)));

    let type;

    if (_link.includes(0)) {
        // Includes reference node, determine if type 1 or 3
        const node = mathjs.max(..._link);
        type = scannedNodes.includes(node) ? 3 : 1;
        type === 1 && scannedNodes.push(node);
    } else {
        // Doesn't include reference node, determine if type 2 or 4
        type = scannedNodes.includes(_link[0]) && scannedNodes.includes(_link[1]) ? 4 : 2;
        type === 2 && scannedNodes.includes(_link[0]) && scannedNodes.push(_link[1]);
        type === 2 && scannedNodes.includes(_link[1]) && scannedNodes.push(_link[0]);
    }

    return type;
}

/**
 * Builds Y-bus matrix from Z-bus matrix.
 *
 * @param {object} zbus Z-bus
 *
 * @return {object} Y-bus
 */
function yBusBuilder(zbus) {
    utils.debugLog(DEBUG, '[yBusBuilder] Receiving z-bus', zbus.valueOf());

    let ybus = zbus.clone();
    ybus = mathjs.divide(1, zbus);

    utils.debugLog(DEBUG, '[yBusBuilder] Returning y-bus', ybus.valueOf());
    return ybus;
}

module.exports.zBusBuilder = zBusBuilder;
module.exports.yBusBuilder = yBusBuilder;
