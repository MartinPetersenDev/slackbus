/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';
require('source-map-support').install();

exports.handler = async (event) => {
    const message = 'SlackBUS Engine API v1';

    const response = {
        statusCode: 200,
        body: JSON.stringify(message),
    };
    return response;
};
