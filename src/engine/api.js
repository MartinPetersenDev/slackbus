/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const fetch = require('node-fetch');
const utils = require('../sdk/utils');

// Config DEBUG
const DEBUG = false;

/**
 * apiCalculateNet uses the RESTful API to perform calculations on network.
 * Set network.options as relevant.
 *
 * @param {object} net - Network object to send to RESTful API
 * @param {strin} url - URL to RESTful API
 *
 */
const apiCalculateNet = async (net, url) => {
    // Set POST options
    const postOptions = {
        Accept: 'application/json',
        'Content-type': 'application/json',
    };

    /* istanbul ignore next */
    utils.debugLog(DEBUG, 'Calling API...', '');

    // Call API with fetch
    let response;
    try {
        response = await fetch(url, {
            method: 'POST',
            headers: postOptions,
            body: JSON.stringify(net),
        });

        // Proces response
        if (response.status === 200) {
            const result = await response.json();

            /* istanbul ignore next */
            utils.debugLog(DEBUG, 'Got status 200 OK');
            utils.debugLog(DEBUG, '---Raw data---', result.data);

            net = utils.convertNodeUtoComplex(result.data);

            // Output to console
            utils.resultsToConsole(net);
        } else {
            // eslint-disable-next-line spellcheck/spell-checker
            // throw new Error(`API responded with status ${response.status} `);
            console.log(`API responded with status ${response.status} `);
        }
    } catch (err) {
        console.log(`Failed to reach API at ${url}`);
    }
    // Return network
    return net;
};

module.exports.apiCalculateNet = apiCalculateNet;
