/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';
const mathjs = require('mathjs');
const utils = require('../sdk/utils.js');
const bb = require('./busBuilder');
const sc = require('./shortcircuit');
const lf = require('./loadflow');
const os = require('os');

// Config DEBUG
const DEBUG = false;
const VERBOSE = false;
const showCPUInfo = false;

/**
 * Performs network calculations on network as selected in options.
 * @param {object} network - Network, assumed to be of type object.
 * @param {object} options - Calculation options.
 *
 * @returns {object} network
 */
function calculateNet(net) {
    // Variables
    let network = { ...net };

    // Reconstruct connectivity matrix, if it has been stringified
    if (mathjs.typeOf(network.matrix.connectivity) === 'Object') {
        utils.debugLog(DEBUG, '[calculateNet] Connectivity matrix', JSON.stringify(network.matrix.connectivity));
        network.matrix.connectivity = mathjs.matrix(network.matrix.connectivity.data || network.matrix.connectivity._data);
    }

    utils.debugLog(DEBUG, '[calculateNet] DUMP Network to console', JSON.stringify(network));

    network = utils.convertNodeUtoComplex(network);

    /* Calculate loadflow currents and update network */
    if (network.options.calcLoadflow) {
        network = calculateLoadflow(network);
    }

    /* Shortcircuit calculations */
    if (network.options.calcIk1 || network.options.calcIk2 || network.options.calcIk2EE || network.options.calcIk3) {
        // Find ik_max
        // network = calculateShortcircuit(network, network.options.cMax, false);
        network = calculateShortcircuit(network, false);
        for (let i = 0; i < network.nodes.length; i++) {
            const array = [
                network.nodes[i].sc.ik3,
                network.nodes[i].sc.ik2,
                network.nodes[i].sc.ik1,
                // temp.nodes[i].sc.ik2EE /// Should this even be included?
            ];
            network.nodes[i].sc.ikmax = Math.max.apply(null, array.filter(Boolean)); // Excludes zero, 0 is falsy
        }

        // Clone network and calculate ik_min with applied conductor heating
        // const temp = calculateShortcircuit(JSON.parse(JSON.stringify(network)), network.options.cMin, true);
        const temp = calculateShortcircuit(JSON.parse(JSON.stringify(network)), true);
        for (let i = 0; i < temp.nodes.length; i++) {
            const array = [
                temp.nodes[i].sc.ik3,
                temp.nodes[i].sc.ik2,
                temp.nodes[i].sc.ik1,
                // temp.nodes[i].sc.ik2EE /// Should this even be included?
            ];
            network.nodes[i].sc.ikmin = Math.min.apply(null, array.filter(Boolean)); // Excludes zero, 0 is falsy
        }
    }

    /* istanbul ignore next */
    if (showCPUInfo) {
        if (typeof network.addMessage === 'function') {
            network.addMessage('calculateNet', 'CPU type', os.cpus());
        } else {
            network.messages.push({ source: 'calculateNet', message: 'CPU type', value: os.cpus() });
        }
    }

    // Return network object
    utils.debugLog(DEBUG, '[calculateNet] Returning network', JSON.stringify(network));
    return network;
}

/**
 * Calculates shortcircuit currents, and returns network
 */
// function calculateShortcircuit(net, cfactor = 1, heatedConductor = false) {
function calculateShortcircuit(net, ikmin = false) {
    const network = { ...net };

    // Apply optional heated conductor factor
    // if (heatedConductor) {
    if (ikmin) {
        network.links.posSeq = utils.applyConductorHeating(network.links.posSeq);
        network.links.zeroSeq = utils.applyConductorHeating(network.links.zeroSeq);
    }

    network.links.posSeq = utils.convertLinksToComplex(network.links.posSeq);
    network.links.zeroSeq = utils.convertLinksToComplex(network.links.zeroSeq);

    // Apply Kt to transformers
    // if (cfactor > 1) {
    // network.links.posSeq.map((link) => {
    //     link.zm = link.Kt > 1 ? mathjs.multiply(link.zm, link.Kt) : link.zm;
    //     link.z0 = link.Kt > 1 ? mathjs.multiply(link.z0, link.Kt) : link.z0;
    // });
    // network.links.zeroSeq.map((link) => {
    //     link.zm = link.Kt > 1 ? mathjs.multiply(link.zm, link.Kt) : link.zm;
    //     link.z0 = link.Kt > 1 ? mathjs.multiply(link.z0, link.Kt) : link.z0;
    // });
    // }

    const posSeqLinks = getAllLinkImpedances(network.links.posSeq);
    const zeroSeqLinks = getAllLinkImpedances(network.links.zeroSeq);

    utils.debugLog(DEBUG, '[calculateNet] Dump posSeqLinks and zeroSeqLinks links to console', '');
    utils.debugLog(DEBUG, '... posSeqLinks', JSON.stringify(posSeqLinks));
    utils.debugLog(DEBUG, '... zeroSeqLinks', JSON.stringify(zeroSeqLinks));

    // Build zbus
    let zBusZeroSeq;
    const zBusPosSeq = bb.zBusBuilder(posSeqLinks, 'zm');
    network.matrix.zBusPosSeq = utils.complexMatrixToArray(zBusPosSeq);
    if (network.options.calcIk1 || network.options.calcIk2EE) {
        zBusZeroSeq = bb.zBusBuilder(zeroSeqLinks, 'z0');
        network.matrix.zBusZeroSeq = utils.complexMatrixToArray(zBusZeroSeq);
    }

    // Calculate shortcircuit currents and update network
    let zM;
    let z0;
    let unPu;
    let i;

    utils.debugLog(DEBUG, '[calculateNet] network.nodes.length', network.nodes.length);

    // Ik3 and Ik2
    i = 0;
    network.nodes.map((node) => {
        utils.debugLog(VERBOSE, '[calculateNet] Calling calculateIsc3 on node', node.name);

        zM = zBusPosSeq.subset(mathjs.index(i, i));

        // Always use Un for shortcircuit calculations
        // unPu = network.options.calcLoadflow ? node.U : mathjs.complex(node.Un, 0);
        unPu = mathjs.complex(node.Un, 0);

        // Determince c-factor
        // const cmax = node.vBase <= 1.0 ? 1.05 : 1.1;
        // const cmin = node.vBase <= 1.0 ? 0.95 : 1.0;
        const cmax = node.sc.cMax;
        const cmin = node.sc.cMin;

        // ik3 and ik2
        node.sc.ik3 = sc.calculateIsc3(unPu, zM, ikmin ? cmin : cmax);
        node.sc.ik2 = (mathjs.sqrt(3) / 2) * node.sc.ik3;

        i++;

        utils.debugLog(VERBOSE, '... U', JSON.stringify(node.U));
        utils.debugLog(VERBOSE, '... Un', JSON.stringify(node.Un));
        utils.debugLog(VERBOSE, '... unPu', unPu);
        utils.debugLog(VERBOSE, '... vBase', node.vBase);
        utils.debugLog(VERBOSE, '... zm', JSON.stringify(zM));
    });

    // Ik1
    if (network.options.calcIk1) {
        i = 0;
        network.nodes.map((node) => {
            zM = zBusPosSeq.subset(mathjs.index(i, i)); // <- 5 entries
            z0 = zBusZeroSeq.subset(mathjs.index(i, i)); // <- 4 entries

            // Always use Un for shortcircuit calculations
            // unPu = network.options.calcLoadflow ? bus.U : mathjs.complex(bus.Un, 0);
            unPu = mathjs.complex(node.Un, 0);

            // Determince c-factor
            // const cmax = node.vBase <= 1.0 ? 1.05 : 1.1;
            // const cmin = node.vBase <= 1.0 ? 0.95 : 1.0;
            const cmax = node.sc.cMax;
            const cmin = node.sc.cMin;

            // ik1
            node.sc.ik1 = sc.calculateIsc1(unPu, zM, z0, ikmin ? cmin : cmax);

            i++;
        });
    }

    // Ik2EE
    if (network.options.calcIk2EE) {
        i = 0;
        network.nodes.map((node) => {
            zM = zBusPosSeq.subset(mathjs.index(i, i));
            z0 = zBusZeroSeq.subset(mathjs.index(i, i));

            // Always use Un for shortcircuit calculations
            // unPu = network.options.calcLoadflow ? node.U : mathjs.complex(node.Un, 0);
            unPu = mathjs.complex(node.Un, 0);

            // Determince c-factor
            const cmax = node.vBase <= 1.0 ? 1.05 : 1.1;
            const cmin = node.vBase <= 1.0 ? 0.95 : 1.0;

            // ik2ee
            node.sc.ik2EE = sc.calculateIsc2EE(unPu, zM, z0, ikmin ? cmin : cmax);

            i++;
        });
    }

    return network;
}

/**
 * Calculates loadflow, and returns network
 */
function calculateLoadflow(net) {
    let network = { ...net };

    network.links.posSeq = utils.convertLinksToComplex(network.links.posSeq);
    network.links.posSeqForYbus = utils.convertLinksToComplex(network.links.posSeqForYbus);
    network.links.zeroSeq = utils.convertLinksToComplex(network.links.zeroSeq);

    const posSeqLinks = getAllLinkImpedances(network.links.posSeq);
    const zyLinks = getAllLinkImpedances(network.links.posSeqForYbus);
    const zeroSeqLinks = getAllLinkImpedances(network.links.zeroSeq);

    utils.debugLog(DEBUG, '[calculateNet] Dump zbus_y links to console', JSON.stringify(zyLinks));

    // build z-bus for single phase loads
    const zBusPosSeq = bb.zBusBuilder(posSeqLinks, 'zm');
    const zBusZeroSeq = bb.zBusBuilder(zeroSeqLinks, 'z0');
    network.matrix.zBusPosSeq = utils.complexMatrixToArray(zBusPosSeq);
    network.matrix.zBusZeroSeq = utils.complexMatrixToArray(zBusZeroSeq);

    // Build ybus
    const zBusForYBus = bb.zBusBuilder(zyLinks, 'zm');
    network.matrix.zBusForYBus = utils.complexMatrixToArray(zBusForYBus);
    network.matrix.yBus = bb.yBusBuilder(zBusForYBus);

    // Calculate loadflow
    utils.debugLog(VERBOSE, '[calculateNet] Calling calcLoadflowGaussSeidel, maxIterations', network.options.maxLoadflowIterations);
    network = lf.gaussSeidel(network);

    return network;
}

/**
 * getAllLinkImpedances returns all links sorted by field 'toBus' in ascending order.
 *
 * @param {array} links - zm or z0 links array from network
 */
function getAllLinkImpedances(links = []) {
    let size = 0;

    links.forEach((link) => {
        if (link.from > size) {
            size = link.from;
        } else if (link.to > size) {
            size = link.to;
        }
    });

    // Construct return-data
    return {
        size: size,
        links: links.sort(sortLinksByToBus),
    };
}

/**
 * sortLinksByToBus is a helper function for Array-sorting
 * @param {number} a
 * @param {number} b
 */
function sortLinksByToBus(a, b) {
    // skip ref nodes
    if (a.from === 0) {
        return 0;
        // a should come before b in the sorted order
    } else if (a.to < b.to) {
        return -1;
        // a should come after b in the sorted order
    } else if (a.to > b.to) {
        return 1;
        // a and b are the same
    } else {
        return 0;
    }
}

module.exports.calculateNet = calculateNet;

/* istanbul ignore next */
module.exports.submit = async (event, context, callback) => {
    // Setup multiple CORS origins
    const ALLOWED_ORIGINS = [
        'http://localhost:8000',
        'http://localhost:8080',
        'http://dev042.firmaetpetersen.dk',
        'http://www.slackbus.net',
        'http://slackbus.net',
    ];

    const origin = event.headers.origin;
    let headers;

    if (ALLOWED_ORIGINS.includes(origin)) {
        headers = {
            'Access-Control-Allow-Origin': origin,
            'Access-Control-Allow-Credentials': true,
        };
    } else {
        headers = {
            'Access-Control-Allow-Origin': 'http://none.shall.pass',
        };
    }

    const bodyData = JSON.parse(event.body);
    const returnNetwork = calculateNet(bodyData);

    const response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
            message: 'SlackBUS Engine API executed successfully!',
            data: returnNetwork,
        }),
    };

    callback(null, response);
};
