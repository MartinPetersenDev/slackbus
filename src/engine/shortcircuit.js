/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const mathjs = require('mathjs');
const utils = require('../sdk/utils.js');

// Config DEBUG
const DEBUG = false;
const VERBOSE = false;

/**
 * Calculates three phase shortcircuit
 * @param {*} voltage
 * @param {*} impedanceZm
 * @param {*} cFactor
 *
 * @returns {Number} current
 */
function calculateIsc3(voltage, impedanceZm, cFactor = 1) {
    utils.debugLog(DEBUG, '[calculatesIsc3] called with U typeof', typeof voltage);
    utils.debugLog(DEBUG, '[calculatesIsc3]', JSON.stringify(voltage));
    utils.debugLog(DEBUG, '[calculatesIsc3] impedanceZm typeof', typeof impedanceZm);
    utils.debugLog(DEBUG, '[calculatesIsc3]', JSON.stringify(impedanceZm));
    utils.debugLog(VERBOSE, '[calculatesIsc3] called with U', JSON.stringify(voltage));
    utils.debugLog(VERBOSE, '[calculatesIsc3] called with z_m', JSON.stringify(impedanceZm));
    utils.debugLog(VERBOSE, '[calculatesIsc3] called with cFactor', cFactor);
    utils.debugLog(DEBUG, '[calculatesIsc3] cFactor typeof', typeof cFactor);

    /* 
    MATH
    Isc3 = cFactor*voltage / Zm    *note* sqrt(3) is applied later from vBase on node object

    a = cFactor*voltage
    
    Isc3 = abs (a / b)
    */
    const _a = mathjs.multiply(cFactor, recastAsComplex(voltage));
    const _current = mathjs.abs(mathjs.divide(_a, recastAsComplex(impedanceZm)));
    return _current;
}

/**
 * Recast number as mathjs Complex
 *
 * @param {object} - number
 * @returns {object} - Mathjs complex object
 */
function recastAsComplex(number) {
    if ((number.re || number.re === 0) && (number.im || number.im === 0)) {
        return mathjs.complex(number);
    } else {
        throw new Error(`Shortcircuit calculation not passed a complex number ${JSON.stringify(number)}`);
    }
}

/**
 * Calculates one phase shortcircuit
 * @param {*} voltage
 * @param {*} impedanceZm
 * @param {*} impedanceZ0
 * @param {*} cFactor
 *
 * @returns {Number} current
 */
function calculateIsc1(voltage, impedanceZm, impedanceZ0, cFactor = 1) {
    utils.debugLog(VERBOSE, '[calculatesIsc1] called with U', JSON.stringify(voltage));
    utils.debugLog(VERBOSE, '[calculatesIsc1] called with z_m', JSON.stringify(impedanceZm));
    utils.debugLog(VERBOSE, '[calculatesIsc1] called with z_0', JSON.stringify(impedanceZ0));
    utils.debugLog(VERBOSE, '[calculatesIsc1] called with cFactor', cFactor);

    /*
    MATH
    Isc1 = cFactor*voltage / abs(2*zm+z0)
    
    a = cFactor*voltage    *note* sqrt(3) is applied later from vBase on node object
    b = 2*zm + z0
    Isc1 = abs (a / b) * 3
    */
    const _a = mathjs.multiply(cFactor, recastAsComplex(voltage));
    let _b = mathjs.multiply(2, recastAsComplex(impedanceZm));
    _b = mathjs.add(_b, recastAsComplex(impedanceZ0));
    const _current = mathjs.abs(mathjs.divide(_a, _b)) * 3;

    return _current;
}

/**
 * Calculates two-phase shortcircuit to ground
 * @param {*} voltage
 * @param {*} impedanceZm
 * @param {*} impedanceZ0
 * @param {*} cFactor
 *
 * @returns {Number} current
 */
function calculateIsc2EE(voltage, impedanceZm, impedanceZ0, cFactor = 1) {
    /* 
    MATH
    Isc2EE = sqrt(3)*cFactor*voltage / abs(zm + 2*z0) *note* sqrt(3) is applied later from vBase on node object

    a = cFactor*voltage
    b = zm + 2*z0
    Isc2EE = abs (a / b) * 3
    */
    const _a = mathjs.multiply(cFactor, recastAsComplex(voltage));
    let _b = mathjs.multiply(2, recastAsComplex(impedanceZ0));
    _b = mathjs.add(_b, recastAsComplex(impedanceZm));
    const _current = mathjs.abs(mathjs.divide(_a, _b)) * 3;

    return _current;
}

module.exports.calculateIsc3 = calculateIsc3;
module.exports.calculateIsc1 = calculateIsc1;
module.exports.calculateIsc2EE = calculateIsc2EE;
