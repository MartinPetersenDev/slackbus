/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const mathjs = require('mathjs');
const busjs = require('../sdk/node');
const utils = require('../sdk/utils');

// Config
const VERBOSE = false;
const DEBUG = false;

/**
 * Calculates load-flow with Gauss-Seidel iteration algorithm
 *
 * @param {object} net - network object
 * @returns {object} network
 */
function gaussSeidel(network) {
    let net = { ...network };

    // Setup local variables
    const startDate = new Date();
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];
    let iterations = 0;
    let maxIterations = net.options.maxLoadflowIterations;
    const convergenceLimit = net.options.convergenceLimit;

    utils.debugLog(DEBUG, '[loadflow] Using y-bus', ybus.valueOf());
    utils.debugLog(VERBOSE, '[loadflow] Bus voltages at start of algorithm', '');
    net.nodes.forEach((node) => {
        utils.debugLog(VERBOSE, '... [node.name]', node.name);
        utils.debugLog(VERBOSE, '... ... U', JSON.stringify(node.U));
        utils.debugLog(VERBOSE, '... ... node.type', node.type);
    });

    /*
     * Calculation notes and overall proces:
     *   - There can only be one slack bus, and it must always be at
     *     matrix index=0, meaning calculations start from index=1.
     *   - Calculate load-nodes first
     *   - Calculate generator-nodes
     *   - If a generator-bus reaches its Q limit, isPVLimited is set, and
     *     node is treated as PQ bus.
     *   - Update slackbus P and Q
     *   - Calculate power and power-loss
     *   - Calculate parallel lines and line current
     *
     * MATH Notation in comments:
     *   - * is multiply
     *   - ** is conjugate and not "power of"
     */

    net = addShunts(net);

    for (let n = 0; n < maxIterations; n++) {
        utils.debugLog(DEBUG, '[loadflow] iteration', ++iterations);

        net = iteratePQBus(net);
        net = iteratePVBus(net);
        net = calculatePQ(net);

        // Check each node for convergence
        let maxMismatch = 0;
        for (let i = 0; i < size; i++) {
            const num = Math.abs(Math.max(net.nodes[i].lf.Pmismatch, net.nodes[i].lf.Qmismatch));
            maxMismatch = num > maxMismatch ? num : maxMismatch;

            utils.debugLog(VERBOSE, '[loadflow] node', net.nodes[i].name);
            utils.debugLog(VERBOSE, '... maxMismatch', maxMismatch);
        }
        maxIterations = maxMismatch < convergenceLimit ? 0 : maxIterations;
    }

    // Log results, when iteration loop is done
    const endDate = new Date();
    utils.debugLog(VERBOSE, '[loadflow] Loadflow converged in [s]', ((endDate - startDate) / 1000).toFixed(3));
    utils.debugLog(VERBOSE, '...  after [iterations]', iterations);

    net = calculateSlackbus(net);
    net = calculatePower(net);
    net = calculateParallelLinks(net);

    // Add messages to network
    const strMsgSource = 'Loadflow';
    const strDuration = `Calculation ended after ${((endDate - startDate) / 1000).toFixed(3)}s and ${iterations} iterations`;
    const strDidNotConverge = 'Loadflow did not converge';
    addResultMessage(net, strMsgSource, strDuration, 'message');
    iterations === maxIterations && addResultMessage(net, strMsgSource, strDidNotConverge, 'error');

    return net;
}

/**
 * Check connectivity matrix for parallel links, and divide power between them.
 * Parallel links is assumed to have equal impedance.
 *
 * @param {object} network
 * @returns {object} network
 */
function calculateParallelLinks(network) {
    const net = { ...network };

    net.links.posSeqForYbus.forEach((link) => {
        const noOfConnections = link.from > 0 && net.matrix.connectivity.subset(mathjs.index(link.from - 1, link.to - 1));

        if (link.from > 0 && noOfConnections > 1) {
            link.P = link.P / noOfConnections;
            link.Q = link.Q / noOfConnections;
            link.Ib = link.Ib / noOfConnections;
            link.Ploss = link.Ploss / noOfConnections;
            link.Qloss = link.Qloss / noOfConnections;

            utils.debugLog(VERBOSE, '[loadflow] Parallel links', '');
            utils.debugLog(VERBOSE, '... from', link.from);
            utils.debugLog(VERBOSE, '... to', link.to);
            utils.debugLog(VERBOSE, '... link.name', link.name);
            utils.debugLog(VERBOSE, '... no of parallel connections', noOfConnections);
        }
    });

    return net;
}

/**
 * Calculate power and line-loss and return resulting network
 *
 * @param {object} network
 * @returns {object} network
 */

function calculatePower(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];
    let a = 0;
    let b = 0;
    let c = 0;

    // MATH:
    // a = I_ij = (U_i-U_j)*Y_ij
    //     "Line current" - or more precisely, current between to nodes
    //      through total impedance Z_ji = 1/Y_ij
    //      (eg there could be parallel lines)
    //     I_ji = -1 * I_ij
    // b = S_ij = V_i * I_ij**          // Line dispatch power
    // c = S_ji = V_j * I_ij)**         // Line receive power
    //
    // Then check whichever is the larger of Ui,Uj and b,c
    // temp = b + c                     // Line loss
    for (let i = 0; i < size; i++) {
        for (let j = 0; j < size; j++) {
            if (i !== j) {
                a = mathjs.multiply(ybus.subset(mathjs.index(i, j)), mathjs.subtract(net.nodes[i].lf.U, net.nodes[j].lf.U));
                b = mathjs.multiply(net.nodes[i].lf.U, mathjs.conj(a));
                c = mathjs.multiply(net.nodes[j].lf.U, mathjs.conj(mathjs.multiply(-1, a)));

                const temp = mathjs.add(b, c);
                net.links.posSeqForYbus.forEach((link) => {
                    if (link.from !== 0 && link.from - 1 === i && link.to - 1 === j) {
                        if (Math.abs(b.re) > Math.abs(c.re)) {
                            link.P = b.re < 0 ? -1 * b.re : b.re;
                            // b.re below, is not a typo; im might have different sign
                            link.Q = b.re < 0 ? -1 * b.im : b.im;
                        } else {
                            link.P = c.re < 0 ? -1 * c.re : c.re;
                            // b.re below, is not a typo; im might have different sign
                            link.Q = c.re < 0 ? -1 * c.im : c.im;
                        }

                        link.Ploss = Math.abs(temp.re);
                        link.Qloss = Math.abs(temp.im);
                        link.Ib = Math.sqrt(link.P ** 2 + link.Q ** 2);

                        if (link.singlePhase) {
                            link.dU = 2 * link.Ib * link.zm.abs() * 100;
                        } else {
                            link.dU = link.Ib * link.zm.abs() * 100;
                        }
                    }
                });
            }
        }
    }

    return net;
}

/**
 * Calculate slackbus P and Q and return resulting network
 *
 * @param {object} network
 * @returns {object} network
 */
function calculateSlackbus(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];
    let a = 0;
    let b = 0;
    let c = 0;

    // MATH:
    // P = sum( abs(V0)*abs(Vj)*Abs(Y0j) * cos(angle_0j - angle_0 + angle_j) )
    for (let j = 0; j < size; j++) {
        a = mathjs.multiply(net.nodes[0].lf.U, net.nodes[j].lf.U);
        a = mathjs.abs(mathjs.multiply(a, ybus.subset(mathjs.index(0, j))));
        b = ybus.subset(mathjs.index(0, j)).arg();
        b -= net.nodes[0].lf.U.arg();
        b += net.nodes[j].lf.U.arg();
        b = mathjs.cos(b); // cosine must be in radians
        c = mathjs.add(c, mathjs.multiply(a, b));
    }

    net.nodes[0].lf.Pgen = c + net.nodes[0].lf.Pload;

    // MATH:
    // Q = - sum( abs(V0)*abs(Vj)*Abs(Y0j) * sin(angle_oj - angle_0 + angle_j) )
    a = 0;
    b = 0;
    c = 0;

    for (let j = 0; j < size; j++) {
        a = mathjs.multiply(net.nodes[0].lf.U, net.nodes[j].lf.U);
        a = mathjs.abs(mathjs.multiply(a, ybus.subset(mathjs.index(0, j))));
        b = ybus.subset(mathjs.index(0, j)).arg();
        b -= net.nodes[0].lf.U.arg();
        b += net.nodes[j].lf.U.arg();
        b = mathjs.sin(b); // sine must be in radians
        c = mathjs.add(c, mathjs.multiply(a, b));
    }

    net.nodes[0].lf.Qgen = -1 * c + net.nodes[0].lf.Qload;

    return net;
}

/**
 * Iterate P and Q and return resulting network
 *
 * @param {object} network
 * @returns {object} network
 */
function calculatePQ(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];

    for (let i = 0; i < size; i++) {
        let sm = mathjs.complex(0, 0);

        for (let j = 0; j < size; j++) {
            const temp = mathjs.multiply(ybus.subset(mathjs.index(i, j)), net.nodes[j].lf.U);
            sm = mathjs.add(sm, temp);
        }

        let s = mathjs.conj(net.nodes[i].lf.U);
        s = mathjs.multiply(s, sm);

        net.nodes[i].lf.Pmismatch = net.nodes[i].lf.P - s.re;
        net.nodes[i].lf.Qmismatch = net.nodes[i].lf.Q - s.im;
        net.nodes[i].lf.P = s.re;
        net.nodes[i].lf.Q = s.im;

        utils.debugLog(VERBOSE, '[loadflow] node', net.nodes[i].name);
        utils.debugLog(VERBOSE, '... P', s.re);
        utils.debugLog(VERBOSE, '... Q', s.im);
        utils.debugLog(VERBOSE, '... Pmis', net.nodes[i].lf.Pmismatch);
        utils.debugLog(VERBOSE, '... Qmis', net.nodes[i].lf.Qmismatch);
    }
    return net;
}

/**
 * Iterate PV-busses and return resulting network
 *
 * @param {object} network
 * @returns {object} network
 */
function iteratePVBus(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];

    for (let i = 1; i < size; i++) {
        if (net.nodes[i].lf.type === busjs.typeEnum.PV) {
            // Estimate reactive power for Generator PV-bus
            // MATH: q_i = - Im ( V_i**  * sum( V_i*y_ij )
            let b = 0;
            for (let j = 0; j < size; j++) {
                b = mathjs.add(b, mathjs.multiply(ybus.subset(mathjs.index(i, j)), net.nodes[j].lf.U));
            }

            const c = mathjs.multiply(mathjs.conj(net.nodes[i].lf.U), b);

            // Check Q limit
            if (net.options.respectQLimits) {
                utils.debugLog(DEBUG, '[loadflow] PV-Bus', net.nodes[i].name);
                utils.debugLog(DEBUG, '... QlimitMax', net.nodes[i].lf.QlimitMax);
                utils.debugLog(DEBUG, '... QlimitMin', net.nodes[i].lf.QlimitMin);

                if (-1 * c.im > net.nodes[i].lf.QlimitMax) {
                    utils.debugLog(VERBOSE, '... exceeded QlimitMax', net.nodes[i].lf.QlimitMax);
                    net.nodes[i].lf.Qgen = net.nodes[i].lf.QlimitMax;
                } else if (-1 * c.im < net.nodes[i].lf.QlimitMin) {
                    utils.debugLog(VERBOSE, '... exceeded QlimitMin', net.nodes[i].lf.QlimitMin);
                    net.nodes[i].lf.Qgen = net.nodes[i].lf.QlimitMin;
                } else {
                    net.nodes[i].lf.Qgen = -1 * c.im;
                }
            } else if (!net.options.respectQLimits) {
                net.nodes[i].lf.Qgen = -1 * c.im;
            }

            // Estimate voltage for Generator PV-bus
            // MATH: v_i = 1/(y_ij) * ( (P_i - Q_i i)/V_i* + sum(y_ij*v_j) )
            const a = mathjs.divide(1, ybus.subset(mathjs.index(i, i)));

            // Add loads if bus has any
            b = mathjs.complex(net.nodes[i].lf.Psch, -1 * net.nodes[i].lf.Qgen);
            b = mathjs.divide(b, mathjs.conj(net.nodes[i].lf.U));
            for (let j = 0; j < size; j++) {
                if (i !== j) {
                    b = mathjs.subtract(b, mathjs.multiply(ybus.subset(mathjs.index(j, i)), net.nodes[j].lf.U));
                }
            }
            const temp = mathjs.multiply(a, b);

            utils.debugLog(VERBOSE, '[loadflow] PV-bus', net.nodes[i].name);
            utils.debugLog(VERBOSE, '... correcting voltage', temp);
            utils.debugLog(VERBOSE, '... to', net.nodes[i].lf.U);

            // Correct re part of voltage, but keep magnitude and im.
            temp.re = mathjs.sqrt(mathjs.pow(mathjs.abs(net.nodes[i].lf.U), 2) - mathjs.pow(temp.im, 2));

            // Update mismatch
            net.nodes[i].lf.Umismatch = mathjs.subtract(net.nodes[i].lf.U, temp).abs();
            utils.debugLog(DEBUG, '[loadflow] node', net.nodes[i].name);
            utils.debugLog(DEBUG, '... voltage mismatch', net.nodes[i].lf.Umismatch);

            // Update three-phase voltage
            net.nodes[i].lf.U = temp;
        }
    }
    return net;
}

/**
 * Iterate PQ-busses and return resulting network
 *
 * @param {object} network
 * @returns {object} network
 */
function iteratePQBus(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();
    const size = ybus.size()[0];

    for (let i = 1; i < size; i++) {
        if (net.nodes[i].lf.type === busjs.typeEnum.PQ) {
            // Estimate voltage
            // MATH: v_i = 1/(y_i) * ( (Psch_i - Qsch_i i)/V_i* + sum(y_ij*V_j) )
            const a = mathjs.divide(1, ybus.subset(mathjs.index(i, i)));

            // Add loads if bus has any
            let b = mathjs.complex(net.nodes[i].lf.Psch, -1 * net.nodes[i].lf.Qsch);
            b = mathjs.divide(b, mathjs.conj(net.nodes[i].lf.U));

            for (let j = 0; j < size; j++) {
                if (i !== j) {
                    b = mathjs.subtract(b, mathjs.multiply(ybus.subset(mathjs.index(i, j)), net.nodes[j].lf.U));
                }
            }

            const temp = mathjs.multiply(a, b);

            // Update voltage and apply acceleration factor
            // MATH:
            // U = U + acceleration * (temp - U)
            net.nodes[i].lf.U = mathjs.add(
                net.nodes[i].lf.U,
                mathjs.multiply(net.options.gsAccelerationFactor, mathjs.subtract(temp, net.nodes[i].lf.U))
            );

            utils.debugLog(DEBUG, '[loadflow] PQ-Bus');
            utils.debugLog(DEBUG, '... name', net.nodes[i].name);

            // Calculate single-phase voltage using symmetrical composants
            // MATH:
            // U_ln = Ua / sqrt(3)
            // Ua = Ua1 + Ua2 + U0
            // Ua1 = Un - Zm * I0
            // Ua2 = - Zm * I0
            // U0 =  - Z0 * I0
            // Zb is the impedance of the single-phase load
            // ia = 3*Un / (2*Zm + Z0 + 3*Zb)
            // I0 = ia / 3

            if (net.nodes[i].lf.singlePhase) {
                let zm = net.matrix.zBusPosSeq[i][i];
                zm = mathjs.complex(zm.re, zm.im);

                // Un and Zb
                const _Un = mathjs.complex(net.nodes[i].Un, 0);
                const _s = mathjs.complex(net.nodes[i].lf.Pload, net.nodes[i].lf.Qload);
                const _zb = mathjs.divide(mathjs.pow(_Un, 2), _s);

                // Z0
                let _z0 = net.matrix.zBusZeroSeq[i][i];
                _z0 = mathjs.complex(_z0.re, _z0.im);

                // ia
                let _ia = mathjs.multiply(3, _Un);
                const _zm2 = mathjs.multiply(2, zm);
                const _zb3 = mathjs.multiply(3, _zb);
                let _temp = mathjs.add(_zm2, _z0);
                _temp = mathjs.add(_temp, _zb3);
                _ia = mathjs.divide(_ia, _temp);

                // i0
                const i0 = mathjs.divide(_ia, 3);

                // ua1 and ua2
                let _zmi0 = mathjs.multiply(zm, i0);
                _zmi0 = mathjs.multiply(-1, _zmi0);
                const _ua1 = mathjs.add(_Un, _zmi0);
                const _ua2 = mathjs.add(0, _zmi0);

                // u0
                let _z0i0 = mathjs.multiply(_z0, i0);
                _z0i0 = mathjs.multiply(-1, _z0i0);
                const _u0 = mathjs.add(0, _z0i0);

                // Ua
                let _ua = mathjs.add(_ua1, _ua2);
                _ua = mathjs.add(_ua, _u0);

                // U_ln
                const _uln = mathjs.divide(_ua, mathjs.sqrt(3));
                net.nodes[i].lf.Uln = _uln;

                utils.debugLog(DEBUG, '... voltage Uln=', _uln.abs());
            } else {
                net.nodes[i].lf.Uln = mathjs.divide(net.nodes[i].lf.U, Math.sqrt(3));
                utils.debugLog(DEBUG, '... voltage U=', temp.abs());
            }
        }
    }

    return net;
}

/**
 * Add result message to network
 *
 * @param {object} network
 * @param {string} strSource
 * @param {string} strMessage
 * @param {string} strType
 */
function addResultMessage(network, strSource, strMessage, strType) {
    if (typeof network.addMessage === 'function') {
        network.addMessage(strSource, strMessage, null, strType);
    } else {
        network.messages.push({
            source: strSource,
            message: strMessage,
            value: null,
            type: strType,
        });
    }
}

/**
 * Add shunts to ybus and return new network
 *
 * @param {object} network
 * @returns {object} network
 */
function addShunts(network) {
    const net = { ...network };
    const ybus = net.matrix.yBus.clone();

    // Cast node values as integers
    const zmlinks = [];
    net.links.posSeqForYbus.forEach((link) => {
        link.from = parseInt(link.from);
        link.to = parseInt(link.to);
        zmlinks.push(link);
    });

    let a = 0;
    zmlinks.forEach((link) => {
        if (link.from > 0 && (link.shunt.re !== 0 || link.shunt.im !== 0)) {
            utils.debugLog(DEBUG, '[loadflow] Processing link', link.name);
            utils.debugLog(DEBUG, '... shunt', JSON.stringify(link.shunt));
            utils.debugLog(VERBOSE, '[loadflow] Adding shunt Y', JSON.stringify(link.shunt));
            utils.debugLog(VERBOSE, '... from matrix index', link.from - 1);
            utils.debugLog(VERBOSE, '... to matrix index', link.to - 1);

            // Add shunt/2 to diagonal in y-bus[from,from]
            a = ybus.subset(mathjs.index(link.from - 1, link.from - 1));
            a = mathjs.add(a, mathjs.divide(link.shunt, 2));
            ybus.subset(mathjs.index(link.from - 1, link.from - 1), a);

            // also for shunt to diagonal in y-bus[to,to]
            a = ybus.subset(mathjs.index(link.to - 1, link.to - 1));
            a = mathjs.add(a, mathjs.divide(link.shunt, 2));
            ybus.subset(mathjs.index(link.to - 1, link.to - 1), a);
        }
    });

    net.matrix.yBus = ybus;

    return net;
}

module.exports.gaussSeidel = gaussSeidel;
