/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const utils = require('./utils');
const linkjs = require('../sdk/link');
const sdkUtils = require('../sdk/utils');
const Complex = require('complex.js');
const mathjs = require('mathjs');

// Config
const VERBOSE = false;

/**
 * Add transformer to network
 *
 * @param graph {object} - graph
 * @param cell {object} - cell
 * @param net {object} - network
 */
function addTransformer(graph, cell, net) {
    const name = cell.getAttribute('Name', '');
    let Un1;
    let Un2;

    try {
        // Get node and vBase from connected edges
        const fromEdge = utils.getvBaseFromNode(graph, cell, 0);
        const toEdge = utils.getvBaseFromNode(graph, cell, 1);

        // Set attributes on diagram objects
        cell.setAttribute('to', parseInt(toEdge.node));
        cell.setAttribute('from', parseInt(fromEdge.node));
        const vBase = Math.min(toEdge.vBase, fromEdge.vBase);
        cell.setAttribute('vBase', parseFloat(vBase));
        const displayUnits = net.options.slackbus.displayUnits;
        cell.setAttribute('displayUnits', displayUnits);

        const transformer = linkjs.LinkFactory(name);
        const hasZeroSequence = cell.getAttribute('hasZ0', '').toLowerCase() === 'true';

        // Calculate actual zm from input values and convert to per-unit
        Un1 = parseFloat(cell.getAttribute('Un1', ''));
        Un2 = parseFloat(cell.getAttribute('Un2', ''));
        const Un = Math.min(Un1, Un2);
        const S = parseFloat(cell.getAttribute('S', ''));
        const zk = (parseFloat(cell.getAttribute('ek', '') / 100) * Un ** 2) / S;
        const angle = Math.atan(parseFloat(cell.getAttribute('X--R', '')));
        const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
        const xt = (Math.sin(angle) * zk) / (Un ** 2 / S);

        const Un2kV = net.options.slackbus.displayUnits === 1 ? Un2 : Un2 / 1000;
        const cmax = Un2kV <= 1.0 ? 1.05 : 1.1;
        const kt = (0.95 * cmax) / (1 + 0.6 * xt);

        let Ztk = sdkUtils.anyToComplex(strZm);
        Ztk = mathjs.multiply(kt, Ztk);

        transformer
            .setId(cell.getId())
            .setFrom(fromEdge.node)
            .setTo(toEdge.node)
            .setBase(net.sBase, vBase)
            .setZm(new Complex(sdkUtils.convertToPerUnit(vBase, net.sBase, Ztk)))
            .setKt(kt);

        // Get variables from connected edges
        const toEdgevBase = parseFloat(toEdge.vBase);
        const fromEdgevBase = parseFloat(fromEdge.vBase);
        const toEdgeIsUn2 = sdkUtils.withinTolerance(Un2, toEdgevBase, 10);

        // Handle zero impedance
        if (hasZeroSequence) {
            transformer.setZ0(transformer.zm).setFromZeroSeq(0);
            transformer.setToZeroSeq(toEdgeIsUn2 ? toEdge.node : fromEdge.node);
        } else {
            transformer.setZ0(`${linkjs.complex.re}+${linkjs.complex.im}i`);
        }

        // Error check - Un2 must be within 10% of edge.vBase
        let voltageMismatch = false;
        if (toEdgeIsUn2) {
            voltageMismatch = !sdkUtils.withinTolerance(Un2, toEdgevBase, 10) || !sdkUtils.withinTolerance(Un1, fromEdgevBase, 10);
        } else if (!toEdgeIsUn2) {
            voltageMismatch = !sdkUtils.withinTolerance(Un2, fromEdgevBase, 10) || !sdkUtils.withinTolerance(Un1, toEdgevBase, 10);
        }
        voltageMismatch && net.addMessage('[checkGraphData]', `Transformer "${name}" U1/U2 must be within +/- 10% of connected nodes`, null, 'error');

        // Add transformer to network
        net.addLinkImpedance(transformer);

        // Error check - Un1 must not equal Un2
        if (Un1 === Un2) {
            net.addMessage('[checkGraphData]', `Transformer "${name}" cannot have Un1 = Un2`, null, 'error');
        }

        // VERBOSE logging
        sdkUtils.debugLog(VERBOSE, 'Added transformer', name);
        sdkUtils.debugLog(VERBOSE, '... vBase', vBase);
        sdkUtils.debugLog(VERBOSE, '... displayUnits', net.options.slackbus.displayUnits);
        sdkUtils.debugLog(VERBOSE, '... Un2kV', Un2kV);
        sdkUtils.debugLog(VERBOSE, '... cmax', cmax);
        sdkUtils.debugLog(VERBOSE, '... actual impedance', strZm);
        sdkUtils.debugLog(VERBOSE, '... kt', kt);
        sdkUtils.debugLog(VERBOSE, '... corrected impedance', Ztk.toString());
        sdkUtils.debugLog(VERBOSE, '... per-unit impedance', transformer.zm.toString());
        sdkUtils.debugLog(VERBOSE, '... from', fromEdge.node);
        sdkUtils.debugLog(VERBOSE, '... to', toEdge.node);
    } catch (error) {
        // Catch other errors and log them
        net.addMessage(
            '[checkGraphData]',
            `Transformer "${name}" has one or more invalid connections or is missing some configuration values`,
            null,
            'error'
        );
        console.error(error);
    }

    // Error check - Edge must be a node
    if (!utils.hasEdgeToType(cell, 'node')) {
        net.addMessage('[checkGraphData]', `${name} must be connected to a node`, null, 'error');
    }
}

module.exports.addTransformer = addTransformer;
