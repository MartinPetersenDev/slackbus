/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
import { AboutDialog } from './dialogs/aboutDialog';
import { getOptions, OptionsDialog } from './dialogs/optionsDialog';
import { DataDialog } from './dialogs/dataDialog';

('use strict');

// Import
const networkjs = require('../sdk/network');
const api = require('./callAPI.js');
const cg = require('./checkGraph');
const gtn = require('./graphToNetwork');
const utils = require('./utils');
const report = require('../report/report');

// Config
const iconUrl = 'url(/app/slackbus/favicon-16x16.png)';
const libUrl = '/app/slackbus/slackbusnet-lib.xml';

Draw.loadPlugin(function (ui) {
    // Create empty network
    let network = new networkjs.Network('Network');
    network.options.calcIk1 = true;
    network.options.calcIk2 = true;
    network.options.calcIk3 = true;
    network.options.calcIk2EE = true;
    // network.options.cMax = 1.05;
    // network.options.cMin = 0.95;
    network.options.calcLoadflow = true;
    network.options.maxLoadflowIterations = 2500;
    network.options.gsAccelerationFactor = 1.4;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;
    network.sBase = 100; // Fixed sBase = 100 MVA

    // Add plugin specific properties to network object
    network.options.slackbus = {
        voltages: [],
        calcIsc: false,
        calcLoadflow: false,
        showDescription: false,
        showVoltage: false,
        showAngle: false,
        showShortcircuit: false,
        showPower: false,
        showCurrent: false,
        showLosses: false,
        maxVoltageWarning: 10,
        displayUnits: 1,
    };

    // Load SlackBus library
    mxUtils.get(
        libUrl,
        function (req) {
            if (req.getStatus() >= 200 && req.getStatus() <= 299) {
                ui.spinner.stop();

                try {
                    ui.loadLibrary(new UrlLibrary(this, req.getText(), libUrl));
                } catch (e) {
                    ui.handleError(e, mxResources.get('errorLoadingFile'));
                }
            } else {
                ui.spinner.stop();
                ui.handleError(null, mxResources.get('errorLoadingFile'));
            }
        },
        function () {
            ui.spinner.stop();
            ui.handleError(null, mxResources.get('errorLoadingFile'));
        }
    );

    // Sidebar is null in lightbox
    if (ui.sidebar != null) {
        const graph = ui.editor.graph;

        // Adds resource for action
        mxResources.parse('slackbusCalculate=Calculate network');
        mxResources.parse('slackbusOptions=Project settings');
        mxResources.parse('slackbusAbout=About SlackBUS');
        mxResources.parse('slackbusReport=Generate report');

        // Adds calculate action
        ui.actions.addAction('slackbusCalculate', async function () {
            if (getOptions(ui, network) === 0) {
                gtn.graphToNetwork(graph, network);
                if (!cg.graphHasErrors(network)) {
                    network = await api.calculate(graph, network, ui);
                }
            }
        });

        // Adds report action
        ui.actions.addAction('slackbusReport', async function () {
            if (getOptions(ui, network) === 0) {
                gtn.graphToNetwork(graph, network);
                if (!cg.graphHasErrors(network)) {
                    network = await api.calculate(graph, network, ui);
                    const reportUrl = window.URL.createObjectURL(new Blob([report.HTMLReport(graph, network)], { type: 'text/html' }));
                    window.open(reportUrl, '_blank');
                }
            }
        });

        // Adds options dialog action
        ui.actions.addAction('slackbusOptions', function () {
            OptionsDialog(ui);
        });

        // Adds options dialog about
        ui.actions.addAction('slackbusAbout', function () {
            AboutDialog(ui);
        });

        // Adds menu
        if (uiTheme === 'min') {
            var theMenu = ui.menus.get('diagram');
            var oldMenu = theMenu.funct;

            theMenu.funct = function (menu, parent) {
                ui.menus.addMenuItems(menu, ['slackbusOptions'], parent);
                ui.menus.addMenuItems(menu, ['slackbusCalculate'], parent);
                ui.menus.addMenuItems(menu, ['slackbusReport'], parent);
                menu.addSeparator();
                ui.menus.addMenuItems(menu, ['slackbusAbout'], parent);
                menu.addSeparator();
                oldMenu.apply(this, arguments);
            };
        } else {
            ui.menubar.addMenu('SlackBUS', function (menu, parent) {
                ui.menus.addMenuItem(menu, 'slackbusOptions');
                ui.menus.addMenuItem(menu, 'slackbusCalculate');
                ui.menus.addMenuItems(menu, ['slackbusReport'], parent);
                menu.addSeparator();
                ui.menus.addMenuItems(menu, ['slackbusAbout'], parent);
            });

            // Reorders menubar
            ui.menubar.container.insertBefore(ui.menubar.container.lastChild, ui.menubar.container.lastChild.previousSibling.previousSibling);
        }

        // Add event listener to open showDataDialog on slackbus elements
        graph.addListener(mxEvent.TAP_AND_HOLD, function (sender, evt) {
            var cell = evt.getProperty('cell');
            if (cell != null) {
                // Open dialog if doubleclick is on slackbus cell
                utils.slackbusVersion(cell) && DataDialog(ui, cell);
            }
        });

        // Uncheck "Connection Arrows"
        graph.addListener(mxEvent.ROOT, function (sender, evt) {
            ui.editor.graph.connectionArrowsEnabled = false;
        });

        // Extend default popup menu with slackbus elements
        const oldFactoryMethod = graph.popupMenuHandler.factoryMethod;
        graph.popupMenuHandler.factoryMethod = function (menu, cell, evt) {
            try {
                if (utils.slackbusVersion(cell)) {
                    menu.addItem('Edit SlackBUS data', null, function () {
                        DataDialog(ui, cell);
                    });
                    menu.addSeparator();
                }
            } catch (error) {
                // no action
            }

            menu.addItem('Project settings', null, function () {
                ui.actions.actions.slackbusOptions.funct();
            });
            menu.addItem('Calculate network', null, function () {
                ui.actions.actions.slackbusCalculate.funct();
            });
            menu.addItem('Generate report', null, function () {
                ui.actions.actions.slackbusReport.funct();
            });
            menu.addSeparator();

            oldFactoryMethod.apply(this, arguments);
        };
    }
});
