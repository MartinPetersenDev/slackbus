/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const utils = require('./utils');
const linkjs = require('../sdk/link');
const sdkUtils = require('../sdk/utils');
const Complex = require('complex.js');

// Config
const VERBOSE = false;

// Version settings
const proVersion = false; // HACK

/**
 * Add source to network
 *
 * @param graph {object} - graph
 * @param cell {object} - cell
 * @param net {object} - network
 */
function addGenerator(graph, cell, net) {
    const name = cell.getAttribute('Name', '');
    let toEdge;

    try {
        // Get node and vBase from connected edges
        toEdge = utils.getvBaseFromNode(graph, cell, 0);

        // Set attributes on diagram objects
        cell.setAttribute('vBase', parseFloat(toEdge.vBase));
        cell.setAttribute('to', parseInt(toEdge.node));
        cell.setAttribute('displayUnits', net.options.slackbus.displayUnits);

        const hasZeroSequence = cell.getAttribute('hasZ0', '').toLowerCase() === 'true';

        // Calculate actual zm from input values and convert to per-unit

        const atrUFactor = cell.getAttribute('Ufactor', '');
        const UFactor = typeof atrUFactor === 'undefined' || atrUFactor === '' ? 1 : parseFloat(atrUFactor);

        // Create generator-source object
        const source = linkjs.GeneratorFactory(name);
        const S = parseFloat(cell.getAttribute('S', '')) / net.sBase;
        const pf = parseFloat(cell.getAttribute('pf', ''));
        source.setId(cell.getId()).setFrom(0).setTo(toEdge.node).setSn(S).setPf(pf).setBase(net.sBase, toEdge.vBase);
        // Calculate impedance
        const xd = parseFloat(cell.getAttribute('x..d', ''));
        const x0 = parseFloat(cell.getAttribute('x0', ''));
        const Un = parseFloat(cell.getAttribute('Un', ''));
        const angle = Math.acos(pf);
        const kg = UFactor / (1 + (xd / 100) * Math.sin(angle));
        const zk = (kg * ((xd / 100) * Un ** 2)) / S / net.sBase;
        const z0 = (kg * ((x0 / 100) * Un ** 2)) / S / net.sBase;

        const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;

        // Handle zero impedance
        const strZ0 = hasZeroSequence ? `${Math.cos(angle) * z0}+${Math.sin(angle) * z0}i` : `${linkjs.complex.re}+${linkjs.complex.im}i`;

        source
            .setZm(new Complex(sdkUtils.convertToPerUnit(toEdge.vBase, net.sBase, strZm)))
            .setZ0(new Complex(sdkUtils.convertToPerUnit(toEdge.vBase, net.sBase, strZ0)));

        // Error check before adding source - Only one source/generator per nodes
        const busIdx = net.nodes.findIndex((bus) => bus.node === toEdge.node);

        if (net.nodes[busIdx].Pgen > 0) {
            net.addMessage('[checkGraphData]', `Node "${net.nodes[busIdx].name}" cannot have more than one source connected`, null, 'error');
        }

        net.nodes[busIdx].setUFactor(UFactor);
        proVersion && net.nodes[busIdx].addGeneratorPower(source); // only in pro version

        // Add source to network
        net.addLinkImpedance(source);

        // VERBOSE logging
        sdkUtils.debugLog(VERBOSE, 'Added source', name);
        sdkUtils.debugLog(VERBOSE, '... actual impedance', strZm);
        sdkUtils.debugLog(VERBOSE, '... per-unit impedance', source.zm.toString());
        sdkUtils.debugLog(VERBOSE, '... z0 impedance', strZ0);
        sdkUtils.debugLog(VERBOSE, '... to', toEdge.node);
    } catch (error) {
        // Catch other errors and log them
        net.addMessage(
            '[checkGraphData]',
            `Generator "${name}" has one or more invalid connections or is missing some configuration values`,
            null,
            'error'
        );
    }

    // Error check - Edge must be a node
    if (!utils.hasEdgeToType(cell, 'node')) {
        net.addMessage('[checkGraphData]', `${name} must be connected to a node`, null, 'error');
    }
}

module.exports.addGenerator = addGenerator;
