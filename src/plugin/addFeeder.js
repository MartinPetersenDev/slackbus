/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const utils = require('./utils');
const linkjs = require('../sdk/link');
const sdkUtils = require('../sdk/utils');
const Complex = require('complex.js');

// Config
const VERBOSE = false;

/**
 * Add source to network
 *
 * @param graph {object} - graph
 * @param cell {object} - cell
 * @param net {object} - network
 */
function addFeeder(graph, cell, net) {
    const name = cell.getAttribute('Name', '');
    let toEdge;

    try {
        // Get node and vBase from connected edges
        toEdge = utils.getvBaseFromNode(graph, cell, 0);

        // Set attributes on diagram objects
        cell.setAttribute('vBase', parseFloat(toEdge.vBase));
        cell.setAttribute('to', parseInt(toEdge.node));
        cell.setAttribute('displayUnits', net.options.slackbus.displayUnits);

        const hasZeroSequence = cell.getAttribute('Ik1', '').toLowerCase().length > 0;

        // Calculate actual zm from input values and convert to per-unit
        const atrUFactor = cell.getAttribute('Ufactor', '');
        const UFactor = typeof atrUFactor === 'undefined' || atrUFactor === '' ? 1 : parseFloat(atrUFactor);

        // Create source object
        const source = linkjs.LinkFactory(name);
        source.setFrom(0).setTo(toEdge.node).setBase(net.sBase, toEdge.vBase).setId(cell.getId());

        // Calculate impedance and handle case of sk/xr or ik3/ik1
        const Un = parseFloat(cell.getAttribute('Un', ''));
        const UnkV = net.options.slackbus.displayUnits === 1 ? Un : Un / 1000;
        const cmax = UnkV <= 1.0 ? 1.05 : 1.1;
        let angle;
        let zk;
        if (cell.getAttribute('CosPhi', '').toString().length > 0) {
            zk = (cmax * Un) / (Math.sqrt(3) * parseFloat(cell.getAttribute('Ik3', '')));
            angle = Math.acos(parseFloat(cell.getAttribute('CosPhi', '')));
        } else {
            zk = (cmax * Un ** 2) / parseFloat(cell.getAttribute('Sk', ''));
            angle = Math.atan(parseFloat(cell.getAttribute('X--R', '')));
        }

        const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;

        // Handle zero impedance
        const strZ0 = hasZeroSequence
            ? `${(Math.sqrt(3) * Un * cmax) / parseFloat(cell.getAttribute('Ik1', '')) - 2 * zk}+0i`
            : `${linkjs.complex.re}+${linkjs.complex.im}i`;

        source
            .setZm(new Complex(sdkUtils.convertToPerUnit(toEdge.vBase, net.sBase, strZm)))
            .setZ0(new Complex(sdkUtils.convertToPerUnit(toEdge.vBase, net.sBase, strZ0)));

        // Error check before adding source - Only one source/generator per node
        const nodeIdx = net.nodes.findIndex((n) => n.node === toEdge.node);

        if (net.nodes[nodeIdx].Pgen > 0) {
            net.addMessage('[checkGraphData]', `Node "${net.nodes[nodeIdx].name}" cannot have more than one source connected`, null, 'error');
        }

        net.nodes[nodeIdx].setUFactor(UFactor);
        // HACK - set a negible small generator power value, to prevent multiple sources on same node
        net.nodes[nodeIdx].Pgen = 0.0000000001;

        // Add source to network
        net.addLinkImpedance(source);

        // VERBOSE logging
        sdkUtils.debugLog(VERBOSE, 'Added source', name);
        sdkUtils.debugLog(VERBOSE, '... vBase', toEdge.vBase);
        sdkUtils.debugLog(VERBOSE, '... cmax', cmax);
        sdkUtils.debugLog(VERBOSE, '... actual impedance', strZm);
        sdkUtils.debugLog(VERBOSE, '... per-unit impedance', source.zm.toString());
        sdkUtils.debugLog(VERBOSE, '... z0 impedance', strZ0);
        sdkUtils.debugLog(VERBOSE, '... to', toEdge.node);
    } catch (error) {
        console.error(error);
        // Catch other errors and log them
        net.addMessage(
            '[checkGraphData]',
            `Feeder "${name}" has one or more invalid connections or is missing some configuration values`,
            null,
            'error'
        );
    }

    // Error check - Edge must be a node
    if (!utils.hasEdgeToType(cell, 'node')) {
        net.addMessage('[checkGraphData]', `${name} must be connected to a node`, null, 'error');
    }
}

module.exports.addFeeder = addFeeder;
