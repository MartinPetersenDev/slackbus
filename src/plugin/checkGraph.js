/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

// Import
const utils = require('./utils');
const sdkUtils = require('../sdk/utils');

/**
 * Return true if network has error messages
 *
 * @returns {boolean}
 */
const graphHasErrors = function (network) {
    const errorMessages = network.getErrorMessages();
    errorMessages.length > 0 && utils.showArrayAsAlert(errorMessages);
    return sdkUtils.networkHasError(network);
};

module.exports.graphHasErrors = graphHasErrors;
