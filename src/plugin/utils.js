/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const sdkUtils = require('../sdk/utils');

// DEBUG
const VERBOSE = true;

/**
 * Returns true, if 'cell' has edge which connects to 'type'
 */
const hasEdgeToType = function (cell, type = '', alert = true) {
    let retVal = false;
    const _type = type.toLowerCase();

    // If cell is a line, both ends must connect to 'type'

    try {
        if (cell.getAttribute('type', '').toLowerCase() === 'line' && cell.source.edges && cell.target.edges) {
            retVal =
                cell.getTerminal(false).getAttribute('type', '').toLowerCase() === _type &&
                cell.getTerminal(true).getAttribute('type', '').toLowerCase() === _type;
        }
        // If cell is any other object, just one end must connect to 'type'
        else if (cell.getAttribute('type', '').toLowerCase() !== 'line' && cell.edges) {
            const thisId = cell.getId();
            cell.edges.forEach((edge) => {
                const otherId1 = edge.getTerminal(false).getId();
                const otherType1 = edge.getTerminal(false).getAttribute('type', '').toLowerCase();
                const otherId2 = edge.getTerminal(true).getId();
                const otherType2 = edge.getTerminal(true).getAttribute('type', '').toLowerCase();
                retVal = retVal || (otherType1 === _type && otherId1 !== thisId) || (otherType2 === _type && otherId2 !== thisId);
            });
        }
    } catch (error) {
        // eslint-disable-next-line no-undef
        alert && mxUtils.alert('Please make sure all elements are connected before trying to calculate network');
    }

    return retVal;
};

/**
 * Returns true if node is directly connected to node, without Slackbus.net elements
 */
const hasNodeToNodeEdge = function (cell) {
    let retVal = false;
    const type = cell.getAttribute('type', '').toLowerCase();

    // If cell is a node, edges with attribute "SlackBUS-version" are ignored
    if (type === 'node' && cell.edges) {
        const thisId = cell.getId();

        cell.edges.forEach((edge) => {
            const otherId1 = edge.getTerminal(false).getId();

            let edgeHasSlackbusVersion = false;
            if (edge.value) {
                edgeHasSlackbusVersion = edge.hasAttribute('SlackBUS-version');
            }

            const otherType1 = edge.getTerminal(false).getAttribute('type', '').toLowerCase();
            const otherId2 = edge.getTerminal(true).getId();
            const otherType2 = edge.getTerminal(true).getAttribute('type', '').toLowerCase();

            retVal =
                retVal ||
                (!edgeHasSlackbusVersion && otherType1 === 'node' && otherId1 !== thisId) ||
                (!edgeHasSlackbusVersion && otherType2 === 'node' && otherId2 !== thisId);
        });
    }

    return retVal;
};

/**
 * Output all network.messages of type as mxUtils.alert
 *
 * @param {object} msgArray
 * @param {string} type
 */
const showArrayAsAlert = function (msgArray, type = 'error') {
    let errorStr = `${msgArray.length} ${type}s where detected:\n\n`;

    let n = 1;
    msgArray.forEach((msg) => {
        errorStr += `(${n}) ${msg}\n`;
        n++;
    });

    // eslint-disable-next-line no-undef
    mxUtils.alert(errorStr);
};

/**
 * Returns object={node, vBase} from cell with edge[edgeNo]
 *
 * @param graph {object} - graph
 * @param cell {object} - cell
 * @param edgeNo {int} - index of edge array
 * @returns {object} - {node, vBase}
 */
function getvBaseFromNode(graph, cell, edgeNo) {
    const edges = graph.getEdges(cell);
    let node;
    let vBase;

    if (edges[edgeNo].getTerminal(false).getId() === cell.getId()) {
        node = parseInt(edges[edgeNo].getTerminal(true).getAttribute('node', ''));
        vBase = parseFloat(edges[edgeNo].getTerminal(true).getAttribute('Un', ''));
    } else {
        node = parseInt(edges[edgeNo].getTerminal(false).getAttribute('node', ''));
        vBase = parseFloat(edges[edgeNo].getTerminal(false).getAttribute('Un', ''));
    }

    return { node, vBase };
}

/**
 * Returns Slackbus version attribute
 */
const slackbusVersion = (cell) => cell.getAttribute('SlackBUS-version', '');

/**
 * Returns cell of slackbus node
 * @param {object} cells - array of cells
 */
function getSlackbus(cells) {
    for (const c of cells) {
        if (c.getAttribute('isSlack', '').toLowerCase() === 'true') return c;
    }
}

/**
 * Add voltage-level to voltages array. Only new voltages will be added. The voltage array is used to select colors for network elements.
 * @param {number} v - Voltage
 * @param {object} network
 */
const addVoltage = function (v, network) {
    if (network.options.slackbus.voltages.indexOf(v) === -1) {
        network.options.slackbus.voltages.push(v.toString());
    }
};

/**
 * If passed a string containing a positive number, replace decimal-point from comma to dot and return a float, else return empty string
 * @param {string} value
 * @return {string}
 */
const cleanUpFloat = function (value) {
    return parseFloat(value) < 0 || isNaN(parseFloat(value)) ? '' : value.toString().replace(/,/g, '.');
};

module.exports.hasEdgeToType = hasEdgeToType;
module.exports.hasNodeToNodeEdge = hasNodeToNodeEdge;
module.exports.showArrayAsAlert = showArrayAsAlert;
module.exports.getvBaseFromNode = getvBaseFromNode;
module.exports.slackbusVersion = slackbusVersion;
module.exports.getSlackbus = getSlackbus;
module.exports.addVoltage = addVoltage;
module.exports.cleanUpFloat = cleanUpFloat;
