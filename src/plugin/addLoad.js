/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const utils = require('./utils');
const linkjs = require('../sdk/link');
const sdkUtils = require('../sdk/utils');

// Config
const VERBOSE = true;

/**
 * Add load to network
 *
 * @param cell {object} - cell
 * @param net {object} - network
 */
function addLoad(graph, cell, net) {
    const name = cell.getAttribute('Name', '');

    try {
        // Get node and vBase from connected edges
        const toEdge = utils.getvBaseFromNode(graph, cell, 0);

        // Set attributes on diagram objects
        cell.setAttribute('to', parseInt(toEdge.node));
        cell.setAttribute('vBase', parseFloat(toEdge.vBase));
        cell.setAttribute('displayUnits', net.options.slackbus.displayUnits);

        const atrP = cell.getAttribute('P', '');
        const P = typeof atrP === 'undefined' || atrP === '' ? parseFloat(0) : parseFloat(atrP);
        const atrQ = cell.getAttribute('Q', '');
        const Q = typeof atrQ === 'undefined' || atrQ === '' ? parseFloat(0) : parseFloat(atrQ);

        const load = linkjs.LoadFactory(name);

        load.setId(cell.getId())
            .setP(P / net.sBase)
            .setQ(Q / net.sBase);
        const i = net.nodes.findIndex((bus) => bus.node.toString() === toEdge.node.toString());

        // Add load  to network
        net.nodes[i].addLoadPower(load);

        // VERBOSE logging
        sdkUtils.debugLog(VERBOSE, 'Added load', name);
        sdkUtils.debugLog(VERBOSE, '... P', P);
        sdkUtils.debugLog(VERBOSE, '... Q', Q);
        sdkUtils.debugLog(VERBOSE, '... to', toEdge.node);
    } catch (error) {
        // Catch other errors and log them
        net.addMessage(
            '[checkGraphData]',
            `Load "${name}" has one or more invalid connections or is missing some configuration values`,
            null,
            'error'
        );
    }

    // Error check - Edge must be a node
    if (!utils.hasEdgeToType(cell, 'node')) {
        net.addMessage('[checkGraphData]', `${name} must be connected to a node`, null, 'error');
    }
}

module.exports.addLoad = addLoad;
