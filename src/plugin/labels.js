/* eslint-disable new-cap */
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

const stylesjs = require('./styles.js');
const sdkUtils = require('../sdk/utils');

// Config DEBUG
const VERBOSE = false;

/**
 * Updates label-info on graph
 * @param {*} graph
 * @param {*} network
 */
const updateGraphLabels = function (g, network) {
    sdkUtils.debugLog(VERBOSE, 'Updating graph labels', '');

    const warningIconUrl = '/app/slackbus/img/warning.svg';

    // Set display units
    const voltUnit = network.options.slackbus.displayUnits === 1 ? 'kV' : 'V';
    const wattUnit = network.options.slackbus.displayUnits === 1 ? 'MW' : 'kW';
    const vaUnit = network.options.slackbus.displayUnits === 1 ? 'MVA' : 'kVA';
    const varUnit = network.options.slackbus.displayUnits === 1 ? 'MVAr' : 'kVAr';
    const lengthUnit = network.options.slackbus.displayUnits === 1 ? 'km' : 'm';
    const roundToPower = network.options.slackbus.displayUnits === 1 ? 3 : 1;
    const roundToVoltage = network.options.slackbus.displayUnits === 1 ? 3 : 1;
    const roundToShortCircuit = 3;

    const defaultParent = g.getDefaultParent(); // Returns cell
    const cells = g.getChildCells(defaultParent);

    // Check if any node is singlePhase - this modifies behaviour of feeder label
    let hasSinglePhase;
    cells.forEach((c) => {
        const isNode = c.getAttribute('type', '').toLowerCase() === 'node';
        hasSinglePhase = isNode ? c.getAttribute('singlePhase', '') === 'true' : hasSinglePhase;
    });

    const results = sdkUtils.getResultsActualUnits(network);

    cells.forEach((c) => {
        const name = c.getAttribute('Name', '');
        const type = c.getAttribute('type', '').toLowerCase();

        // Nodes
        if (type === 'node') {
            const id = c.getId();
            const index = results.nodes.findIndex((bus) => bus.id === id);
            const isSinglePhase = c.getAttribute('singlePhase', '') === 'true';

            const P = results.nodes[index].lf.Pload;
            const Q = results.nodes[index].lf.Qload;
            const _P = isSinglePhase ? (P / 3).toFixed(roundToPower) : P.toFixed(roundToPower);
            const _Q = isSinglePhase ? (Q / 3).toFixed(roundToPower) : Q.toFixed(roundToPower);
            const strLoad = `${_P} ${wattUnit} / ${_Q} ${varUnit}`;

            const Ull = results.nodes[index].lf.U;
            const Uln = results.nodes[index].lf.Uln;
            const ang = results.nodes[index].lf.angle.toFixed(1);

            const ik3 = results.nodes[index].sc.ik3.toFixed(roundToShortCircuit);
            const ik2 = results.nodes[index].sc.ik2.toFixed(roundToShortCircuit);
            const ik1 = results.nodes[index].sc.ik1.toFixed(roundToShortCircuit);
            const ikmin = results.nodes[index].sc.ikmin.toFixed(roundToShortCircuit);
            const ikmax = results.nodes[index].sc.ikmax.toFixed(roundToShortCircuit);

            let label = `${name} `;

            if (network.options.slackbus.showVoltage && !isSinglePhase) {
                label = label + `\nU = ${Ull.toFixed(roundToVoltage)} / ${Uln.toFixed(roundToVoltage)} ${voltUnit}`;
            } else if (network.options.slackbus.showVoltage && isSinglePhase) {
                label = label + `\nU = - / ${Uln.toFixed(roundToVoltage)} ${voltUnit}`;
            }

            if (P > 0 || Q > 0) {
                label = label + `\nS = ${strLoad}`;
            }
            if (network.options.slackbus.showShortcircuit) {
                label = isSinglePhase ? label : label + `\nIk''3 = ${ik3} kA`;
                label = isSinglePhase ? label : label + `\nIk''2 = ${ik2} kA`;
                label = label + `\nIk''1 = ${ik1} kA`;
            }
            if (network.options.slackbus.showIkMinMax) {
                label = isSinglePhase ? label : label + `\nIk max = ${ikmax} kA`;
                label = label + `\nIk min = ${ikmin} kA`;
            }

            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);

            // Add warning overlay, if voltage difference between U and Un is too large
            const voltageDiff = (Math.abs(results.nodes[index].lf.U - results.nodes[index].Un) / results.nodes[index].Un) * 100;

            if (voltageDiff > network.options.slackbus.maxVoltageWarning || isNaN(results.nodes[index].lf.U)) {
                sdkUtils.debugLog(
                    VERBOSE,
                    `... ${name} voltage diff. ${voltageDiff.toFixed(1)}% > warning threshold`,
                    network.options.slackbus.maxVoltageWarning.toFixed(1)
                );

                const nodeOverlays = g.getCellOverlays(c);

                if (nodeOverlays == null) {
                    // Creates a new overlay with an image
                    const netNodeOverlay = new mxCellOverlay(
                        new mxImage(warningIconUrl, 41, 36),
                        null,
                        mxConstants.ALIGN_RIGHT,
                        mxConstants.ALIGN_TOP,
                        null,
                        'default'
                    );
                    // Sets the overlay for the cell in the graph
                    g.addCellOverlay(c, netNodeOverlay);
                }
            } else {
                g.removeCellOverlays(c);
            }
        }

        // Feeder
        if (type === 'feeder') {
            const index = results.nodes.findIndex((bus) => bus.node.toString() === c.getAttribute('to', '').toString());
            const Pgen = results.nodes[index].lf.Pgen;
            const Qgen = results.nodes[index].lf.Qgen;

            /** * SHOULD BE CALCULATED IN RESULTS OBJ */
            const I = (Math.sqrt(Pgen ** 2 + Qgen ** 2) / results.nodes[0].vBase / Math.sqrt(3)) * 1000;

            let label = `${name}\nUn = ${c.getAttribute('Un', '')} ${voltUnit}`;

            if (network.options.slackbus.showCurrent) {
                label = label + `\nIb = ${I.toFixed(1)} A`;
            }

            if (network.options.slackbus.showPower && !hasSinglePhase) {
                label = label + `\nS = ${Pgen.toFixed(roundToPower)} ${wattUnit} / ${Qgen.toFixed(roundToPower)} ${varUnit}`;
            }

            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);
        }

        // Generator
        if (type === 'generator') {
            const index = network.nodes.findIndex((bus) => bus.node.toString() === c.getAttribute('to', '').toString());
            const S = parseFloat(c.getAttribute('S', ''));

            const label = `${name}\nUn = ${c.getAttribute('Un', '')} ${voltUnit}\nSn = ${S} ${vaUnit}`;

            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);
        }

        // Lines
        if (type === 'line') {
            const id = c.getId();
            const index = results.links.findIndex((link) => link.id === id);
            const Ib = results.links[index].Ib;
            const dU = results.links[index].dU.toFixed(1);

            const descr = c.hasAttribute('Description') ? c.getAttribute('Description') : '';
            const length = c.hasAttribute('Length') ? parseFloat(c.getAttribute('Length')) : 0;

            let label = `${name}`;
            if (network.options.slackbus.showDescription) {
                label = label + `\n${descr}\nLength = ${length} ${lengthUnit}`;
            }

            if (network.options.slackbus.showCurrent) {
                label = label + `\nIb = ${Ib.toFixed(1)} A`;
            }

            if (network.options.slackbus.showLosses) {
                label = label + `\ndU = ${dU} %`;
            }

            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);

            // Overlay at max voltage drop
            if (results.links[index].dU > network.options.slackbus.maxVoltageWarning) {
                sdkUtils.debugLog(
                    VERBOSE,
                    `... ${name} voltage drop ${results.links[index].dU.toFixed(1)}% > warning threshold`,
                    network.options.slackbus.maxVoltageWarning.toFixed(1)
                );

                const lineOverlays = g.getCellOverlays(c);

                if (lineOverlays == null) {
                    // Creates a new overlay with an image
                    const netlineOverlays = new mxCellOverlay(
                        new mxImage(warningIconUrl, 41, 36),
                        null,
                        mxConstants.ALIGN_RIGHT,
                        mxConstants.ALIGN_BOTTOM,
                        null,
                        'default'
                    );
                    // Sets the overlay for the cell in the graph
                    g.addCellOverlay(c, netlineOverlays);
                }
            } else {
                g.removeCellOverlays(c);
            }
        }

        // Transformers
        if (type === 'transformer') {
            const id = c.getId();
            const index = results.links.findIndex((link) => link.id === id);
            const P = results.links[index].P;
            const Q = results.links[index].Q;
            const S = Math.sqrt(P ** 2 + Q ** 2);
            const I = results.links[index].Ib;
            const Sn = parseFloat(c.getAttribute('S', ''));

            let label = `${name} \nUn = ${c.getAttribute('Un1', '')} / ${c.getAttribute('Un2', '')} ${voltUnit}\nSn = ${Sn} ${vaUnit}`;

            if (network.options.slackbus.showCurrent) {
                label = label + `\nIb = ${I.toFixed(1)} A`;
            }

            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);

            // Add warning overlay, if S > Sn
            if (S > Sn || isNaN(P)) {
                const transformerOverlays = g.getCellOverlays(c);

                if (transformerOverlays == null) {
                    // Creates a new overlay with an image
                    const newTransformerOverlay = new mxCellOverlay(
                        new mxImage(warningIconUrl, 41, 36),
                        null,
                        mxConstants.ALIGN_RIGHT,
                        mxConstants.ALIGN_TOP,
                        null,
                        'default'
                    );
                    // Sets the overlay for the cell in the graph
                    g.addCellOverlay(c, newTransformerOverlay);
                }
            } else {
                g.removeCellOverlays(c);
            }
        }

        // Loads
        if (type === 'load') {
            let label = `${name} `;
            label = label + `\nSn = ${c.getAttribute('P', '')} ${wattUnit} / ${c.getAttribute('Q', '')} ${varUnit} `;
            g.cellLabelChanged(c, label, false);
            stylesjs.updateLabelStyle(network, g, c);
        }
    });
};

/**
 * Refresh graph labels
 */
const refreshLabels = function (g) {
    sdkUtils.debugLog(VERBOSE, 'Refreshing graph labels', '');

    const defaultParent = g.getDefaultParent(); // Returns cell
    const cells = g.getChildCells(defaultParent);

    // update name on all labels
    cells.forEach((c) => {
        if (c.getAttribute('SlackBUS-version', '') >= 1.0) {
            const name = c.getAttribute('Name', '');
            let label = c.getAttribute('label', '');

            // replace first line with updated name
            const strArray = label.split(/\r?\n/);
            strArray[0] = name;
            label = strArray.join('\n');

            // Update graph with new label
            g.cellLabelChanged(c, label, false);
        }
    });
};

module.exports.updateGraphLabels = updateGraphLabels;
module.exports.refreshLabels = refreshLabels;
