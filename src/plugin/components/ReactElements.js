/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

import '../dialogs/dialogs.css';
import React from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

/**
 * React jsx for input dialog
 */
const DialogInput = (props) => {
    const disabled = props.disabled;

    return (
        <div className="dialog-input mb-3">
            {disabled && <label className="dialog-input mb-3 dialog-label disabled-text">{props.label}</label>}
            {!disabled && <label className="dialog-input mb-3 dialog-label">{props.label}</label>}
            <InputGroup className="mb-3" size="sm">
                {disabled && <FormControl id={props.id} data-cy={props.datacy} onChange={props.handleDisabled} disabled />}
                {!disabled && <FormControl id={props.id} data-cy={props.datacy} onChange={props.handleDisabled} />}
                <InputGroup.Append>
                    {disabled && <InputGroup.Text className="disabled-text">{props.append}</InputGroup.Text>}
                    {!disabled && <InputGroup.Text>{props.append}</InputGroup.Text>}
                </InputGroup.Append>
            </InputGroup>
        </div>
    );
};

/**
 * React jsx for input checkbox
 */
const DialogCheckbox = (props) => {
    return (
        <div className="dialog-input mb-3">
            <label className="dialog-input mb-3 dialog-label">{props.label}</label>
            <InputGroup className="mb-3 input-group-checkbox" size="sm">
                <InputGroup.Checkbox id={props.id} data-cy={props.datacy} />
            </InputGroup>
        </div>
    );
};

export { DialogInput, DialogCheckbox };
