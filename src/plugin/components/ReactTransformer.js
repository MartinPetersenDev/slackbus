/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

// {props.ih > props.maxIh && <Image src="/app/slackbus/img/transformer-model.svg" fluid></Image>}

const ReactTransformer = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Rated voltage Un1" append={props.units.voltUnit} id="un1" datacy="numeric-input" />
                    <DialogInput label="Rated voltage Un2" append={props.units.voltUnit} id="un2" datacy="numeric-input" />
                    <DialogInput label="Rated power S" append={props.units.vaUnit} id="s" datacy="numeric-input" />
                    <DialogInput label="Shortcircuit voltage ek" append="%" id="ek" datacy="numeric-input" />
                    <DialogInput label="X/R ratio" append=" " id="xr" datacy="numeric-input" />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Connection to ground / PE</Card.Header>
                    <br />
                    <DialogCheckbox label="U2 connected to ground / PE" id="hasz0" />
                </Card.Body>
            </Card>
        </div>
    );
};

export default ReactTransformer;
