/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

// Version
const proVersion = false;

//  {props.ih > props.maxIh && <Image src="/app/slackbus/img/generator-model.svg" fluid></Image>}
const ReactGenerator = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Rated voltage Un" append={props.units.voltUnit} id="un" datacy="numeric-input" />
                    {proVersion && <DialogInput label="Actual voltage factor" append="%" id="ufactor" datacy="percentage-input" />}
                    <DialogInput label="Rated power S" append={props.units.vaUnit} id="s" datacy="numeric-input" />
                    <DialogInput label="Actual powerfactor pf" append=" " id="pf" datacy="numeric-input" />
                    <DialogInput label="Subtransient reactance x''d" append="%" id="xd" datacy="numeric-input" />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Connection to ground / PE</Card.Header>
                    <br />
                    <DialogCheckbox label="Connected to ground / PE" id="hasz0" />
                    <DialogInput label="Reactance x0" append="%" id="x0" datacy="numeric-input" />
                </Card.Body>
            </Card>
            <br />
            <p>Generator load-flow is not available in the free version</p>
            <br />
        </div>
    );
};

export default ReactGenerator;
