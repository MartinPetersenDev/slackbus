/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Image from 'react-bootstrap/Image';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import ReactFeeder from '../components/ReactFeeder';
import ReactTransformer from '../components/ReactTransformer';
import ReactGenerator from '../components/ReactGenerator';
import ReactLine from '../components/ReactLine';
import ReactLoad from '../components/ReactLoad';
import ReactNode from '../components/ReactNode';

// Config
const DEBUG = false;

class ReactDataDialog extends React.Component {

    state = {
        skDisabled: false,
        ikDisabled: false,
    };

    handleFeederDisabled = (e) => {
        const skLength = document.getElementById('sk').value.length + document.getElementById('xr').value.length;
        const iklength = document.getElementById('ik3').value.length + document.getElementById('cosphi').value.length + document.getElementById('ik1').value.length;
        this.setState(() => ({
            skDisabled: iklength > 0,
            ikDisabled: skLength > 0
        }));
    };

    render = () => {
        // Set inner height
        const ih = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        const maxIh = 600;

        let dialogJsx;

        const type = this.props.type;
        const units = this.props.units;

        if (type === 'transformer') {
            dialogJsx = <ReactTransformer ih={ih} maxIh={maxIh} units={units} />;
        } else if (type === 'feeder') {
            dialogJsx = <ReactFeeder ih={ih} maxIh={maxIh} units={units} handleDisabled={this.handleFeederDisabled} skDisabled={this.state.skDisabled} ikDisabled={this.state.ikDisabled} />;
        } else if (type === 'generator') {
            dialogJsx = <ReactGenerator ih={ih} maxIh={maxIh} units={units} />;
        } else if (type === 'line') {
            dialogJsx = <ReactLine ih={ih} maxIh={maxIh} units={units} />;
        } else if (type === 'load') {
            dialogJsx = <ReactLoad ih={ih} maxIh={maxIh} units={units} />;
        } else if (type === 'node') {
            dialogJsx = <ReactNode ih={ih} maxIh={maxIh} units={units} />;
        }

        return (
            <span>
                <Container>
                    <Row>
                        <Image src="/app/slackbus/img/logo.png"></Image>
                    </Row>
                    <Row>{DEBUG && <p>Datadialog for ID={this.props.id}</p>}</Row>
                    <Row>{dialogJsx}</Row>
                </Container>
            </span>
        );
    }
}

export default ReactDataDialog;
