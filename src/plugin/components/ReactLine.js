/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

// {props.ih > props.maxIh && <Image src="/app/slackbus/img/line-model.svg" fluid></Image>}
// <DialogCheckbox label="Single-phase cable L-N" id="singlephase" />
const ReactLine = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Description" append=" " id="description" />
                    <DialogInput label="Length" append="km" id="length" append={props.units.lengthUnit} datacy="numeric-input" />
                    <DialogInput label="Resistance Rm" append="Ω/km" id="rm" datacy="numeric-input" />
                    <DialogInput label="Reactance Xm" append="Ω/km" id="xm" datacy="numeric-input" />
                    <DialogInput label="Ambient temperature" append="°C" id="ambtemp" datacy="numeric-input" />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Advanced</Card.Header>
                    <br />
                    <DialogInput label="Max. fault temperature" append="°C" id="maxtemp" datacy="numeric-input" />
                    <DialogInput label="Zero-seq. resistance R0" append="Ω/km" id="r0" datacy="numeric-input" />
                    <DialogInput label="Zero-seq. reactance X0" append="Ω/km" id="x0" datacy="numeric-input" />
                    <DialogInput label="Conductance G" append="S/km" id="g" datacy="numeric-input" />
                    <DialogInput label="Susceptance B" append="S/km" id="b" datacy="numeric-input" />
                </Card.Body>
            </Card>
        </div>
    );
};

export default ReactLine;
