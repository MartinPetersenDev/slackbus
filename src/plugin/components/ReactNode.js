/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

const ReactNode = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Rated voltage Un" append={props.units.voltUnit} id="un" datacy="numeric-input" />
                    <DialogCheckbox label="Slackbus" append=" " id="isslack" />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Load Data</Card.Header>
                    <br />
                    <DialogInput
                        label="Active power P"
                        append={props.units.wattUnit}
                        id="p"
                        datacy="numeric-input"
                        disabled={props.skDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                    <DialogInput label="Reactive power Q" append={props.units.varUnit} id="q" datacy="numeric-input" />
                    <DialogCheckbox label="Node is single-phase L-N" id="singlephase" />
                </Card.Body>
            </Card>
        </div>
    );
};

export default ReactNode;
