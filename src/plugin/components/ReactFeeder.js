/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

// <DialogInput label="Actual voltage factor" append="%" id="ufactor" datacy="percentage-input" />
const ReactFeeder = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Rated voltage Un" append={props.units.voltUnit} id="un" datacy="numeric-input" />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Shortcircuit Current</Card.Header>
                    <br />
                    <DialogInput
                        label="Three-phase shortcircuit Ik3"
                        append="kA"
                        id="ik3"
                        datacy="numeric-input-disable"
                        disabled={props.ikDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                    <DialogInput
                        label="cos phi"
                        append=" "
                        id="cosphi"
                        datacy="numeric-input-disable"
                        disabled={props.ikDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                    <DialogInput
                        label="Single-phase shortcircuit Ik1"
                        append="kA"
                        id="ik1"
                        datacy="numeric-input-disable"
                        disabled={props.ikDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                </Card.Body>
            </Card>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Shortcircuit Power</Card.Header>
                    <br />
                    <DialogInput
                        label="Sk"
                        append={props.units.vaUnit}
                        id="sk"
                        datacy="numeric-input-disable"
                        disabled={props.skDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                    <DialogInput
                        label="X/R ratio"
                        append=" "
                        id="xr"
                        datacy="numeric-input-disable"
                        disabled={props.skDisabled}
                        handleDisabled={props.handleDisabled}
                    />
                </Card.Body>
            </Card>
        </div>
    );
};

export default ReactFeeder;
