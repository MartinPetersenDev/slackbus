/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Card from 'react-bootstrap/Card';
import { DialogInput, DialogCheckbox } from './ReactElements.js';

//  {props.ih > props.maxIh && <Image src="/app/slackbus/img/load-model.svg" fluid></Image>}
const ReactLoad = (props) => {
    return (
        <div>
            <br />
            <Card style={{ width: '95%' }}>
                <Card.Body>
                    <Card.Header>Basic Data</Card.Header>
                    <br />
                    <DialogInput label="Name" append=" " id="name" />
                    <DialogInput label="Active power P" append={props.units.wattUnit} id="p" datacy="numeric-input" />
                    <DialogInput label="Reactive power Q" append={props.units.varUnit} id="q" datacy="numeric-input" />
                </Card.Body>
            </Card>
        </div>
    );
};

export default ReactLoad;
