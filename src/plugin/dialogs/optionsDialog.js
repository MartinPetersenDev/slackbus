/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

import './dialogs.css';
import React from 'react';
import ReactDOM from 'react-dom';
import Image from 'react-bootstrap/Image';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import { DialogInput, DialogCheckbox } from '../components/ReactElements.js';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
const apiUtils = require('../../sdk/utils');
const utils = require('../utils');

// Global variables
const maxVoltageDiff = 5;
const convergenceLimit = 0.0000001;
const maxIterations = 2500;
const cmax = 1.05;
const cmin = 0.95;
const VERBOSE = false;

/**
 * Write options to graph
 * */
const writeOptions = (cell) => {
    try {
        cell.setAttribute('calcIsc', document.getElementById('calc_isc').checked ? 'true' : 'false');
        cell.setAttribute('showShortcircuit', document.getElementById('show_shortcircuit').checked ? 'true' : 'false');
        cell.setAttribute('show_ik_minmax', document.getElementById('show_ik_minmax').checked ? 'true' : 'false');
        cell.setAttribute('calcLoadflow', document.getElementById('calc_loadflow').checked ? 'true' : 'false');
        cell.setAttribute('showVoltage', document.getElementById('show_voltage').checked ? 'true' : 'false');
        cell.setAttribute('showCurrent', document.getElementById('show_current').checked ? 'true' : 'false');
        cell.setAttribute('showLosses', document.getElementById('show_losses').checked ? 'true' : 'false');
        cell.setAttribute('maxVoltageWarning', utils.cleanUpFloat(document.getElementById('max_voltage_warning').value));
        cell.setAttribute('maxLoadflowIterations', utils.cleanUpFloat(document.getElementById('max_loadflow_iterations').value));
        cell.setAttribute('convergenceLimit', utils.cleanUpFloat(document.getElementById('convergence_limit').value));
        cell.setAttribute('showDescription', document.getElementById('show_descriptions').checked ? 'true' : 'false');
        cell.setAttribute('projectName', document.getElementById('project_name').value);

        if (document.getElementById('unit_mw').checked) {
            cell.setAttribute('displayUnits', 1);
        } else if (document.getElementById('unit_kw').checked) {
            cell.setAttribute('displayUnits', 2);
        } else {
            /* no default - the user must choose one! */
        }
    } catch (e) {
        mxUtils.alert(e);
    }
};

/**
 * Read options from graph
 * */
const readOptions = (cell) => {
    try {
        document.getElementById('calc_isc').checked = cell.hasAttribute('calcIsc') ? cell.getAttribute('calcIsc', '') === 'true' : true;
        document.getElementById('show_shortcircuit').checked = cell.hasAttribute('showShortcircuit')
            ? cell.getAttribute('showShortcircuit', '') === 'true'
            : true;

        document.getElementById('show_ik_minmax').checked = cell.hasAttribute('show_ik_minmax')
            ? cell.getAttribute('show_ik_minmax', '') === 'true'
            : true;

        document.getElementById('calc_loadflow').checked = cell.hasAttribute('calcLoadflow')
            ? cell.getAttribute('calcLoadflow', '') === 'true'
            : true;
        document.getElementById('show_voltage').checked = cell.hasAttribute('showVoltage') ? cell.getAttribute('showVoltage', '') === 'true' : true;
        document.getElementById('show_current').checked = cell.hasAttribute('showCurrent') ? cell.getAttribute('showCurrent', '') === 'true' : true;
        document.getElementById('show_losses').checked = cell.hasAttribute('showLosses') ? cell.getAttribute('showLosses', '') === 'true' : false;
        document.getElementById('max_voltage_warning').value = cell.hasAttribute('maxVoltageWarning')
            ? cell.getAttribute('maxVoltageWarning', '')
            : maxVoltageDiff;
        document.getElementById('max_loadflow_iterations').value = cell.hasAttribute('maxLoadflowIterations')
            ? cell.getAttribute('maxLoadflowIterations', '')
            : maxIterations;
        document.getElementById('convergence_limit').value = cell.hasAttribute('convergenceLimit')
            ? cell.getAttribute('convergenceLimit', '')
            : convergenceLimit;
        document.getElementById('show_descriptions').checked = cell.hasAttribute('showDescription')
            ? cell.getAttribute('showDescription', '') === 'true'
            : false;
        document.getElementById('project_name').value = cell.hasAttribute('projectName') ? cell.getAttribute('projectName', '') : 'New project';

        if (cell.hasAttribute('displayUnits')) {
            const displayUnits = parseFloat(cell.getAttribute('displayUnits', ''));
            document.getElementById('unit_mw').checked = displayUnits === 1;
            document.getElementById('unit_kw').checked = displayUnits === 2;
        } else {
            document.getElementById('unit_mw').checked = false;
            document.getElementById('unit_kw').checked = true; // default
        }
    } catch (e) {
        mxUtils.alert(e);
    }
};

/**
 * React dialog jsx
 */
const ReactOptions = (props) => {
    let key;

    return (
        <span>
            <Image src="/app/slackbus/img/logo.png"></Image>
            <br />
            <Tabs activeKey={key}>
                <Tab eventKey="labels" title="Project">
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Basic Data</Card.Header>
                            <br />
                            <DialogInput label="Project name" id="project_name" append=" " />
                        </Card.Body>
                    </Card>
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Units and Labels</Card.Header>
                            <br />
                            <DialogCheckbox label="Show descriptions" id="show_descriptions" />
                            <br />
                            <Form>
                                <Form.Group as={Row}>
                                    <Form.Label as="legend" column sm={2} className="mb-3">
                                        Display units
                                    </Form.Label>
                                    <Form.Check inline type="radio" label="MW | kV | km" name="formHorizontalRadios" id="unit_mw" />
                                    <Form.Check inline type="radio" label="kW | V | m" name="formHorizontalRadios" id="unit_kw" />
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                </Tab>
                <Tab eventKey="loadflow" title="Loadflow">
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Basic Data</Card.Header>
                            <br />
                            <DialogCheckbox label="Calculate loadflow" id="calc_loadflow" />
                            <DialogCheckbox label="Show voltage" id="show_voltage" />
                            <DialogCheckbox label="Show current" id="show_current" />
                            <DialogCheckbox label="Show voltage drop" id="show_losses" />
                        </Card.Body>
                    </Card>
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Advanced</Card.Header>
                            <br />
                            <DialogInput label="Max. voltage drop" id="max_voltage_warning" datacy="numeric-input" append="%" />
                            <DialogInput label="Max. loadflow iterations" id="max_loadflow_iterations" datacy="numeric-input" append=" " />
                            <DialogInput label="Loadflow convergence limit" id="convergence_limit" datacy="numeric-input" append=" " />
                        </Card.Body>
                    </Card>
                </Tab>
                <Tab eventKey="shortcircuit" title="Shortcircuit">
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Basic Data</Card.Header>
                            <br />
                            <DialogCheckbox label="Calculate shortcircuit current" id="calc_isc" />
                            <DialogCheckbox label="Show all shortcircuit currents" id="show_shortcircuit" />
                            <DialogCheckbox label="Show Ik min/max" id="show_ik_minmax" />
                        </Card.Body>
                    </Card>
                    <br />
                    <Card style={{ width: '95%' }}>
                        <Card.Body>
                            <Card.Header>Advanced</Card.Header>
                            <br />
                        </Card.Body>
                    </Card>
                </Tab>
            </Tabs>
        </span>
    );
};

/**
 * Options dialog
 * @param {*} ui
 * @param {*} network
 * @param {*} slackbus
 */
const OptionsDialog = function (ui) {
    /*
     * Setup div elements
     */

    var div = document.createElement('div');
    const reactElements = document.createElement('div');
    reactElements.id = 'react-options';
    reactElements.style.cssText = 'position:absolute;left:30px;right:30px;overflow-y:auto;top:30px;bottom:80px;';

    // Inject React elements into div
    div.appendChild(reactElements);

    /**
     * Get cell info from graph
     */
    const graph = ui.editor.graph;
    const parent = graph.getDefaultParent().getParent(); // Returns cell
    let value = graph.getModel().getValue(parent);

    // Converts the value to an XML node
    if (!mxUtils.isNode(value)) {
        var doc = mxUtils.createXmlDocument();
        var obj = doc.createElement('object');
        obj.setAttribute('label', value || '');
        value = obj;
    }

    /**
     * Add buttons
     */

    // Cancel button
    var cancelBtn = mxUtils.button(mxResources.get('cancel'), function () {
        ui.hideDialog.apply(ui, arguments);
    });
    cancelBtn.className = 'geBtn';

    // Apply button
    var applyBtn = mxUtils.button(mxResources.get('apply'), function () {
        try {
            writeOptions(value);
            ui.hideDialog.apply(ui, arguments);
            // Updates the value of the cell (undoable)
            graph.getModel().setValue(parent, value);

            // Update displayUnits on all graph elements
            updateUnits(value, graph);
        } catch (e) {
            mxUtils.alert(e);
        }
    });
    applyBtn.className = 'geBtn gePrimaryBtn';

    var buttons = document.createElement('div');
    buttons.style.cssText = 'position:absolute;left:30px;right:30px;text-align:right;bottom:30px;height:40px;';
    buttons.appendChild(cancelBtn);
    buttons.appendChild(applyBtn);
    div.appendChild(buttons);

    /**
     * Show dialog
     */
    ui.showDialog(div, 420, 420, true, true, null, false, false, null, true);
    ReactDOM.render(<ReactOptions />, document.getElementById('react-options'));
    readOptions(value);
};

/**
 * Get options from ui to network object, without using dialog
 *
 * @param {*} ui
 * @param {*} network
 * @returns {number} - Return 0 if successful
 * */
const getOptions = function (ui, network) {
    try {
        // Get default parent for UI options
        const graph = ui.editor.graph;
        const parent = graph.getDefaultParent().getParent(); // Returns cell
        const value = graph.getModel().getValue(parent);
        const calcIsc = value.getAttribute('calcIsc', 'true') === 'true';
        network.options.calcIk1 = calcIsc;
        network.options.calcIk2 = calcIsc;
        network.options.calcIk2EE = calcIsc;
        network.options.calcIk3 = calcIsc;
        network.options.cMax = value.hasAttribute('cmax') ? parseFloat(value.getAttribute('cmax')) : cmax;
        network.options.cMin = value.hasAttribute('cmin') ? parseFloat(value.getAttribute('cmin')) : cmin;
        network.options.calcLoadflow = value.getAttribute('calcLoadflow', 'true') === 'true';
        network.options.slackbus.showDescription = value.getAttribute('showDescription', 'false') === 'true';
        network.options.slackbus.showVoltage = value.getAttribute('showVoltage', 'true') === 'true';
        network.options.slackbus.showShortcircuit = value.getAttribute('showShortcircuit', 'true') === 'true';
        network.options.slackbus.showCurrent = value.getAttribute('showCurrent', 'true') === 'true';
        network.options.slackbus.showLosses = value.getAttribute('showLosses', 'false') === 'true';
        network.options.slackbus.maxVoltageWarning = parseFloat(value.getAttribute('maxVoltageWarning', maxVoltageDiff));
        network.options.convergenceLimit = parseFloat(value.getAttribute('convergenceLimit', convergenceLimit));
        network.options.maxLoadflowIterations = parseFloat(value.getAttribute('maxLoadflowIterations', maxIterations));
        network.options.slackbus.displayUnits = parseFloat(value.getAttribute('displayUnits', '2')); // default
        network.options.slackbus.showIkMinMax = value.getAttribute('show_ik_minmax', 'true') === 'true';

        // Error check
        if (
            parseFloat(network.options.convergenceLimit) > 0 &&
            parseFloat(network.options.slackbus.maxVoltageWarning) > 0 &&
            parseInt(network.options.maxLoadflowIterations) > 0 &&
            parseInt(network.options.slackbus.displayUnits) > 0
        ) {
            return 0;
        } else {
            throw new Error('Display units must be selected in project settings');
        }
    } catch (error) {
        mxUtils.alert('Please set project settings before trying to calculate network');
        console.error(error);
        return -1;
    }
};

/** Updates voltage and power units, based on selection of displayUnits
 *
 * @param {*} value
 * @param {*} graph
 */
function updateUnits(value, graph) {
    const projectDisplayUnits = parseInt(value.getAttribute('displayUnits'));

    const defaultParent = graph.getDefaultParent();
    const cells = graph.getChildCells(defaultParent);
    cells.forEach((c) => {
        const cellDisplayUnits = parseInt(c.getAttribute('displayUnits'));

        if (cellDisplayUnits !== projectDisplayUnits || isNaN(cellDisplayUnits)) {
            apiUtils.debugLog(
                VERBOSE,
                `Updating units on ${c.getAttribute('Name')}, cellDisplayUnits=${cellDisplayUnits} => projectDisplayUnits`,
                projectDisplayUnits
            );

            const Un = parseFloat(c.getAttribute('Un'));
            Un > 0 && c.setAttribute('Un', projectDisplayUnits === 1 ? Un / 1000 : Un * 1000);

            const Un1 = parseFloat(c.getAttribute('Un1'));
            Un1 > 0 && c.setAttribute('Un1', projectDisplayUnits === 1 ? Un1 / 1000 : Un1 * 1000);

            const Un2 = parseFloat(c.getAttribute('Un2'));
            Un2 > 0 && c.setAttribute('Un2', projectDisplayUnits === 1 ? Un2 / 1000 : Un2 * 1000);

            const S = parseFloat(c.getAttribute('S'));
            S > 0 && c.setAttribute('S', projectDisplayUnits === 1 ? S / 1000 : S * 1000);

            const P = parseFloat(c.getAttribute('P'));
            P > 0 && c.setAttribute('P', projectDisplayUnits === 1 ? P / 1000 : P * 1000);

            const Q = parseFloat(c.getAttribute('Q'));
            Q > 0 && c.setAttribute('Q', projectDisplayUnits === 1 ? Q / 1000 : Q * 1000);

            const Sk = parseFloat(c.getAttribute('Sk'));
            Sk > 0 && c.setAttribute('Sk', projectDisplayUnits === 1 ? Sk / 1000 : Sk * 1000);

            const length = parseFloat(c.getAttribute('Length'));
            length > 0 && c.setAttribute('Length', projectDisplayUnits === 1 ? length / 1000 : length * 1000);
        }

        c.setAttribute('displayUnits', projectDisplayUnits);
    });
}

export { OptionsDialog, getOptions };
