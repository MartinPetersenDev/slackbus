/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Image from 'react-bootstrap/Image';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

/**
 * React dialog jsx
 */
const ReactElements = () => (
    <span>
        <Container fluid>
            <Row>
                <Image src="/app/slackbus/img/logo.png"></Image>
            </Row>
            <Row>
                <p>SlackBUS</p>
                <p>Copyright (c) 2020 Firmaet Martin Petersen</p>
                <p>Build version: <span id="build-version">@@CI_COMMIT_SHORT_SHA</span></p>
            </Row>
        </Container>
    </span>
);

/**
 * Options dialog
 * @param {*} ui
 */
const AboutDialog = function (ui) {
    /*
     * Setup div elements
     */
    const div = document.createElement('div');
    const reactElements = document.createElement('div');
    reactElements.id = 'react-elements';
    div.appendChild(reactElements);

    /**
     * Add mxUtils buttons
     */
    const OKBtn = mxUtils.button(mxResources.get('ok'), () => ui.hideDialog.apply(ui, arguments));
    OKBtn.className = 'geBtn gePrimaryBtn';

    var buttons = document.createElement('div');
    buttons.style.cssText = 'position:absolute;left:30px;right:30px;text-align:right;bottom:30px;height:40px;';
    buttons.appendChild(OKBtn);

    div.appendChild(buttons);

    /**
     * Show dialog
     */
    ui.showDialog(div, 380, 240, true, true, null, false, false, null, true);
    ReactDOM.render(<ReactElements />, document.getElementById('react-elements'));
};

// module.exports.AboutDialog = AboutDialog;
export { AboutDialog };
