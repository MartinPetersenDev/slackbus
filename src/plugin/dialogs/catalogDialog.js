/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

/**
 * Catalog dialog
 * @param {*} ui
 * @param {*} cell
 */
var CatalogDialog = function (ui) {
    const cablesCatalog = require('../catalog/cables.json');
    const div = document.createElement('div');

    // Creates the dialog contents
    var form = new mxForm('catalog');
    form.table.style.width = '100%';
    const logo = new Image();
    logo.src = '/app/slackbus/img/logo.png';
    logo.alt = 'Transformer model';
    const top = document.createElement('div');
    top.style.cssText = 'position:absolute;left:30px;right:30px;overflow-y:auto;top:15px;bottom:60px;';
    top.appendChild(logo);
    div.appendChild(top);

    // Add text
    const txt = document.createElement('p');
    txt.textContent = 'Select from catalog:';
    txt.style.cssText += 'width:95%;';

    // Setup combo
    const combo = document.createElement('select');
    combo.style.cssText += 'width:95%;';
    combo.size = 8;

    // Add options from catalog
    let i = 0;
    cablesCatalog.catalog.forEach((o) => {
        const option = document.createElement('option');
        option.text = `${o.vendor} ${o.noOfConductors}X${o.crossSection} ${o.conductorMaterial}`;
        option.value = i;
        combo.appendChild(option);
        i++;
    });

    var top2 = document.createElement('div');
    top2.style.cssText = `position:absolute;left:30px;right:30px;overflow-y:auto;top:80px;bottom:80px;`;
    top2.appendChild(txt);
    top2.appendChild(combo);
    div.appendChild(top2);

    var cancelBtn = mxUtils.button(mxResources.get('cancel'), function () {
        ui.hideDialog.apply(ui, arguments);
    });
    cancelBtn.className = 'geBtn';

    var applyBtn = mxUtils.button(mxResources.get('apply'), function () {
        try {
            ui.hideDialog.apply(ui, arguments);
            // Transfer catalogdata to dataDialog
            const cable = cablesCatalog.catalog[combo.value];
            document.getElementById('description').value = `${cable.vendor} ${cable.noOfConductors}X${cable.crossSection} ${cable.conductorMaterial}`;
            document.getElementById('rm').value = cable.rm;
            document.getElementById('xm').value = cable.xm;
            document.getElementById('r0').value = cable.r0;
            document.getElementById('x0').value = cable.x0;
            document.getElementById('g').value = cable.g;
            document.getElementById('b').value = cable.b;
            document.getElementById('maxtemp').value = cable.maxTemp;
        } catch (e) {
            mxUtils.alert(e);
        }
    });
    applyBtn.className = 'geBtn gePrimaryBtn';

    var buttons = document.createElement('div');
    buttons.style.cssText = 'position:absolute;left:30px;right:30px;text-align:right;bottom:30px;height:40px;';
    buttons.appendChild(cancelBtn);
    buttons.appendChild(applyBtn);
    div.appendChild(buttons);

    ui.showDialog(div, 380, 300, true, true, null, false, false, null, true);
};

module.exports.CatalogDialog = CatalogDialog;
