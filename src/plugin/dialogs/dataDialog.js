/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

import './dialogs.css';
import React from 'react';
import ReactDOM from 'react-dom';
import ReactDataDialog from '../components/ReactDataDialog';

const labelsjs = require('../labels');
const catalogjs = require('./catalogDialog');
const apiUtils = require('../../sdk/utils');
const utils = require('../utils');

// Config DEBUG
const DEBUG = false;

/**
 * Data dialog
 * @param {*} ui
 * @param {*} cell
 */
var DataDialog = function (ui, cell) {
    /*
     * Setup div elements
     */
    const div = document.createElement('div');
    const reactElements = document.createElement('div');
    reactElements.id = 'react-elements';
    reactElements.style.cssText = 'position:absolute;left:30px;right:30px;overflow-y:auto;top:30px;bottom:80px;';

    // Inject React elements into div
    div.appendChild(reactElements);

    /**
     * Get cell info from graph
     */
    const graph = ui.editor.graph;
    const value = graph.getModel().getValue(cell);
    const id = EditDataDialog.getDisplayIdForCell != null ? EditDataDialog.getDisplayIdForCell(ui, cell) : null;
    const type = cell.getAttribute('type', '').toLowerCase();

    apiUtils.debugLog(DEBUG, '[DEBUG] showing dialog for id', id);

    /**
     * Add buttons
     */

    // Catalog button
    if (type === 'line') {
        var catalogBtn = mxUtils.button('Catalog', function () {
            // var dlg =
            catalogjs.CatalogDialog(ui);
            // ui.showDialog(dlg, 380, 300, true, true, null, false, false, null, true);
        });
        catalogBtn.className = 'geBtn';
    }

    // Cancel button
    var cancelBtn = mxUtils.button(mxResources.get('cancel'), function () {
        ui.hideDialog.apply(ui, arguments);
    });

    cancelBtn.className = 'geBtn';

    // Apply button
    var applyBtn = mxUtils.button(mxResources.get('apply'), function () {
        if (writeAttributes(cell) === 0) {
            graph.getModel().setValue(cell, value);
            labelsjs.refreshLabels(graph);
        }
        ui.hideDialog.apply(ui, arguments);
    });
    applyBtn.className = 'geBtn gePrimaryBtn';

    var buttons = document.createElement('div');
    buttons.style.cssText = 'position:absolute;left:30px;right:30px;text-align:right;bottom:30px;height:40px;';
    catalogBtn && buttons.appendChild(catalogBtn);
    buttons.appendChild(cancelBtn);
    buttons.appendChild(applyBtn);
    div.appendChild(buttons);

    // Fetch parent object, to check if options are set
    const parent = graph.getDefaultParent().getParent(); // Returns cell

    // HACK
    // Arcane workaround, to make sure an unitialized graph does not fail, when testing for hasAttribute
    let parentValue = graph.getModel().getValue(parent);
    if (!mxUtils.isNode(parentValue)) {
        var doc = mxUtils.createXmlDocument();
        var obj = doc.createElement('object');
        obj.setAttribute('label', parentValue || '');
        parentValue = obj;
    }
    graph.getModel().setValue(parent, parentValue);

    // Fetch displayUnits from project settings and store on object
    const displayUnits = parseInt(parent.getAttribute('displayUnits', '2'));
    cell.setAttribute('displayUnits', displayUnits);

    /**
     * Show dialog
     */
    const units = {
        voltUnit: displayUnits === 1 ? 'kV' : 'V',
        wattUnit: displayUnits === 1 ? 'MW' : 'kW',
        vaUnit: displayUnits === 1 ? 'MVA' : 'kVA',
        varUnit: displayUnits === 1 ? 'MVAr' : 'kVAr',
        lengthUnit: displayUnits === 1 ? 'km' : 'm',
    };

    ui.showDialog(div, 430, 600, true, true, null, false, false, null, true);
    ReactDOM.render(<ReactDataDialog id={id} type={type} units={units} />, document.getElementById('react-elements'));
    readAttributes(cell);
};

/** Write attributes to cell */
const writeAttributes = (cell) => {
    const type = cell.getAttribute('type', '').toLowerCase();

    try {
        if (type === 'transformer') {
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('Un1', utils.cleanUpFloat(document.getElementById('un1').value));
            cell.setAttribute('Un2', utils.cleanUpFloat(document.getElementById('un2').value));
            cell.setAttribute('S', utils.cleanUpFloat(document.getElementById('s').value));
            cell.setAttribute('ek', utils.cleanUpFloat(document.getElementById('ek').value));
            cell.setAttribute('X--R', utils.cleanUpFloat(document.getElementById('xr').value));
            cell.setAttribute('hasZ0', document.getElementById('hasz0').checked);
        } else if (type === 'feeder') {
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('Un', utils.cleanUpFloat(document.getElementById('un').value));

            // Check for case of sk/xr or ik3/ik1
            if (utils.cleanUpFloat(document.getElementById('ik3').value) !== '') {
                cell.setAttribute('Ik3', utils.cleanUpFloat(document.getElementById('ik3').value));
                cell.setAttribute('CosPhi', utils.cleanUpFloat(document.getElementById('cosphi').value));
                cell.setAttribute('Ik1', utils.cleanUpFloat(document.getElementById('ik1').value));
                cell.setAttribute('Sk', '');
                cell.setAttribute('X--R', '');
            } else if (utils.cleanUpFloat(document.getElementById('sk').value) !== '') {
                cell.setAttribute('Ik3', '');
                cell.setAttribute('CosPhi', '');
                cell.setAttribute('Ik1', '');
                cell.setAttribute('Sk', utils.cleanUpFloat(document.getElementById('sk').value));
                cell.setAttribute('X--R', utils.cleanUpFloat(document.getElementById('xr').value));
            }

            // const ufactor = utils.cleanUpFloat(document.getElementById('ufactor').value);
            // const ufactorPct = document.getElementById('ufactor').value === '' ? 1 : parseFloat(ufactor) / 100;
            // cell.setAttribute('Ufactor', ufactorPct);
            cell.setAttribute('Ufactor', 1);
        } else if (type === 'generator') {
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('Un', utils.cleanUpFloat(document.getElementById('un').value));
            cell.setAttribute('S', utils.cleanUpFloat(document.getElementById('s').value));
            cell.setAttribute('pf', utils.cleanUpFloat(document.getElementById('pf').value));
            cell.setAttribute('x..d', utils.cleanUpFloat(document.getElementById('xd').value));
            cell.setAttribute('x0', utils.cleanUpFloat(document.getElementById('x0').value));
            cell.setAttribute('hasZ0', document.getElementById('hasz0').checked);
            //  const ufactor = utils.cleanUpFloat(document.getElementById('ufactor').value);
            // const ufactorPct = document.getElementById('ufactor').value === '' ? 1 : parseFloat(ufactor) / 100;
            // cell.setAttribute('Ufactor', ufactorPct);
            cell.setAttribute('Ufactor', 1);
        } else if (type === 'line') {
            cell.setAttribute('Description', document.getElementById('description').value);
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('Length', utils.cleanUpFloat(document.getElementById('length').value));
            cell.setAttribute('Rm', utils.cleanUpFloat(document.getElementById('rm').value));
            cell.setAttribute('Xm', utils.cleanUpFloat(document.getElementById('xm').value));
            cell.setAttribute('R0', utils.cleanUpFloat(document.getElementById('r0').value));
            cell.setAttribute('X0', utils.cleanUpFloat(document.getElementById('x0').value));
            cell.setAttribute('G', utils.cleanUpFloat(document.getElementById('g').value));
            cell.setAttribute('B', utils.cleanUpFloat(document.getElementById('b').value));
            cell.setAttribute('ambtemp', utils.cleanUpFloat(document.getElementById('ambtemp').value));
            cell.setAttribute('maxtemp', utils.cleanUpFloat(document.getElementById('maxtemp').value));
        } else if (type === 'load') {
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('P', utils.cleanUpFloat(document.getElementById('p').value));
            cell.setAttribute('Q', utils.cleanUpFloat(document.getElementById('q').value));
        } else if (type === 'node') {
            cell.setAttribute('Name', document.getElementById('name').value);
            cell.setAttribute('Un', utils.cleanUpFloat(document.getElementById('un').value));
            cell.setAttribute('isSlack', document.getElementById('isslack').checked);
            const isSinglePhase = document.getElementById('singlephase').checked;
            cell.setAttribute('singlePhase', isSinglePhase);
            const P = utils.cleanUpFloat(document.getElementById('p').value);
            const Q = utils.cleanUpFloat(document.getElementById('q').value);

            // cell.setAttribute('P', isSinglePhase ? P * 3 : P);
            // cell.setAttribute('Q', isSinglePhase ? Q * 3 : Q);
            cell.setAttribute('P', P);
            cell.setAttribute('Q', Q);
        }
    } catch (e) {
        mxUtils.alert(e);
    }
};

/** Read attributes from cell */
const readAttributes = (cell) => {
    const type = cell.getAttribute('type', '').toLowerCase();

    try {
        if (type === 'transformer') {
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('un1').value = cell.getAttribute('Un1', '');
            document.getElementById('un2').value = cell.getAttribute('Un2', '');
            document.getElementById('s').value = cell.getAttribute('S', '');
            document.getElementById('ek').value = cell.getAttribute('ek', '');
            document.getElementById('xr').value = cell.getAttribute('X--R', '');
            document.getElementById('hasz0').checked = cell.getAttribute('hasZ0', 'false') === 'true';
        } else if (type === 'feeder') {
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('un').value = cell.getAttribute('Un', '');
            // document.getElementById('ufactor').value = parseFloat(cell.getAttribute('Ufactor', ''), 10) * 100 || 100;

            // Check for case of sk/xr or ik3/ik1
            if (cell.getAttribute('Ik3', '').toString().length > 0) {
                document.getElementById('ik3').value = cell.getAttribute('Ik3', '');
                document.getElementById('cosphi').value = cell.getAttribute('CosPhi', '');
                document.getElementById('ik1').value = cell.getAttribute('Ik1', '');
                document.getElementById('ik3').disabled = false;
                document.getElementById('cosphi').disabled = false;
                document.getElementById('ik1').disabled = false;
                document.getElementById('sk').disabled = true;
                document.getElementById('xr').disabled = true;
            } else if (cell.getAttribute('Sk', '').toString().length > 0) {
                document.getElementById('sk').value = cell.getAttribute('Sk', '');
                document.getElementById('xr').value = cell.getAttribute('X--R', '');
                document.getElementById('ik3').disabled = true;
                document.getElementById('cosphi').disabled = true;
                document.getElementById('ik1').disabled = true;
                document.getElementById('sk').disabled = false;
                document.getElementById('xr').disabled = false;
            }
        } else if (type === 'generator') {
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('un').value = cell.getAttribute('Un', '');
            // document.getElementById('ufactor').value = parseFloat(cell.getAttribute('Ufactor', ''), 10) * 100 || 100;
            document.getElementById('s').value = cell.getAttribute('S', '');
            document.getElementById('pf').value = cell.getAttribute('pf', '');
            document.getElementById('xd').value = cell.getAttribute('x..d', '');
            document.getElementById('x0').value = cell.getAttribute('x0', '');
            document.getElementById('hasz0').checked = cell.getAttribute('hasZ0', 'false') === 'true';
        } else if (type === 'line') {
            document.getElementById('description').value = cell.getAttribute('Description', '');
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('length').value = cell.getAttribute('Length', '');
            document.getElementById('rm').value = cell.getAttribute('Rm', '');
            document.getElementById('xm').value = cell.getAttribute('Xm', '');
            document.getElementById('r0').value = cell.getAttribute('R0', '');
            document.getElementById('x0').value = cell.getAttribute('X0', '');
            document.getElementById('g').value = cell.getAttribute('G', '');
            document.getElementById('b').value = cell.getAttribute('B', '');
            document.getElementById('ambtemp').value = cell.getAttribute('ambtemp', '');
            document.getElementById('maxtemp').value = cell.getAttribute('maxtemp', '');
        } else if (type === 'load') {
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('p').value = cell.getAttribute('P', '');
            document.getElementById('q').value = cell.getAttribute('Q', '');
        } else if (type === 'node') {
            document.getElementById('name').value = cell.getAttribute('Name', '');
            document.getElementById('un').value = cell.getAttribute('Un', '');
            document.getElementById('isslack').checked = !utils.hasEdgeToType(cell, 'feeder', false)
                ? false
                : cell.getAttribute('isSlack', 'false') === 'true';
            document.getElementById('isslack').disabled = !utils.hasEdgeToType(cell, 'feeder', false);

            const P = cell.getAttribute('P', '');
            const Q = cell.getAttribute('Q', '');
            document.getElementById('p').value = P;
            document.getElementById('q').value = Q;

            // get singlePhase or default to false
            const isSinglePhase = cell.getAttribute('singlePhase', 'false') === 'true';
            // Write isSinglePhase back to cell
            cell.setAttribute('singlePhase', isSinglePhase);
            document.getElementById('singlephase').checked = isSinglePhase;
        }
    } catch (e) {
        mxUtils.alert(e);
    }
};

export { DataDialog };
