/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

// Config DEBUG
const DEBUG = false;

// Global variables
const voltageColors = ['#66B2FF', '#97D077', '#FFCC99', '#FFFF99', '#CC99FF', '#666600', '#CCCCCC'];

/**
 * Updates colors in label style on cell
 * @param {*} g - Graph
 * @param {*} c - Cell
 */
const updateLabelStyle = function (network, g, c) {
    const voltage = parseFloat(c.getAttribute('Un', '')) || parseFloat(c.getAttribute('vBase', ''));
    const type = c.getAttribute('type', '').toLowerCase();
    const index = network.options.slackbus.voltages.indexOf(voltage.toString()) % voltageColors.length;
    let color;

    if (index === -1) {
        color = '#CCCCCC';
    } else {
        color = voltageColors[index];
    }

    DEBUG &&
        console.log(
            `updatelabel called with voltage=${voltage} from voltage array ${JSON.stringify(
                network.options.slackbus.voltages
            )}, using color ${color} from color array ${JSON.stringify(voltageColors)}`
        );

    // Regex for setting color, two named capture groups
    const labelBgRgx = /(labelBackgroundColor=)(#\w{6})/gi;
    const fillRgx = /(fillColor=)(#\w{6})/gi;
    const removeFillRgx = /(fillColor=#\w{6})/gi;
    const strokeRgx = /(strokeColor=)(#\w{6})/gi;
    const fontRgx = /(fontColor=)(#\w{6})/gi;
    const fontFamilyRgx = /(fontFamily=)(\w+)(;)/gi;
    const fontSizeRgx = /(fontSize=)(\d+)(;)/gi;

    // get cell style
    let style = g.model.getStyle(c);

    // eslint-disable-next-line no-undef
    const dark = uiTheme === 'dark';
    const semiColon = style[style.length - 1] === ';' ? '' : ';';

    // Set style according to type
    style = style.includes('labelBackgroundColor')
        ? (style = style.replace(labelBgRgx, `$1${color}`))
        : style.concat(semiColon).concat(`labelBackgroundColor=${color};`);

    style = style.includes('strokeColor')
        ? (style = style.replace(strokeRgx, `$1${color}`))
        : style.concat(semiColon).concat(`strokeColor=${color};`);

    style = style.includes('fontFamily') ? (style = style.replace(fontFamilyRgx, `$1Arial;`)) : style.concat(semiColon).concat('fontFamily=Arial;');

    style = style.includes('fontSize') ? (style = style.replace(fontSizeRgx, `$111;`)) : style.concat(semiColon).concat('fontSize=11;');

    // Dark theme
    if (dark) {
        style = style.includes('fontColor') ? style.replace(fontRgx, `$1$#000000`) : style.concat(semiColon).concat('fontColor=#000000;');
    }
    if (type === 'node') {
        style = dark ? style.replace(removeFillRgx, '') : style.replace(fillRgx, `$1${color}`);
    } else if (type === 'load') {
        style = dark ? style.replace(removeFillRgx, '') : style.replace(fillRgx, `$1${color}`);
    }

    g.model.setStyle(c, style);
};

module.exports.updateLabelStyle = updateLabelStyle;
