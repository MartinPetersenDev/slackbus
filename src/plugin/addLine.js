/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const utils = require('./utils');
const linkjs = require('../sdk/link');
const sdkUtils = require('../sdk/utils');
const Complex = require('complex.js');

// Config
const VERBOSE = false;

/**
 * Add line to network
 *
 * @param cell {object} - cell
 * @param net {object} - network
 */
function addLine(cell, net) {
    const name = cell.getAttribute('Name', '');
    let hasNegativeValues;
    let vBase;
    let vBase2;

    try {
        // Line has source and target, use these as from/to
        const from = parseInt(cell.getTerminal(true).getAttribute('node', ''));
        const to = parseInt(cell.getTerminal(false).getAttribute('node', ''));

        // Get vBase from connected node
        vBase = parseFloat(cell.getTerminal(true).getAttribute('Un', ''));
        vBase2 = parseFloat(cell.getTerminal(false).getAttribute('Un', ''));

        // Set attributes on diagram objects
        cell.setAttribute('vBase', parseFloat(vBase));
        cell.setAttribute('to', parseInt(to));
        cell.setAttribute('from', parseInt(from));
        cell.setAttribute('displayUnits', net.options.slackbus.displayUnits);

        const line = linkjs.LinkFactory(name);

        // Get singlePhase attribute
        const singlePhase =
            cell.getTerminal(true).getAttribute('singlePhase', '') === 'true' || cell.getTerminal(false).getAttribute('singlePhase', '') === 'true';

        // Adjust sBase for pu and length calculation, according to display unit
        const factor = parseInt(cell.getAttribute('displayUnits')) === 1 ? 1 : 1000; // MW|kV|km = 1, kW|V|m = 1000
        const sBasePu = net.sBase * factor;

        // Calculate actual zm from input values and convert to per-unit
        const Rm = cell.hasAttribute('Rm') ? parseFloat(cell.getAttribute('Rm', '')) : parseFloat(0);
        const Xm = cell.hasAttribute('Xm') ? parseFloat(cell.getAttribute('Xm', '')) : parseFloat(0);
        const maxtemp = cell.hasAttribute('maxtemp') ? cell.getAttribute('maxtemp', '') : 20;

        const Rfault = 1 + 0.004 * (parseFloat(maxtemp) - 20);

        const length = parseFloat(cell.getAttribute('Length', '')) / factor;
        const strZm = `${Rm * length}+${Xm * length}i`;

        line.setId(cell.getId())
            .setSinglePhase(singlePhase)
            .setFrom(from)
            .setTo(to)
            .setBase(net.sBase, vBase)
            .setZm(new Complex(sdkUtils.convertToPerUnit(vBase, sBasePu, strZm)))
            .setRfault(Rfault);

        // Calculate actual z0 from input values and convert to per-unit
        let strZ0;
        const atrR0 = cell.getAttribute('R0', '');
        const R0 = typeof atrR0 === 'undefined' || atrR0 === '' ? parseFloat(0) : parseFloat(atrR0);
        const atrX0 = cell.getAttribute('X0', '') || cell.getAttribute('x0', '');
        const X0 = typeof atrX0 === 'undefined' || atrX0 === '' ? parseFloat(0) : parseFloat(atrX0);

        if (R0 > 0 || X0 > 0) {
            strZ0 = `${R0 * length}+${X0 * length}i`;
            line.setZ0(sdkUtils.convertToPerUnit(vBase, sBasePu, strZ0));
        }

        // Calculate shunt from input values and convert to per-unit
        const atrG = cell.getAttribute('G', '');
        const G = typeof atrG === 'undefined' || atrG === '' ? parseFloat(0) : parseFloat(atrG);
        const atrB = cell.getAttribute('B', '');
        const B = typeof atrB === 'undefined' || atrB === '' ? parseFloat(0) : parseFloat(atrB);

        let strShunt;
        if (G > 0 || B > 0) {
            strShunt = `${G * length}+${B * length}i`;
            line.setShunt(sdkUtils.convertToPerUnit(vBase, sBasePu, strShunt));
        }

        hasNegativeValues = Rm < 0 || Xm < 0 || R0 < 0 || X0 < 0 || G < 0 || B < 0 || length < 0;

        // Add line to network
        net.addLinkImpedance(line);

        // VERBOSE logging
        sdkUtils.debugLog(VERBOSE, 'Added line', name);
        sdkUtils.debugLog(VERBOSE, '... actual impedance', strZm);
        sdkUtils.debugLog(VERBOSE, '... per-unit impedance', line.zm.toString());
        sdkUtils.debugLog(VERBOSE, '... z0', strZ0);
        sdkUtils.debugLog(VERBOSE, '... from', from);
        sdkUtils.debugLog(VERBOSE, '... to', to);
    } catch (error) {
        net.addMessage(
            '[checkGraphData]',
            `Line "${name}" has one or more invalid connections or is missing some configuration values`,
            null,
            'error'
        );
    }

    // Error check - Edge must be a node
    if (!utils.hasEdgeToType(cell, 'node')) {
        net.addMessage('[checkGraphData]', `Line ${name} must be connected to a node at both ends`, null, 'error');
    }

    // Error check - Impedance cannot be negative
    if (hasNegativeValues) {
        net.addMessage('[checkGraphData]', `Line ${name} cannot have negative values`, null, 'error');
    }

    // Error check - vBase in both line ends must be identical
    if (vBase !== vBase2) {
        net.addMessage('[checkGraphData]', `Line ${name} cannot connect nodes of different voltage ${vBase} and ${vBase2}`, null, 'error');
    }
}

module.exports.addLine = addLine;
