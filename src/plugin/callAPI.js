/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';
require('es6-promise').polyfill();
const fetch = require('isomorphic-fetch');
const labelsjs = require('./labels.js');
const utils = require('./utils');
const sdkutils = require('../sdk/utils');
const engine = require('../engine/calculateNet');

// Config
const DEBUG = false;
const VERBOSE = false;
const useLocalApi = true;

// Setup RESTful API - if not useLocalApi
const apiUrl = 'https://l9im0ttsze.execute-api.eu-central-1.amazonaws.com/dev/calculateNet';
const postOptions = {
    Accept: 'application/json',
    'Content-type': 'application/json',
    Origin: 'http://localhost:8000',
};

/**
 * calculate uses the API to perform calculations on network. Set network.options as relevant.
 * @param {object} network - Network object to send to API
 * @returns {object} network - Calculated network
 */
const calculate = async (graph, net, ui) => {
    sdkutils.debugLog(VERBOSE, '[calculate] Calling API', '');
    sdkutils.debugLog(VERBOSE, '[DEBUG] Calling API', JSON.stringify(net));

    // eslint-disable-next-line no-undef
    ui.spinner.spin(document.body, mxResources.get('loading'));

    if (useLocalApi) {
        let network = { ...net };
        network.addMessage('callAPI', 'Using API type', 'local');

        try {
            network = await engine.calculateNet(network);
            network = processResult(network);
            labelsjs.updateGraphLabels(graph, network);
            return network;
        } catch (error) {
            // eslint-disable-next-line no-undef
            mxUtils.alert(`Failed to execute local calculation`);
            console.error(error);
            return net;
        } finally {
            ui.spinner.stop();
        }
    } else {
        net.addMessage('callAPI', 'Using API type', 'REST');
        const network = JSON.stringify(net);
        let response;

        try {
            response = await fetch(apiUrl, {
                method: 'POST',
                headers: postOptions,
                body: network,
            });

            if (response.status === 200) {
                const result = await response.json();
                sdkutils.debugLog(VERBOSE, '[calculate] Got status', response.status);

                const data = processResult(result.data);
                labelsjs.updateGraphLabels(graph, data);

                return data;
            } else if (response.status % 500 <= 99) {
                // eslint-disable-next-line no-undef
                mxUtils.alert('Network could not be calculated, please check network data');
                return net;
            }
        } catch (error) {
            let str = `Failed to contact calculation server, because of a network access issue`;
            str = DEBUG ? `${str}, URL=${apiUrl}` : str;
            // eslint-disable-next-line no-undef
            mxUtils.alert(str);
            console.error(error);
            return net;
        } finally {
            ui.spinner.stop();
        }
    }
};

/**
 * Process calculation result, returns updated network
 * @param {object} data - network object returned from calculation
 * @param {object} ui
 * @param {object} graph
 * @returns {object} network
 */
function processResult(data) {
    // Construct new network and copy results
    const networkjs = require('../sdk/network');
    let net = new networkjs.Network(data.name);
    net.links = data.links;
    net.matrix = data.matrix;
    net.nodes = data.nodes;
    net.messages = data.messages;
    net.options.slackbus = data.options.slackbus;

    // Turn items into complex objects
    net = { ...sdkutils.convertNodeUtoComplex(net) };

    // API messages
    const errorMessages = [];
    console.log('API reply contained the follow messages:');
    net.messages.forEach((msg) => {
        if (msg.type === 'message') {
            console.log(`[${msg.source}] ${msg.message} = ${JSON.stringify(msg.value)}`);
        } else if (msg.type === 'error') {
            errorMessages.push(msg.message);
        }
    });

    sdkutils.debugLog(DEBUG, '[DEBUG] API return network', JSON.stringify(net));

    // Check for error messages
    if (errorMessages.length > 0) {
        utils.showArrayAsAlert(errorMessages);
    }

    return net;
}

module.exports.calculate = calculate;
