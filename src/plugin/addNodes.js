/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

const nodejs = require('../sdk/node');
const linkjs = require('../sdk/link');
const utils = require('./utils');
const sdkUtils = require('../sdk/utils');

// Config
const VERBOSE = false;

/** Add all nodes
 * @param {object} graph
 * @param {object} cells - cells array
 * @param {object} network
 * @returns {number} - 0 = success
 */
function addNodes(graph, cells, network) {
    let slackNo = 0; // Count no of slackbus
    let nodeNo = 1; // Count number of other busses, starts at 1
    let slackbusCell;
    let node;

    cells.forEach((c) => {
        const type = c.getAttribute('type', '').toLowerCase();
        const isSlack = c.getAttribute('isSlack', '').toLowerCase() === 'true';
        const isSinglePhase = c.getAttribute('singlePhase') === 'true';

        // Process slackbus first
        if (type === 'node' && isSlack) {
            slackNo++;

            // Store first, and only first, slackbus cell for graph traversal later
            slackbusCell = slackNo === 1 ? c : undefined;

            // Set node attribute
            c.setAttribute('node', parseInt(1));
            c.setAttribute('displayUnits', network.options.slackbus.displayUnits);

            // Add voltagelevel to voltages array, for coloring labels
            utils.addVoltage(c.getAttribute('Un', ''), network);

            const atrUFactor = c.getAttribute('Ufactor', '');
            const UFactor = typeof atrUFactor === 'undefined' || atrUFactor === '' ? 1 : parseFloat(atrUFactor);

            const Un = parseFloat(c.getAttribute('Un', ''));
            const UnkV = network.options.slackbus.displayUnits === 1 ? Un : Un / 1000;
            const cmax = UnkV <= 1.0 ? 1.05 : 1.1;
            const cmin = UnkV <= 1.0 ? 0.95 : 1.0;

            // Build slackbus object and add to network
            node = nodejs.NodeFactory(c.getAttribute('Name', ''));
            node.setNode(slackNo)
                .setBase(network.sBase, Un)
                .setId(c.getId())
                .setUFactor(UFactor)
                .setNodeType(nodejs.typeEnum.slackbus)
                .setCFactor(cmax, cmin);

            // Add load to node
            const load = linkjs.LoadFactory('load');
            const P = c.getAttribute('P', '') || '0';
            const Q = c.getAttribute('Q', '') || '0';
            load.setP(parseFloat(P) / network.sBase).setQ(parseFloat(Q) / network.sBase);
            node.addLoadPower(load);

            network.addNode(node);

            // Error check - Slackbus must be connected to a source/generator
            try {
                if (!utils.hasEdgeToType(c, 'feeder')) {
                    network.addMessage('[checkGraphData]', `Slackbus must be connected to a feeder`, null, 'error');
                }
            } catch (error) {
                network.addMessage('[checkGraphData]', `"${c.getAttribute('Name', '')}" has an invalid connection`, null, 'error');
            }
        }

        // Error check - node must not be directly connected to node
        try {
            if (utils.hasNodeToNodeEdge(c)) {
                network.addMessage(
                    '[checkGraphData]',
                    `"${c.getAttribute('Name', '')}" is not allowed to have a direct connection to another node`,
                    null,
                    'error'
                );
            }
        } catch (error) {
            network.addMessage('[checkGraphData]', `"${c.getAttribute('Name', '')}" has an invalid connection`, null, 'error');
        }

        // Error check - node must not be unconnected
        if (type === 'node' && (!c.edges || c.edges.length === 0)) {
            network.addMessage('[checkGraphData]', `"${c.getAttribute('Name', '')}" must not be unconnected`, null, 'error');
        }

        // Error check - Slackbus must not be singlephase
        if (isSinglePhase && isSlack) {
            network.addMessage('[checkGraphData]', `Slackbus ${c.getAttribute('Name', '')} cannot be single phase`, null, 'error');
        }

        // Error check - Singlephase nodes must not be connected to transformers
        if (isSinglePhase && utils.hasEdgeToType(c, 'transformer')) {
            network.addMessage(
                '[checkGraphData]',
                `${c.getAttribute('Name', '')} is a single phase node. Single phase nodes cannot be connected to transformers`,
                null,
                'error'
            );
        }

        if (isSinglePhase) {
        }
    });

    // Find other buses by traversing graph with slackbus as new root node
    if (slackbusCell !== undefined) {
        graph.traverse(slackbusCell, false, function (vertex, edge) {
            const isNode = vertex.getAttribute('type', '').toLowerCase() === 'node';
            const isNotSlack = vertex.getAttribute('isSlack', '').toLowerCase() === 'false';

            if (isNode && isNotSlack) {
                // Error check - SinglePhaseNode can only have one (1) edge
                if (vertex.getAttribute('singlePhase') === 'true' && graph.getEdges(vertex).length > 1) {
                    network.addMessage(
                        '[checkGraphData]',
                        `SinglePhase node "${vertex.getAttribute('Name', '')}" is only allowed to have 1 connection`,
                        null,
                        'error'
                    );
                }

                // Add voltagelevel to voltages array, for coloring labels
                utils.addVoltage(vertex.getAttribute('Un', ''), network);

                const Un = parseFloat(vertex.getAttribute('Un', ''));
                const UnkV = network.options.slackbus.displayUnits === 1 ? Un : Un / 1000;
                const cmax = UnkV <= 1.0 ? 1.05 : 1.1;
                const cmin = UnkV <= 1.0 ? 0.95 : 1.0;

                // Set node attribute
                vertex.setAttribute('node', parseInt(nodeNo + 1));

                // Get singlephase attribute
                const isSinglePhase = vertex.getAttribute('singlePhase') === 'true';

                // Build node object and add to network
                node = nodejs.NodeFactory(vertex.getAttribute('Name', ''));
                node.setNode(nodeNo + 1)
                    .setBase(network.sBase, parseFloat(vertex.getAttribute('Un', '')))
                    .setId(vertex.getId())
                    .setSinglePhase(isSinglePhase)
                    .setCFactor(cmax, cmin);

                // Add load to node
                const load = linkjs.LoadFactory('load');
                const P = vertex.getAttribute('P', '') || '0';
                const Q = vertex.getAttribute('Q', '') || '0';

                load.setP(parseFloat(P) / network.sBase).setQ(parseFloat(Q) / network.sBase);
                node.addLoadPower(load);

                network.addNode(node);
                nodeNo++;
            }
        });
    }

    // Error check - There must be one slackbus node
    if (slackNo === 0) {
        network.addMessage('[checkGraphData]', `One node must be configured as slackbus`, null, 'error');
    } else if (slackNo > 1) {
        network.addMessage(
            '[checkGraphData]',
            `There can be only one node configured as slackbus, but you have configured ${slackNo}`,
            null,
            'error'
        );
    }

    return sdkUtils.networkHasError(network) ? 1 : 0;
}

module.exports.addNodes = addNodes;
