/**
 *
 * SlackBUS.net plugin for diagrams.net.
 *
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/* eslint-disable new-cap */
'use strict';

// Import
const addLoad = require('./addLoad');
const addTransformer = require('./addTransformer');
const addLine = require('./addLine');
const addFeeder = require('./addFeeder');
const addGenerator = require('./addGenerator');
const addNodes = require('./addNodes');
// const { utils } = require('mocha');

// DEBUG
const DEBUG = false;

/**
 * Adds graph objects to network
 */
function graphToNetwork(graph, network) {
    network.clear();
    network.options.slackbus.voltages = [];
    const defaultParent = graph.getDefaultParent();

    // Gets the subtree from defaultParent downwards
    const cells = graph.getChildCells(defaultParent);

    // Update old library items
    updateLibraryItems(cells);

    // Add all nodes to network
    const nodesOK = addNodes.addNodes(graph, cells, network) === 0;

    // Add sources, links, transformers etc.
    if (nodesOK) {
        cells.forEach((c) => {
            const type = c.getAttribute('type', '').toLowerCase();
            type === 'feeder' && addFeeder.addFeeder(graph, c, network);
            type === 'generator' && addGenerator.addGenerator(graph, c, network);
            type === 'transformer' && addTransformer.addTransformer(graph, c, network);
            type === 'line' && addLine.addLine(c, network);
            type === 'load' && addLoad.addLoad(graph, c, network);
        });
    }
}

/**
 * Updates old library items
 */
function updateLibraryItems(cells) {
    // <= v0.8: Update attribute "Busbar" to "node"
    cells.forEach((c) => {
        if (c.getAttribute('type', '').toLowerCase() === 'busbar') {
            c.setAttribute('type', 'node');
            DEBUG && console.log(`Upgrading 'busbar' to 'node'`);
        }
    });
}

module.exports.graphToNetwork = graphToNetwork;
