/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Shortcircuit calculation example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');

network.options.calcIk1 = true;
network.options.calcIk2 = true;
network.options.calcIk3 = true;
network.options.calcIk2EE = true;
network.options.calcLoadflow = false;
network.options.cMax = 1.1;
network.options.cMin = 0.95;
network.options.maxLoadflowIterations = 500;
network.options.gsAccelerationFactor = 1.5;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 500; // MVa

const bus0 = nodejs.NodeFactory('Net');
bus0.setBase(network.sBase, 65).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

const bus1 = nodejs.NodeFactory('A');
bus1.setBase(network.sBase, 11).setNode(2);

const bus2 = nodejs.NodeFactory('B');
bus2.setBase(network.sBase, 11).setNode(3);

const bus3 = nodejs.NodeFactory('C');
bus3.setBase(network.sBase, 0.42).setNode(4);

const bus4 = nodejs.NodeFactory('D');
bus4.setBase(network.sBase, 0.42).setNode(5);

const feeder = linkjs.LinkFactory('Feeder');
const vBase = 65;
const zk = 65 ** 2 / 500;
const angle = Math.atan(10);
const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
let zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, strZm)));
feeder.setFrom(0).setTo(bus0.node).setBase(network.sBase, 65).setZm(zm);

const vBaseT1 = 11;
const T1 = linkjs.LinkFactory('T1');
const zkt1 = ((11 / 100) * 12 ** 2) / 16;
const anglet1 = Math.atan(55);
const strZmt1 = `${Math.cos(anglet1) * zkt1}+${Math.sin(anglet1) * zkt1}i`;
zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBaseT1, network.sBase, strZmt1)));
const xt1 = (Math.sin(anglet1) * zkt1) / (12 ** 2 / 16);
T1.setFrom(bus0.node)
    .setTo(bus1.node)
    .setBase(network.sBase, vBaseT1)
    .setZm(zm)
    .setKt((0.95 * network.options.cMax) / (1 + 0.6 * xt1));

const L1 = linkjs.LinkFactory('L1');
zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBaseT1, network.sBase, '0.02+0.01i')));
L1.setFrom(bus1.node).setTo(bus2.node).setBase(network.sBase, vBaseT1).setZm(zm).setRfault(1.56);

const T2 = linkjs.LinkFactory('T2');
const vBaseT2 = 0.42;
const zkt2 = ((8 / 100) * 0.42 ** 2) / 1.2;
const anglet2 = Math.atan(88);
const strZmt2 = `${Math.cos(anglet2) * zkt2}+${Math.sin(anglet2) * zkt2}i`;
zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBaseT2, network.sBase, strZmt2)));
let z0 = zm;
const xt2 = (Math.sin(anglet2) * zkt2) / (0.42 ** 2 / 1.2);
T2.setFrom(bus2.node)
    .setTo(bus3.node)
    .setBase(network.sBase, vBaseT2)
    .setZm(zm)
    .setZ0(z0)
    .setFromZeroSeq(0)
    .setKt((0.95 * network.options.cMax) / (1 + 0.6 * xt2));

const T3 = linkjs.LinkFactory('T3');
zm = T2.zm;
z0 = T2.z0;
T3.setFrom(bus2.node).setTo(bus3.node).setBase(network.sBase, vBaseT2).setZm(zm).setZ0(z0).setFromZeroSeq(0);

const L2 = linkjs.LinkFactory('L2');
zm = mathjs.complex(utils.convertToPerUnit(vBaseT2, network.sBase, '0.0127+0.0077i'));
z0 = mathjs.complex(utils.convertToPerUnit(vBaseT2, network.sBase, '0.0508+0.0308i'));
L2.setFrom(bus3.node).setTo(bus4.node).setBase(network.sBase, vBaseT2).setZm(zm).setZ0(z0).setRfault(1.56);

const load = linkjs.LoadFactory('Load');
load.setP(0.2 / network.sBase).setQ(0.02 / network.sBase);
bus4.addLoadPower(load);

// Add elements to network in sorted order
network.addNode(bus0);
network.addNode(bus1);
network.addNode(bus2);
network.addNode(bus3);
network.addNode(bus4);

// Add impedance objects
network.addLinkImpedance(feeder);
network.addLinkImpedance(T1);
network.addLinkImpedance(L1);
network.addLinkImpedance(T2);
network.addLinkImpedance(T3);
network.addLinkImpedance(L2);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].lf.U === 'number') {
            temp.re = network.nodes[b].lf.U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].lf.U.re;
            temp.im = network.nodes[b].lf.U.im;
        }

        network.nodes[b].lf.U = temp;
    }

    utils.resultsToConsole(network);
}
