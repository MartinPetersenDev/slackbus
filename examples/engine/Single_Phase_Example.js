/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Single Phase loadflow example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');
network.options.calcIk1 = false;
network.options.calcIk2 = false;
network.options.calcIk3 = false;
network.options.calcIk2EE = false;
network.options.calcLoadflow = true;
network.options.cFactor = 1.0;
network.options.maxLoadflowIterations = 500;
network.options.gsAccelerationFactor = 1.4;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 100; // MVA
const vBase = 0.4; // kV

const bus1 = nodejs.NodeFactory('bus1');
bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

const bus2 = nodejs.NodeFactory('bus2');
bus2.setBase(network.sBase, vBase).setNode(2).setSinglePhase(true);

const bus3 = nodejs.NodeFactory('bus3');
bus3.setBase(network.sBase, vBase).setNode(3);

const line12 = linkjs.LinkFactory('L12');
line12
    .setBase(network.sBase, vBase)
    .setSinglePhase(true)
    .setFrom(bus1.node)
    .setTo(bus2.node)
    .setZm(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3}+${0.1 * 30e-3}i`)) // 0,030 km * 12.1+0.1i
    .setZ0(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3 * 4}+${0.1 * 30e-3 * 4}i`)); // 4*Zm

const load2 = linkjs.LoadFactory('Load');
load2.setP(0.0023 / network.sBase).setQ(0);
bus2.addLoadPower(load2);

const line13 = linkjs.LinkFactory('L13');
line13
    .setBase(network.sBase, vBase)
    .setFrom(bus1.node)
    .setTo(bus3.node)
    .setZm(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3}+${0.1 * 30e-3}i`)); // 0,030 km * 12.1+0.1i

const load3 = linkjs.LoadFactory('Load');
load3.setP(0.0023 / network.sBase).setQ(0);
bus3.addLoadPower(load3);

const feeder = linkjs.LinkFactory('Feeder');
feeder
    .setBase(network.sBase, vBase)
    .setFrom(0)
    .setTo(bus1.node)
    .setZm(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'))
    .setZ0(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'));
network.addLinkImpedance(feeder);

// Add elements to network in sorted order
network.addNode(bus1);
network.addNode(bus2);
network.addNode(bus3);

// Add impedance objects
network.addLinkImpedance(line12);
network.addLinkImpedance(line13);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].U === 'number') {
            temp.re = network.nodes[b].U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].lf.U.re;
            temp.im = network.nodes[b].lf.U.im;
        }

        network.nodes[b].U = temp;
    }

    utils.resultsToConsole(network);
}
