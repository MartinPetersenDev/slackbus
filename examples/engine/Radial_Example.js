/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Low-voltage radial calculation example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Example Config
const noRadialBusses = 10;

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');
network.options.calcIk1 = false;
network.options.calcIk2 = false;
network.options.calcIk3 = false;
network.options.calcIk2EE = false;
network.options.calcLoadflow = true;
network.options.cFactor = 1.0;
network.options.maxLoadflowIterations = 500;
network.options.gsAccelerationFactor = 1.5;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 100; // MVA

// MV 10 kV
const Un1 = 10;
const busMV = nodejs.NodeFactory('busMV');
busMV.setBase(network.sBase, Un1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);
network.addNode(busMV);

const feeder = netelmjs.LinkFactory('Feeder');
feeder.setBase(network.sBase, Un1).setFrom(0).setTo(busMV.node).setZm(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'));
network.addLinkImpedance(feeder);

// LV 0.4 kV
const Un2 = 0.4;

const busLV = nodejs.NodeFactory('busLV');
busLV.setBase(network.sBase, Un2).setNode(2);
network.addNode(busLV);

const zk = ((6 / 100) * Un2 ** 2) / 0.5; // zk = ek/100*Un^2/Sn
const angle = Math.atan(60);
const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
const t1 = netelmjs.LinkFactory('T1');
t1.setBase(network.sBase, Un2).setFrom(busMV.node).setTo(busLV.node).setZm(utils.convertToPerUnit(Un2, network.sBase, strZm));
network.addLinkImpedance(t1);

// Add noRadialBusses additional busses with load
for (let i = 1; i <= noRadialBusses; i++) {
    const bus = nodejs.NodeFactory(`Bus${i}`);
    bus.setBase(network.sBase, Un2).setNode(i + 2);

    const load = netelmjs.LoadFactory(`Load${i}`);
    load.setP(0.02 / network.sBase).setQ(0.001 / network.sBase);
    bus.addLoadPower(load);
    network.addNode(bus);
}

// Add 15 additional links
for (let i = 1; i <= noRadialBusses; i++) {
    const line = netelmjs.LinkFactory(`Line${i}`);
    line.setBase(network.sBase, Un2)
        .setFrom(1 + i)
        .setTo(2 + i)
        .setZm(utils.convertToPerUnit(Un2, network.sBase, '0.0127+0.008i'));
    network.addLinkImpedance(line);
}

// Add parallel Line1B
const line1B = netelmjs.LinkFactory(`Line1B`);
line1B.setBase(network.sBase, Un2).setFrom(2).setTo(3).setZm(utils.convertToPerUnit(Un2, network.sBase, '0.0127+0.008i'));
network.addLinkImpedance(line1B);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].U === 'number') {
            temp.re = network.nodes[b].U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].U.re;
            temp.im = network.nodes[b].U.im;
        }

        network.nodes[b].U = temp;
    }

    utils.resultsToConsole(network);
}
