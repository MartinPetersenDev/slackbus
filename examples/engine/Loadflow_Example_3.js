/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Loadflow calculation example 3

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');
network.options.calcIk1 = false;
network.options.calcIk2 = false;
network.options.calcIk3 = false;
network.options.calcIk2EE = false;
network.options.cFactor = 1.0;
network.options.calcLoadflow = true;
network.options.gsAccelerationFactor = 1.4;
network.options.maxLoadflowIterations = 50;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 100; // MVA

const bus1 = nodejs.NodeFactory('bus1');
bus1.setBase(network.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

const bus2 = nodejs.NodeFactory('bus2');
bus2.setBase(network.sBase, 1).setNode(2);

const bus3 = nodejs.NodeFactory('bus3');
bus3.setBase(network.sBase, 1).setNode(3);

const bus4 = nodejs.NodeFactory('bus4');
bus4.setBase(network.sBase, 1).setNode(4);

const bus5 = nodejs.NodeFactory('bus5');
bus5.setBase(network.sBase, 1).setNode(5).setNodeType(nodejs.typeEnum.PV);

// Generators
const G1 = netelmjs.GeneratorFactory('G1');
G1.setBase(network.sBase, 1).setFrom(0).setTo(bus1.node);

const G5 = netelmjs.GeneratorFactory('G5');
G5.setBase(network.sBase, 1).setFrom(0).setTo(bus5.node).setSn(0.6).setPf(0.8);
bus5.addGeneratorPower(G5).setUFactor(1.02);

const load2 = netelmjs.LoadFactory('Load2');
load2.setP(0.96).setQ(0.62);
bus2.addLoadPower(load2);

const load3 = netelmjs.LoadFactory('load3');
load3.setP(0.35).setQ(0.14);
bus3.addLoadPower(load3);

const load4 = netelmjs.LoadFactory('load4');
load4.setP(0.16).setQ(0.08);
bus4.addLoadPower(load4);

const load5 = netelmjs.LoadFactory('load5');
load5.setP(0.24).setQ(0.11);
bus5.addLoadPower(load5);

// Lines
const line12 = netelmjs.LinkFactory('L12');
line12.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus2.node).setZm(mathjs.complex(0.02, 0.1)).setShunt(mathjs.complex(0.0, 0.03));

const line15 = netelmjs.LinkFactory('L15');
line15.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus5.node).setZm(mathjs.complex(0.05, 0.25)).setShunt(mathjs.complex(0.0, 0.02));

const line23 = netelmjs.LinkFactory('L23');
line23.setBase(network.sBase, 1).setFrom(bus2.node).setTo(bus3.node).setZm(mathjs.complex(0.04, 0.2)).setShunt(mathjs.complex(0.0, 0.025));

const line25 = netelmjs.LinkFactory('L25');
line25.setBase(network.sBase, 1).setFrom(bus2.node).setTo(bus5.node).setZm(mathjs.complex(0.05, 0.25)).setShunt(mathjs.complex(0.0, 0.02));

const line34 = netelmjs.LinkFactory('L34');
line34.setBase(network.sBase, 1).setFrom(bus3.node).setTo(bus4.node).setZm(mathjs.complex(0.05, 0.25)).setShunt(mathjs.complex(0.0, 0.02));

const line35 = netelmjs.LinkFactory('L35');
line35.setBase(network.sBase, 1).setFrom(bus3.node).setTo(bus5.node).setZm(mathjs.complex(0.08, 0.4)).setShunt(mathjs.complex(0.0, 0.01));

const line45 = netelmjs.LinkFactory('L45');
line45.setBase(network.sBase, 1).setFrom(bus4.node).setTo(bus5.node).setZm(mathjs.complex(0.1, 0.5)).setShunt(mathjs.complex(0.0, 0.075));

// Add elements to network in sorted order
network.addNode(bus1);
network.addNode(bus2);
network.addNode(bus3);
network.addNode(bus4);
network.addNode(bus5);

// Zm part of network
network.addLinkImpedance(G1);
network.addLinkImpedance(G5);

network.addLinkImpedance(line12);
network.addLinkImpedance(line15);
network.addLinkImpedance(line23);
network.addLinkImpedance(line25);
network.addLinkImpedance(line34);
network.addLinkImpedance(line35);
network.addLinkImpedance(line45);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].U === 'number') {
            temp.re = network.nodes[b].U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].U.re;
            temp.im = network.nodes[b].U.im;
        }

        network.nodes[b].U = temp;
    }

    utils.resultsToConsole(network);
}
