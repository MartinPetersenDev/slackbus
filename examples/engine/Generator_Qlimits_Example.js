/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Generator Q-limits example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');
network.options.calcIk1 = false;
network.options.calcIk2 = false;
network.options.calcIk3 = false;
network.options.calcIk2EE = false;
network.options.cFactor = 1.0;
network.options.calcLoadflow = true;
network.options.gsAccelerationFactor = 1.4;
network.options.maxLoadflowIterations = 50;
network.options.respectQLimits = false;
network.options.convergenceLimit = 1e-7;
network.sBase = 1; // MVA

const bus1 = nodejs.NodeFactory('bus1');
bus1.setBase(network.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

const bus2 = nodejs.NodeFactory('bus2');
bus2.setBase(network.sBase, 1).setNode(2);

const bus3 = nodejs.NodeFactory('bus3');
bus3.setBase(network.sBase, 1).setNode(3).setNodeType(nodejs.typeEnum.PV);

const G1 = netelmjs.LinkFactory('G1');
G1.setBase(network.sBase, 1).setFrom(0).setTo(bus1.node);

const G3 = netelmjs.GeneratorFactory('G3');
G3.setBase(network.sBase, 1).setFrom(0).setTo(bus3.node).setSn(2.5).setPf(0.8);
bus3.addGeneratorPower(G3).setUFactor(1.04);

const line12 = netelmjs.LinkFactory('L12');
line12.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus2.node).setZm(mathjs.complex(0.02, 0.04)).setShunt(mathjs.complex(0, 0.0000056386));

const line13 = netelmjs.LinkFactory('L13');
line13.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus3.node).setZm(mathjs.complex(0.01, 0.03)).setShunt(mathjs.complex(0, 0.0000056386));

const line23 = netelmjs.LinkFactory('L23');
line23.setBase(network.sBase, 1).setFrom(bus2.node).setTo(bus3.node).setZm(mathjs.complex(0.0125, 0.025)).setShunt(mathjs.complex(0, 0.0000056386));

const load2 = netelmjs.LoadFactory('Load2');
load2.setP(4.0).setQ(3.5);
bus2.addLoadPower(load2);

// Add elements to network in sorted order
network.addNode(bus1);
network.addNode(bus2);
network.addNode(bus3);

// Add impedance objects
network.addLinkImpedance(G1);
network.addLinkImpedance(G3);
network.addLinkImpedance(line12);
network.addLinkImpedance(line13);
network.addLinkImpedance(line23);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].U === 'number') {
            temp.re = network.nodes[b].U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].U.re;
            temp.im = network.nodes[b].U.im;
        }

        network.nodes[b].U = temp;
    }

    utils.resultsToConsole(network);
}
