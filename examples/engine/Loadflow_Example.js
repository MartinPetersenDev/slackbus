/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Loadflow calculation example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');
network.options.calcIk1 = false;
network.options.calcIk2 = false;
network.options.calcIk3 = false;
network.options.calcIk2EE = false;
network.options.calcLoadflow = true;
network.options.cFactor = 1.0;
network.options.maxLoadflowIterations = 500;
network.options.gsAccelerationFactor = 1.5;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 1; // MVA

const bus1 = nodejs.NodeFactory('bus1');
bus1.setBase(network.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

const bus2 = nodejs.NodeFactory('bus2');
bus2.setBase(network.sBase, 1).setNode(2);

const bus3 = nodejs.NodeFactory('bus3');
bus3.setBase(network.sBase, 1).setNode(3);

const G1 = linkjs.LinkFactory('G1');
G1.setBase(network.sBase, 1).setFrom(0).setTo(bus1.node);

const line12 = linkjs.LinkFactory('L12');
line12.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus2.node).setZm(mathjs.complex(0.02, 0.04));

const line13 = linkjs.LinkFactory('L13');
line13.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus3.node).setZm(mathjs.complex(0.01, 0.03));

const line23 = linkjs.LinkFactory('L23');
line23.setBase(network.sBase, 1).setFrom(bus2.node).setTo(bus3.node).setZm(mathjs.complex(0.0125, 0.025));

// load2.P = 2.566; // 256.6 MW / sBase => 2.566
// load2.Q = 1.102; // 110.2 MVAR / sBase => 1.102
const load2 = linkjs.LoadFactory('Load2');
load2.setP(2.566).setQ(1.102);
bus2.addLoadPower(load2);

const load3 = linkjs.LoadFactory('Load3');
load3.setP(1.386).setQ(0.452);
bus3.addLoadPower(load3);

// Add elements to network in sorted order
network.addNode(bus2);
network.addNode(bus3);
network.addNode(bus1);

// Add impedance objects
network.addLinkImpedance(G1);
network.addLinkImpedance(line12);
network.addLinkImpedance(line13);
network.addLinkImpedance(line23);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].lf.U === 'number') {
            temp.re = network.nodes[b].lf.U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].lf.U.re;
            temp.im = network.nodes[b].lf.U.im;
        }

        network.nodes[b].lf.U = temp;
    }

    utils.resultsToConsole(network);
}
