/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Parallel Lines calculation example

'use strict';

const networkjs = require('../../src/sdk/network');
const nodejs = require('../../src/sdk/node');
const linkjs = require('../../src/sdk/link');
const mathjs = require('mathjs');
const calcjs = require('../../src/engine/calculateNet');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// Config
const useAPI = false;

// Create network
let network = networkjs.Network('Example Network');

network.options.calcIk1 = true;
network.options.calcIk2 = false;
network.options.calcIk3 = true;
network.options.calcIk2EE = false;
network.options.calcLoadflow = true;
network.options.cFactor = 1.0;
network.options.maxLoadflowIterations = 500;
network.options.gsAccelerationFactor = 1.5;
network.options.respectQLimits = true;
network.options.convergenceLimit = 1e-7;
network.sBase = 100; // MVa
const vBase = 0.4;

const bus1 = nodejs.NodeFactory('Net');
bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

const bus2 = nodejs.NodeFactory('Bus2');
bus2.setBase(network.sBase, vBase).setNode(2);

const bus3 = nodejs.NodeFactory('Bus3');
bus3.setBase(network.sBase, vBase).setNode(3);

const bus4 = nodejs.NodeFactory('Bus4');
bus4.setBase(network.sBase, vBase).setNode(4).setSinglePhase(true);

const bus5 = nodejs.NodeFactory('Bus5');
bus5.setBase(network.sBase, vBase).setNode(5).setSinglePhase(true);

const feeder = linkjs.LinkFactory('Feeder');
const zk = vBase ** 2 / 0.0178;
const angle = Math.atan(10);
const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
let zm = mathjs.complex(new Complex((vBase, network.sBase, strZm)));
let z0 = mathjs.multiply(zm, 230);
feeder.setFrom(0).setTo(bus1.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0);

zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '0.363+0.003i'))); // 30 m * 12.1+0.1i ohm/km
z0 = mathjs.multiply(zm, 3);

const L12 = linkjs.LinkFactory('L12');
L12.setFrom(bus1.node).setTo(bus2.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0);

const L13A = linkjs.LinkFactory('L13A');
L13A.setFrom(bus1.node).setTo(bus3.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0);

const L13B = linkjs.LinkFactory('L13B');
L13B.setFrom(bus1.node).setTo(bus3.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0);

const L14 = linkjs.LinkFactory('L14');
L14.setFrom(bus1.node).setTo(bus4.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0).setSinglePhase(true);

const L15A = linkjs.LinkFactory('L15A');
L15A.setFrom(bus1.node).setTo(bus5.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0).setSinglePhase(true);

const L15B = linkjs.LinkFactory('L15B');
L15B.setFrom(bus1.node).setTo(bus5.node).setBase(network.sBase, vBase).setZm(zm).setZ0(z0).setSinglePhase(true);

const load2 = linkjs.LoadFactory('Load2');
load2.setP(0.0023 / network.sBase).setQ(0);
bus2.addLoadPower(load2);

const load3 = linkjs.LoadFactory('Load3');
load3.setP(0.0023 / network.sBase).setQ(0);
bus3.addLoadPower(load3);

const load4 = linkjs.LoadFactory('Load4');
load4.setP(0.0023 / network.sBase).setQ(0);
bus4.addLoadPower(load4);

const load5 = linkjs.LoadFactory('Load5');
load5.setP(0.0023 / network.sBase).setQ(0);
bus5.addLoadPower(load5);

// Add elements to network in sorted order
network.addNode(bus1);
network.addNode(bus2);
network.addNode(bus3);
network.addNode(bus4);
network.addNode(bus5);

// Add impedance objects
network.addLinkImpedance(feeder);
network.addLinkImpedance(L12);
network.addLinkImpedance(L13A);
network.addLinkImpedance(L13B);
network.addLinkImpedance(L14);
network.addLinkImpedance(L15A);
network.addLinkImpedance(L15B);

// Calculate network
if (useAPI) {
    utils.apiCalculateNet(network);
} else {
    network = calcjs.calculateNet(network);

    // Turn back into complex objects
    for (let b = 0; b < network.nodes.length; b++) {
        const temp = mathjs.complex(0, 0);

        if (typeof network.nodes[b].U === 'number') {
            temp.re = network.nodes[b].U;
            temp.im = 0;
        } else {
            temp.re = network.nodes[b].U.re;
            temp.im = network.nodes[b].U.im;
        }

        network.nodes[b].U = temp;
    }

    utils.resultsToConsole(network, 4);
}
