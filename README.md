SlackBUS.net - Easy and intuitive calculating of power networks.
Copyright (c) Martin Petersen 2020 / @MartinPetersenDev


INTRODUCTION
------------

SlackBUS.net is a javascript application, which provides easy and intuitive 
shortcircuit and load-flow calculations for electrical power networks.

The intended use is for MV/LV networks and electrical installations, although
there is no actual limit to the voltage levels used.


LICENSE
-------

SlackBUS.net is released under the Apache License version 2. 
You must read the terms of the license, before using SlackBUS.net.


WIKI
----

Please read the repository Wiki for instructions on using SlackBUS.net. 
