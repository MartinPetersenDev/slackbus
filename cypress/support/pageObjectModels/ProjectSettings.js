/// <reference types="Cypress" />
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * Page Object Model for menuproject settings
 */

export class ProjectSettings {
    onLoadflow() {
        cy.get('a').contains('Loadflow').click();
        return this;
    }

    onShortcircuit() {
        cy.get('a').contains('Shortcircuit').click();
        return this;
    }

    onProject() {
        cy.get('a').contains('Project').click();
        return this;
    }

    checkAll() {
        cy.get(`input[type="checkbox"]`).check({ force: true });
        return this;
    }

    /**
     * @param {String} unit - 'unit_mw' or 'unit_kw'
     */
    setUnits(unit) {
        cy.get(`input[id="${unit}"]`).click();
        return this;
    }

    setUnitskW() {
        cy.get('input[id="unit_kw"]').click();
        return this;
    }

    apply() {
        cy.get('Button').contains('Apply').click();
        return this;
    }
}

export const onProjectSettings = new ProjectSettings();
