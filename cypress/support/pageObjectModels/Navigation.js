/// <reference types="Cypress" />

/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * Page Object Model for SlackBUS navigation
 */

export class NavigationPage {
    projectSettings() {
        cy.get('a').contains('SlackBUS').click();
        cy.get('td').contains('Project settings').click();
        cy.get('a').contains('Project').click();
    }

    generateReport() {
        cy.get('a').contains('SlackBUS').click();
        cy.get('td').contains('Generate report').click();
    }

    about() {
        cy.get('a').contains('SlackBUS').click();
        cy.get('td').contains('About SlackBUS').click();
    }

    calculateNetwork() {
        cy.get('a').contains('SlackBUS').click();
        cy.get('td').contains('Calculate network').click();
    }

    /**
     * @param {String} element - Element type to search for
     * @param {String} name - Name of element to open datadialog for
     */
    dataDialog(element, name) {
        cy.get(element).contains(name).rightclick({ force: true });
        cy.get('td').contains('Edit SlackBUS data').click();
        return this;
    }
}

export const navigateTo = new NavigationPage();
