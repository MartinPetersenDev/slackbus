/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

'use strict';

// Query parameters
const queryPrefix = '?local=1&stealth=1&mode=device&libs=0&format=0&edit=_blank&layers=1&nav=1&title=cypress-test.xml#';
const queryPrefixNoSplash = '?splash=0&local=1&stealth=1&mode=device&libs=0&format=0&edit=_blank&layers=1&nav=1';

module.exports.prefix = queryPrefix;
module.exports.prefixNoSplash = queryPrefixNoSplash;
