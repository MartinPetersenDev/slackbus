/// <reference types="Cypress" />
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test checks content of the project settings menu
 */

'use strict';

import { navigateTo } from '../support/pageObjectModels/Navigation';
import { onProjectSettings } from '../support/pageObjectModels/ProjectSettings';

/**
 * Generic test helper function
 *
 * @param {Array} elements
 * @param {Array} asserts
 *
 */
const projectSettingsTest = (elements, asserts) => {
    elements.map((element) => {
        describe(`${element.name}`, function () {
            asserts.map((assert) => {
                it(`${assert.id} ${element.name}'s ${assert.description}`, () => {
                    // Setup
                    navigateTo.projectSettings();
                    cy.get('a').contains(element.name).click();
                    cy.get(`input[data-cy="${assert.attributeValue}"]`).each((input) => {
                        cy.wrap(input).clear({ force: true }).type(assert.testValue, { force: true });
                    });
                    onProjectSettings.apply();

                    // Assert
                    navigateTo.projectSettings();
                    cy.get('a').contains(element.name).click();
                    cy.get(`input[data-cy="${assert.attributeValue}"]`).each((input) => {
                        cy.wrap(input).should(`${assert.should}`, assert.expected);
                    });
                    onProjectSettings.apply();
                });
            });
        });
    });
};

/**
 * TEST
 */
context('Project settings dialog', () => {
    before(() => {
        cy.openDiagram(
            'RzVdfl6I6DP80nn3aOVDE1UfBUZlVd9Y%2Fo%2FJWKINVoF6oInz6m0JR1BnP3ftw5zowNGlCfk2TJjQ0MzwNYrzfjBnxggZSyKmh9RoIqU2EGuJSSFZyms1WyfBjSqTQhTGjuSeZiuQeKPGSK0HOWMDp%2FprpsijyXH7Fw3HMUikmX%2FfOgmure%2Bx7d4yZi4N77pISvim5bfTjwh961N9UltVWp5wJcSUsTScbTFhaY2nPDc2MGePliDlbsQCkBNgBJxZypWblqItWeDK9ABzdL0kYSO2rWYXerjX2Il4H1f9EIV85zb564j8JerFnf%2FUcdx58VzW5EJ5V3vEIOEuSEYvgYWx4KMCrMPROlK9grDzpklpfUT0RJUpFZBUR8Thb1YmaliAvagVV6SUcx7wrtvwCxovIDefeE9I5CTvErvdo%2BTL6cOx7%2FJGc3DXhm5oF6eiBx0IPYINA7AWY0%2BN1oGEZr%2F5ZDtW3We7UpwHT9zzixQ0ESaapwiuLCASKOUVV4AfPt9q05ZynlScx2S1hzALs7ozF7PvRixPKIonxScYyz%2FbetUHBneDwA%2B7jmLoL6Xp4QcoUdmhYZKkBYDiF3ByJBb%2ByhPISmsM4ZyEIFJ4wALofs0NETBawuHiV1moZqN%2BvvaMbUF%2FocrYHLk725enxTk8eADYKk92Kq1QcGBPMcUPrliTqJ0e%2FgYwTBD0yX4cTZGdG01meDm6uUDycKm6PHUca0Uima%2BNMP7qhexxvu%2BnY7OQkdKk13HBnoOe%2Fok2Cl3r8OnthZDhNf9H2EbS0UeTmo7CT2Vn79Gu%2B00daKWdRA9mrlxwvO4fXmXUabZ%2FhXWRvD6cMaDSOjCbIMHsZRHj4u2Nt15k79EHG3tsrYjqa37Goko62C3807x4msy4aU3FbvqtNdWew6Fjhy2aNeOSGHdUJf%2FM16iSOZrWs4Uu0pn42mS8Sq2epk1nqj7cL9ec8oa%2Fb085e2vkob7ZtuN3wbUtMowmYUsCjy6eGl1MF9xQ62XapNdgEeEkYOdP23hmkgG8S2sXfGnBPFMAR23OFWjn4ryd8CFgHnb0TTQOXqgEJ3w5kOAbc%2FcQxrSNgSderKbMGsP6d4o9NWJ9pjK3eOrV6NTuhfnTCxec2tGnmIB6MluqeDN4yZxBwvBI%2BfRZrSR0UHAR%2FtHxD9lJXxN5XOhZtQ3y8Do0NGfi%2BDSucC0%2BfkXR9YcEOg8SBOSfsHOzZxSJ4a1uggBss5WT4csRowZ1loMDuQyQAItAbz2E1Q%2BMIkQF8i9srOwCv5BBNlQ4VO%2BIO%2Bgo2jR3wJ1ZvnBaeeE598Lo%2Fzr%2FQI2mBBpAAiv%2BFR2CH8q%2FDsf4yf4yQK3anPJ1gBCffntGIF3VJN%2BASpRiZ4r%2FeE5dmvNMgqJ2178WvKMkx23kfncIeoRw7xTkvztZ3FvE%2BDmkgink3pjiQTNkQquqj0i2OdO%2F0sNbK2XNbKbtRpEk6vfR2LVm%2FN7W2ruL9QXW%2B6ck%2BKtYT6JZF%2Ba1V6%2Bti%2FSTLtSKMIEX%2F8UMTvr%2Bu4Ltv37RbtaLO77o3Yugs1m61lCekNz%2BSUj992XXljwr0%2F6BZuLQFk7PK46YAPewKitIuinTRZNb6TRy7Ml505T4qz7F3G85FMCMRycg8080bunVDtyV98d15Sj%2BLqh%2FS6IZu3tCtG7p9zjNCYwii0sOpl%2FCq46m1Qi4kB3Ren%2FdKst%2BR3c%2BdeNUW%2FUlLdZPMn2f9f5Xm2m2aK%2Fdprt9nuVp9qv37NAfy8kVXiNa%2Bi7XnvwE%3D'
        );
    });

    describe('Display units', function () {
        const elementTypes = [
            { name: 'Feeder', element: 'text' },
            { name: 'Node', element: 'div' },
        ];

        it('Changing display units must work on new project', () => {
            // Setup
            // No setup of project settings, must work with defaults
            elementTypes.map((obj) => {
                navigateTo.dataDialog(obj.element, obj.name);
                cy.get('input[id="un"]').type('10000');

                if (obj.name === 'Feeder') {
                    cy.get('input[id="sk"]').clear().type('123000');
                }

                if (obj.name === 'Node') {
                    cy.get('input[id="p"]').clear().type('2200');
                    cy.get('input[id="q"]').clear().type('1100');
                }

                cy.get('Button').contains('Apply').click();
            });

            // Assert
            elementTypes.map((obj) => {
                navigateTo.dataDialog(obj.element, obj.name);
                cy.get('input[id="un"]').should('have.value', `10000`);

                if (obj.name === 'Feeder') {
                    cy.get('input[id="sk"]').should('have.value', `123000`);
                }

                if (obj.name === 'Node') {
                    cy.get('input[id="p"]').should('have.value', `2200`);
                    cy.get('input[id="q"]').should('have.value', `1100`);
                }

                cy.get('Button').contains('Apply').click();
            });

            // Change Display Units
            navigateTo.projectSettings();
            onProjectSettings.onProject().setUnits('unit_mw').apply();

            // Assert
            elementTypes.map((obj) => {
                navigateTo.dataDialog(obj.element, obj.name);
                cy.get('input[id="un"]').should('have.value', `10`);

                if (obj.name === 'Feeder') {
                    cy.get('input[id="sk"]').should('have.value', `123`);
                }

                if (obj.name === 'Node') {
                    cy.get('input[id="p"]').should('have.value', `2.2`);
                    cy.get('input[id="q"]').should('have.value', `1.1`);
                }

                cy.get('Button').contains('Apply').click();
            });

            navigateTo.projectSettings();
            onProjectSettings.onProject().setUnits('unit_kw').apply();

            // Assert
            elementTypes.map((obj) => {
                navigateTo.dataDialog(obj.element, obj.name);
                cy.get('input[id="un"]').should('have.value', `10000`);

                if (obj.name === 'Feeder') {
                    cy.get('input[id="sk"]').should('have.value', `123000`);
                }

                if (obj.name === 'Node') {
                    cy.get('input[id="p"]').should('have.value', `2200`);
                    cy.get('input[id="q"]').should('have.value', `1100`);
                }

                cy.get('Button').contains('Apply').click();
            });
        });
    });
});

describe('Project settings input fields', function () {
    // Array of elements to test
    const elementTypes = [
        { name: 'Loadflow', element: '' },
        { name: 'Shortcircuit', element: '' },
    ];

    // Array of assertions per elementType
    const assertTypes = [
        {
            id: '(ID 2.19)',
            description: 'numeric inputs must accept numbers as scientific notation',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '1e300',
            expected: '1e300',
        },
        {
            id: '(ID 2.20)',
            description: 'numeric inputs should ignore special characters',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: ';',
            expected: '',
        },
        {
            id: '(ID 2.20)',
            description: 'numeric inputs should ignore letters',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: 'i',
            expected: '',
        },
        {
            id: '(ID 2.17)',
            description: 'numeric inputs should ignore negative values',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '-1',
            expected: '',
        },
        {
            id: '(ID 2.18)',
            description: 'numeric inputs should accept commas as decimal point',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '1,1',
            expected: '1.1',
        },
        {
            id: '(ID 2.18)',
            description: 'numeric inputs should accept commas as decimal point',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '0.9',
            expected: '0.9',
        },
    ];

    // Run dialogInputTest tests
    projectSettingsTest(elementTypes, assertTypes);
});
