/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test checks label generation and content
 */

'use strict';

import chai from 'chai';
import sinonChai from 'sinon-chai';
import { navigateTo } from '../support/pageObjectModels/Navigation';
import { onProjectSettings } from '../support/pageObjectModels/ProjectSettings';
chai.use(sinonChai);

context('(ID 2.13, 2.14) Labels', () => {
    describe(`Labels`, function () {
        it(`Should show the selected information according to project settings`, () => {
            // Setup
            cy.openDiagram(
                'R7VxZd9q8Fv01PCbLI8MjmABOgVCGQHjpErYxAtuithl%2F%2FZVsyRNjKRn63aRJyjmapb2PtmS3OVGxt3UXLGctpBtWTuD0bU6s5gSBlwQhR745fRd6xKIUOkwX6jRT7OjBvUGdHPWuoG54oY%2B6fIQsHy69VGkNOY6h%2BSkfcF20SZedIivd6hKYxoGjpwHr0DuEuj%2BjXj5fihMaBjRntOmiUAgTbMAy06a9GdDRJuESn3Ki4iLkh5%2FQZE4GIHAWmOBJDPLRsQFLUz0tdE2B5Rmxv4mAPrVYvYlEb4Y2r3iiovFl0sqOaR1P6c2Q62vQ1VbQP5qhgzaGezRFWbmu4Rwv1USex1YykWSDLe3mELgOdMz0%2BuNkNkTVN1zgQ%2BTQSgSZ46LFXxuuaTia0YQ26zVvPBTi5quGp7lwScof9GHpIjL3bWDTGUlNgcA9bYG9tGhmHXpLC%2BwGDvTTECSt%2FIKLXzZ0cK8PVytyMsjijCkHI0SMDnurGBYmVC008QeKklQqB7OYjhchnKHaiQK%2F4UvjxXkBtU371esPfoxbbvmBItvzdwwgho45QU08MTNkIgdYT7G34qKVoxukVg5bcZ4mQkvs5LFzbvj%2BjhIcrHyEXTPftmiqsYX%2BKPH5jVT1KFOruqU1B8YuYXQMF9qGH%2BAx8Dm%2BuxsljURNxIyrCixW1%2BHMsVVFK1ej81CTp0XRekbrJny2jGK%2F%2FfOl8iDSqAQwAGnR7nPbszn9qdufV7Ybaf57%2BrZl%2BchcJhqg61I3EB6Fu8MZXMPCKF%2Bn4w%2BgUDOjfFHRDoIOASkFlyjRqEEDrlCU01WEA6KlktjIVCQxdp2qKBzxQUX4Q2I8sSuA3h%2FAsPjPwPBGyAVWFr9X4fAovoRDHJ7D63vjUODk%2B%2BAQh%2FlPxSHPfyYQY%2FC9JxRvRx1%2FZfQTPgZ1vHABLFej7uOi31qXV2LFdI25Ol%2F%2BMMWm8lJ8YPrnK4Q%2F4V8If9KV4a%2FwMUCUCwfh75FLfPG3wTJ%2FiO9z1b43SD81NL6XVBTeTSoWrgyW8sdgtMDfSSoWpPOoFKU7wfLYGbmPx42bz%2BcEkSfLM3CITZK4fDFIquHfZKjc4jWRrxfn47GTa72W6fLslnRG%2By5wvClybYyZIKlnAW1RGfQe8HHTi06S%2FCML1qtJXLjs7EJvfKzs0yUaODylc5E5BLZ%2FhY51BXhsWWmhHjXzFByLdPKoSwMPXcOx4aIHz%2Fi9IqfiMAn3N0iaAW8cjjx5PiU0S8Bo6iKbriYdXNyl6OA9enjINHvydMwCyWlNmg48iZiyJEAx3Kc1JpxHiY8XwIcajhoECB3kQXqwnyDfJx2vRNctZK114M2ikAMsaJKsmuEEwSAZW1i1ZZrHJzGJ1BWsqr01yS3Xo2FhFLok3yN09JXmI9d7XCIfVwiB9cuPYfOLBJMArBWMHDOIfQqyUHh9IpYKVa5QIC34LloYx1KmyPFrwIYW4VbZxfVTJ42PPH8uJJHxGMlrh8MgwgRPlsN5am%2Fia68ShcIsceHFih0LOylixyzOXGMcI3WT6cqITk3oGHchIau6aThmdEX3SJ1dmznoUEd2NgdlDk1OWW9uytomrSOMZowT%2F4Bxv445byOdfJZ0lzdyw9HL5IYVZ3CQY6SJFAKaXZkKqX2fTzLyo0%2Fdp5l2BU3%2FjowXNey1V0nSlfogQV2eYeUvNUP2VkjiM%2Fy%2FVjPIlyq6WSRcuiUdD2bWrGZBcT1ae9pDY%2B115S8lZYv%2F6Hnr6MSWrsTq1bg8sakclYbCSWnI85E05B6ls%2BKQbAUfog5p2I%2FUIR%2FJRYFtQuxyN7GbxE6mENnexSQiFZlMIRapfZ1C9N1VRiBK2e1KPL1dxQKRtXrbXlU4u1f9dwViraYopdKxbStKmULLSvqLypOi5MhZHegQN8jS6B69xGFKQQ6uD%2BBJ6yIf0Inhg4AWmdwHas88n9GeMkXw52hPIcv0%2B2lPWvUF7cmzh8ix%2BiT6I6U%2FccDnGLEjl8hcVyrROHYwcstZckunyX1HLcofPvP8L4jRS1dJp1l9RUh4ZzEqH27wR%2FOx%2B4hLGzzF4AOGtySUDvh9L4FaKGV0pXyjQC1JcraiWy614npZRjSdesaHaNov9AzhftezxXtcz56Tqh9%2FPfsnklb8pyQtlYb%2F55L2NNq%2BJe39d7bbdafE5NOn6M6aYeiEd0efZMiUzleQMmYkqzFNyqR3QAvn5QMSMldvkSVAl%2B3Yp0mSeZgwmAICmJQv5hGr6PhziZPsUhdiUtYqyOvMYNKjLvikydh4%2Bj2Gk2xkZIB28DrlZSaehn8%2BXxFqtZO0A94yfI11CreEvZWgyTLzcsyDP%2BvABzmxHJpCzVubOaGyxbQWlE6jLYx3FWky3K60PQdBo8tpVSy5RV3Ud7LY2slrzdbWrXl501JKe93WoNqY%2BZO6vH9xZh4Yym6n94z0RnfzAotrXEpsOtq%2BaZd2411x%2B9JfyE0xzKfCijAePe%2FBsLTq9NRtc%2F6E69KX40YXYVtoORUJ50HjoeWAxs%2BSOn%2FbaQ0T5xkvxyNdmYhmSYXcpjkfmM1%2BedXulYUWJD%2BqqYldeVIflFT7efYm%2BI5ml%2FiJ%2FdN%2FE0reRFTzauPZeYPmrt0feGpV5du9jdmaD%2FgffQ925tvFeDjeN%2FdScYx%2FNPt1risVCfdpg%2Fsj079FMOxyoMrB9rwM1frMAkMd6ZE9Xk7qG9y%2Ftj0O%2Frzhfrc53A933OegusfzVyVziPtaLy0nTtfSIG%2Fp9utKb7Rwv2veRFHXuC%2Bbt1EXqXU8%2FgVnthQ8PqXSUqtvG7WaaMeW1xN7cLoNsbubCL7VHPJLvf66m9QtH4zInD6RsWwmgrUi%2FubwVRgPZY6sPSujQhzcKp1GZabXTXOMR9gnMx31pGySFsa25U1w2sQurca9uEU8W%2FOgF%2FgHt7TXG89rIAz8ydDi8OpjJOAe4XKtPh5No7LGyMB%2B1R%2BPxhaelT1GEysDyYpo9RoHlMoC%2B9tqtbUJZuJpY%2BJZN1v7T5yRTdAb3BPciy8xI3iF9p%2FXj7dPm4%2BmoJHVCaMT%2FhTcXcFQDeXkCv4mpwpBIb%2FlKvk%2BuAubBl%2FHpEYUhQ%2BE0Udpj0Lmyks6PBLnj0gP5ruv9Gi85oQD9eAg%2FW9uvaAXFMtK7liHNF6v1yCkpsmKHaCppA86mDrh%2Fcl9lAfJv4DozKIiCc3RoeeQ0PqZss7KCOGsjEjfOiUkOHA1Ci%2BZO4RxBNYs%2FgP0CwT6ghLZUsbOZ%2BwitWOdGSXJUVb%2BqC1kbClj5zN2MSKmlRFKEd1OKSmqhrLszIqmPxFcp4PAR3Gez9xNRZhKkF4%2B5Hz0Uut9SV9%2BZ84n%2BBSTvpzmPLsPOPa%2B0wXOCzdw%2Fix3z7%2BEdCN3o1cHvrl7A3e%2F0PtRYvErcbfyGdyt3I27Z67XbuOu9M3db%2B6e5K6cfbfxOu5Gr0Del7vKZ3BXSXP30rX7Bf6eeSh8G3%2FPv4t4I3%2Bj%2B%2BRv%2Ft7A3y90T1%2FIfyX%2BVj%2BDv9W78le%2Bgb%2FXnJfZYzp2Yn7krrl7z3%2BT%2F5v8J8lfymzeopC%2Fhvx3EN7YjP97jvAdjfg%2FORGf%2Fgc%3D'
            );

            /**
             * Baseline
             */
            // Check elements are present, and labels are empty / at default
            cy.get('g').contains('Feeder').parent().should('contain', 'Un = 65 kV');
            cy.get('g').contains('HV').parent().should('not.contain', 'U = 65.000 / 37.528 kV');
            cy.get('g').contains('L1').parent().should('not.contain', 'Ib =').and('not.contain', 'dU =');

            /**
             * Test calcLoadflow and not calcShortcircuit, only shows loadflow
             */

            navigateTo.projectSettings();
            onProjectSettings.onLoadflow();
            cy.get('input[id="calc_loadflow"]').click();
            cy.get('input[id="show_voltage"]').click();
            cy.get('input[id="show_current"]').click();
            cy.get('input[id="show_losses"]').click();
            onProjectSettings.onLoadflow().apply();

            navigateTo.calculateNetwork();
            // cy.wait(7000); // could propably be made async and wait for calculation result

            // I have no clue why these change from <g> to <div> after calculation
            cy.get('div').contains('HV').should('contain', 'U = 65.000 / 37.528 kV');
            cy.get('div').contains('HV').should('not.contain', 'Ik');
            cy.get('div').contains('L1').should('contain', 'Ib =').and('contain', 'dU =');

            /**
             * Test calcLoadflow and calcShortcircuit,shows both loadflow and shortcircuit
             */
            navigateTo.projectSettings();
            onProjectSettings.onShortcircuit();
            cy.get('input[id="calc_isc"]').click();
            cy.get('input[id="show_shortcircuit"]').click();
            cy.get('input[id="show_ik_minmax"]').click();
            onProjectSettings.onShortcircuit().apply();

            navigateTo.calculateNetwork();
            // cy.wait(5000); // could propably be made async and wait for calculation result

            // I have no clue why these change from <g> to <div> after calculation
            cy.get('div').contains('HV').should('contain', 'U = 65.000 / 37.528 kV');
            cy.get('div').contains('HV').should('contain', "Ik''3").and('contain', 'Ik max');
        });
    });
});
