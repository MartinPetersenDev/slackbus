/// <reference types="Cypress" />
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test validates  label results for loadflow and shortcircuit against known results
 */

'use strict';

import chai from 'chai';
import sinonChai from 'sinon-chai';
import { navigateTo } from '../support/pageObjectModels/Navigation';
import { onProjectSettings } from '../support/pageObjectModels/ProjectSettings';
chai.use(sinonChai);

context('Loadflow Validation', () => {
    describe(`Loadflow Test`, function () {
        it(`Load flow calculation results should match known results`, () => {
            cy.openDiagram(
                'R7VxZd6JME%2F41XsYDDbhcuiRKXnWcGDXxJgcBsRMWP8D113%2FV0M3qls3knHEmi1303lVPPVVACkLD2rRcZTHvOppuFhCnbQpCs4AQXyoL8ItItqFEqkihwHCxRivFggHe6VTIUekSa7oXyqjIdxzTxwsv1Vp1bFtX%2FZRMcV1nnW47c8z0qAvF0HOCgaqYeekYa%2F6cSvlSNb7Q1rExp0NXUDm8YCmsMh3amyuas06IhNuC0HAdxw8%2FOdNXsgDEmcoUNjGoR9emmKrsqaFoppieHss7jqLNTNZv4qI3d9Yj2KhofZlrNdsw918ZzB3XV7GrLrG%2Ft0LfWevu3iuNpevq9v5WHcfz2EkmLlnKhk5zrLg2to30%2BcNltkTZ113Fx45NO0ESx0WHv9JdQ7dVvYMtNmtevynHwzd1T3XxgrQPL%2Fvukk5h4Tpk63uKRTekqVsOyG83irUwaSUNewtT2Q5t7LPx485f8NuLhW2YbP6QIiFfZLoKVakeFKvUHJgxxJphbRq6CcZ0FxbhA9WQ1FUOZ%2FU5PoBwd%2B4ONLivqP%2FbetX%2F6q8i36rPBt7LcndDrdXzt0w5dA3sgRZtx4Zf9blvEf3k4aO%2Bwf4TfOaKEi09p0pNsnaOFbasYPvu9ol1QQqJVqQYNwtKrJ3nK65fI1YdT0a3tYwkvxF0bzxn6ar6kdXTejCKoftH6tGTJ1uTGIBuc0t3LB1mDRVc3QSFXaWhRKHqY0T1UPKQ6TkdhIQezAyVCkjgyY4MbbgaXOB4jiP2wI3CYQamor7Vh4MbsAwvUnq%2ByEB0u6DTutN1Dcw5kMYmEAxDREPWMuw%2BEM0U1Xfc1KoOmof8JiSxrOF4%2FTlOSuQ3PlkcvCXGowM%2B3dw8pGFhVVc8PTcx30nN6aimJ4%2BMWkZC6wGsg%2F3BVoCfddhEHwPedshB9B0PUxyZOr7vWFAhOKE6bLnhOktbazgm3SChVKqju7tEHzUTG6St7yxAqniL0G%2FN8EaHCdeDIWtMyjEJfNYUXykItbCI7ryVUUD1DdgiavTbPTTZ1sXpeLNUdxxW2g%2Bc2nRWHUETtK0kdLfSSrXUVfe1tu42qjvNUrHcnvvTlrT7Y889ZSy5%2FcG9o7Uf1n9wZQWthI6t7jpWdTvZVjZ%2FHt%2BkjhDWk3EdTZ7ud8q4uuwP5E3n9Rb60haT9oMDZdS16yLUcSZj01baf6vy6%2FNWbRtQZ7KYPGmNqWBUZcytO69Do%2FNYW%2FYGNdTF5Fs2VOFBmraGVdm6nz8j31atKj%2B1%2FvrPqOpNBbkkt%2B%2FtZ2xse49DT27KfG%2BwNrqvQ%2F6%2FRw%2F3Xzdvk%2FFk19mJlQl8q9boVWvURZjTGuYj0d%2BCMn7glCaHe681LLfmpjLWHC0qTxbT1hrm17Mmwf9nmHcPILvqTh45LO9g%2F5pkD2Gurepiaj%2BYKuZNzRottXYX5n3nTRvyCuayfn56cOQWrP%2BNM7oNWF%2Bj3pWbz2u5mRjHklZTa3h4DOFhO0W%2B2RnzC6012k5b4CqfyJ7ekrWsp8hcEnlnPEKTscSRs2dtZFwB%2Fei363OtZRgTWOEj2eloJjWDjDCxTG8K16ZWdTkZxCPCbr0Gs4BvGGmnte9XChr607HJwemDJsCMoF33EVbTrq9AM0Au%2B5OniQm7sgNtYm0wORG1dccpjfobyHtys7sOduJ2bcCuG93dD%2B7IOpgNzARm8St2BE5o93PzeP6x%2FegglZxOiE7wCZBv4WA79CdSHb4IQ0AN8lNqki%2BhPsOmmcDaWfAvYAqu86bvQ2Fdw74yDXCeYOvMsf07xcIm4Rg1FysmFdJQhOePMQoC6fqmcIwD0Ktl6qJoGCSIlPet46CiRD3gPBFOMNk7SEOGKO7jEN1RAeV4gA2x21nUwVtO41Y1e5ulDt3RKcaMSezRn0cuPEGX99AN7AUTyrL2fiq6%2BpsqBWs5mwqgo1Qg8OfEMwdsNcF9FVelSiJxeVWMFC6rw4EGI6K%2BqBGVxUy5lClXaDlmftElKarK7y2jTFnMlEuZciUyLg27oDqhAqx1z2c0J8F%2FVLAIEgseJEiU5FDKk6vOuNB7eFTGgg%2Bb%2BqVsm69mjJsv5Yxbyts2z7T148Z9XlAnfTSoe09IB4W%2B7mJYQJAbSIV5UeF3hHniLwvzzjvF8gdPkf%2BqU%2Fxlwfq5p1j6wWD98YEPPO2xeJ30hziRxe5R1UFcNUh0cW%2BjWtZnP7qK7c0c12IB%2FCdddzDf0A3z%2BbDfRqFMZJIBxTZW1qmTFrNhu1CkpwA%2Bf8JlPTkL26m9zVziCZJIHAf70dAHycVRGxKPevrAU%2Bvu7UoPHTZ%2FTuAfpXXJmWmKNw%2BoArfP6SWMc38ugGUdrI1BsulF3QSNckm9Ira1JUm6eMWF40OH4Mxe%2FPj0X9Ae13bYl1bLTa5cPsOXRhX3%2BtIv8J1IyPhOKe87q3uIsfhp37nPXjvfSow7o%2FP5r7iX%2FSYqnkd%2F0WespfQdvDjSqCsv%2Fhpe%2FEW2%2FA28WCj9FC%2Fea9sYCAe43YSuNP9AQ04NNg1c8BMvET9b6ySqdHTbgLnGnhh%2BWFmECLv%2BMELs82%2FMJwpZn3jKonP3uQ4vMoNO0f4QOV039e%2Bh7IFOgSvyVXqST5GIK1Ov%2F8AxUbnCiEAkQqxhKwVY9VRJsaa%2Bbi1CWSiylE0s4pleHUWvylH0yjPRBIKFpsPu9xLXmga7yM3zebP7UPD0QYp9Bih8FwqcpOf0nE%2FSc3of%2FSQ9T2AGz4zkbMpOu%2BsTlxPjk8hn8KlcSncRLpK2QolbqZmOpFMdhbuQ6ygAsWiNH8S1XkBPOP47mUs8xoX5C3t65EP8pXrlL1f%2B8gn%2BIv0q%2FkJtEF3AztHX23nKzLNWLn7Gyvn8IzNXM7%2Ba%2BflmXpHSZo4q3M%2BbuRCYeZ0wcUfRLmDz2QG%2FGgAEKQUBUhYDyp%2FCgONP81wx4IoB78MAqfwLUhXoWKqCBPGN4eFMBZnuRTMV1InHmYpT3P10poKtcW%2BigvaSTFSwI2KJCr5YkagqJBIVFQo1LFFRLrJkcpynEMRK4UJ5Ch4dxa4LJyoq%2F1yigiYgTiYqWIz%2FE5mKUvYuyUczFeVTHX1npoIarnAE1k4mYIVLJ2DLXw5rx%2FOvQh7W2JnF%2BVepUsnBWrmShjVUFCSUwzUeXQzXhN%2BEa%2BiKa0e58xm4tolUDaUw5IY92%2FgNwCeVpTReSRnCdS7wVTjueEcHgA90VNkmqtFQ4OCEy2J6HJEa3EFAztQvCaUM7oYzyLRm03FmM0%2F%2FtpyyeIG4k9LGXKgZPx7y3kgTgK%2BQDDVzSWXpBIIfh7Xjz5BcQ81rqHniaVGUuVv0s%2FkmSjvEg6RMeOLJY%2FdHg82L3xaXsqzsVAr5BCtLLnIvLRPztCx7W5wH4GE3vOM75VyGlonVYu6uuChcjpVJv4mV%2FXu3xZntn6Zl4pm07FezruwdtDNZ17tJzHmPUPM0mFsp5pLuzQicjRa8VV7IvPCdsApAdj%2FjzU%2B5KAtrGmled3UP7xKGkHbwxHEqSx88YKhcWQVs6%2BZKJx1nlLBKywkN5sIXZmk9Ovk9b4W%2F22Flz3AP22cvQCT9lfB%2Bd3WEikoXoKLsPYkLUtHSCbfF1HmIvc1s9Kw0LaunCy%2Fjm83t3YlXmK9M9B9gojkr3mPr5zPR6EH%2Bn2Si0qeYKH9pJkot%2BHJMlB5Ikonyv5yJ7kUvdBS9runBd6BAhiN%2BHTM96Xa%2Bl4dWSl%2FEQ%2FlId7%2BbiEIx%2FpNOYev4D2MJt%2F8H'
            );

            navigateTo.projectSettings();
            onProjectSettings.onLoadflow().checkAll().apply();

            /**
             * Baseline
             */
            // Check elements are present, and labels are empty / at default
            cy.get('g').contains('Net').parent().should('contain', 'Un = 10000 V');
            cy.get('g').contains('MV').parent().should('not.contain', 'U = 10000.0 / 5773.5 V');
            cy.get('g').contains('Line 1').parent().should('not.contain', 'Ib =').and('not.contain', 'S =');

            /**
             * Run calculation
             *  */
            navigateTo.calculateNetwork();
            // cy.wait(7000); // could propably be made async and wait for calculation result

            /**
             * Assert labels
             */
            cy.get('div').contains('MV').should('contain', 'U = 10000.0 / 5773.5 V');
            cy.get('div').contains('LV').should('contain', 'U = 399.4 / 230.6 V');
            cy.get('div').contains('Line 1').should('contain', 'Ib = 71.7 A');
            cy.get('div').contains('Node 1').should('contain', 'U = 398.2 / 229.9 V');
            cy.get('div').contains('Line 2').should('contain', 'Ib = 20.3 A');
            cy.get('div').contains('Line 3').should('contain', 'Ib = 51.2 A');
            cy.get('div').contains('Node 3 + Load').should('contain', 'U = 396.6 / 229.0 V');
            cy.get('div').contains('Line 4').should('contain', 'Ib = 10.2 A').and('contain', 'dU = 5.5 %');
            cy.get('div').contains('Line 5').should('contain', 'Ib = 10.0 A').and('contain', 'dU = 1.1 %');
            cy.get('div').contains('Node 4').should('contain', 'U = - / 218.2 V');
            cy.get('div').contains('Node 5').should('contain', 'U = - / 227.4 V');
        });
    });
});
