/// <reference types="Cypress" />

/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test iterates testcases for all assertTypes in elementTypes
 */

'use strict';
import { navigateTo } from '../support/pageObjectModels/Navigation';
import { onProjectSettings } from '../support/pageObjectModels/ProjectSettings';
const diagram =
    'R7Vxpd6pIE%2F41OfMpHmjA5aNLouZVYzQR5cs9bAGUxQFc8Ne%2F1dCsIjHbzZ1z7szNTLp6q66ueqqqm743TNc69l1xq48dRTVvEKUcb5jeDUI0i9AN%2FkMpQURpMISguYZCGqWEuXFSCZEi1J2hqF6uoe84pm9s80TZsW1V9nM00XWdA2lGhnt1zPysW1FTzwhzWTTPqbyh%2BHpEZevNTMVANTSdTM0wFJnKEuPWhODpouIcMiTm7obpuo7jR7850hqvAFGmKIEUw3ZkcaIpDz05IvnuTk3JI0dUXs142LTO053DAuSULC9f1bY1s7RirjuuLxuuvDP8svqpc1DdsoruznVVu7TPyPG8eA%2FTGks8EgZ50bUNW8tvPFTHaxv6qiv6hmOTMRAXyxh2fa%2B6mmrL6siwYo5p9baRzt5TPdk1trh%2FkYWt62CRT0SLyGKiHlIyUVzD25pi8GIbfjx9OvYvY%2FPLMmzg9WxvEhpdo7iYZhAWqFqL0GIjSBXCOnZVE4zoPirCL0QxcrWUUdTjVPqRcO4vdKBW9WZzzKym%2FCsTjDx%2Bf7dmbxmyJj%2BI1UJVwA5IEVRCdzTHFs27lNpxnZ2tqHhUCkppm5HjbIFIA3Gt%2Bn5AjFrc%2BQ6QdN8ySa16NPwl7l7jSGmVK%2FWOZOywEMQF23eDZTwELmR64WLaLSzF%2Fc7lRETnOTtXViuEE%2BOOCJrmV7QjioEll5mA7EJfdSwV%2BIEGrmqCOu%2FzCCMS7dKSdiirA2QbLwLFvaoqYJiofoMYGi%2F3xYYGYR1FUxQ2GGqRqR5KaTWq4dp2pnaeVCKKqgGZ2vCYBAwhimMjyqLtRqzPTVHedF7mt2CLXmJmdI0YqR9s1TyTmJoaXZb6EneOWA5Jr6LsO25h0PkmxYK45fL2dpYHEV30hGgh6FU0PWKb%2B47oqWfzYO3M7MdFux9u6Nvb4YbJAnSlbWW1gNhixs7AK4TiMawQqjsgQ98AZB%2FhvZ06nkGAS3J837GgQbjpHZC4Fhpg1zGJcJh6vYPu7zNjtE1Dw319bJCw6G3kIV%2BNIzbbTjhlO6ZSMQV%2BV0RfvGHaURHde3vtBnWOYLioOx1MkBB0WIk%2F7uQTZYiDGSX3nP2IURgl4JhxwO1lS96P1%2B3DuNs6KZZsDAe6L%2FW506OteyLPudP5g6MMZodHo7mHXszIlk8jqxUIQfP4%2BLzhRkzUbmh0kLB8OIl8azedD4%2Bj9R2MpWyFwcyBMhrbHRbaOAJv2uLgqTVcrwJ5oEEbYSssla7EaK2hQR1G6xdt9NzeTeZtNDbwz1CTmRkn9V9aQ%2BtBXyHflq0WLVlP%2Fgq1PIkZ1oeDB3tlaMHk%2BcUb9ob0ZH7QxusX%2Bn%2FPnjFdHzcCL5xGJ7YpwI9sLdZKt8MCTwfghyP%2FZ0R%2BRok9ypis28awr5sirzhKUha2Uv8A%2FE0sIfx3BXxPwEm0XOGZMoYnkF8PyxB47be2kj0zZYM2FWuxUwZj4Pvek7rDPfByWC1nzrAP699Q2rgL6%2Bt2xsPe6jDsZeaxuL1kvVyeg5kFEvLNEU9vlf4ikPrgm5dYpnd4LQcJmTtMH%2FELJPAchfc%2B7jM0mqAf00FHV%2FqaJsAKn7GkE07aGp5BsExPgjrJau2EeTojSGsdcgE%2FMNNJGTzsRfTiS7xJwe6DJgBH0G%2F8DKsZdPagGUAf%2BsJSMEEqJ9CmuI%2BBd0Tu31Nit7MB%2BmTYGx9CSdwdNJC6Nj79oEQOITfACXDxR0gEduj0c3ysfkweIyTj3YnQCX4D5Ns6hh0BPdeBPzicQF38X66H%2FzCdV8M0M1j7Gv4DdM93nY1ahsKvju3fi5Zh4gik7RqiiaOQiE5iIpquCkwwjKvHm6pQIq6NcwySZdENUj6kOUud%2BCo9k63EtHcEH2%2FFo4qhulZv2On2d9xKe%2F634fx7e8tWhpe2Y6sXIsOPxIXU%2B%2BLCsDRVXQNWHqY2VLirouu3cf6Y8qfaSoHy8ZASnYeUpYJjfjCk7Ow8Ogz60pgyH1LWSFAZx4WNBoPNJhdnRolm3C3q0lO1WjbYtAv1mViTEMJQM41dN%2F%2F8w6SsQDhKNXCrdqENyoa4dY4qa0Nn56ZKmkSp6Juz4WaGXTpYPga2HUW9Kmz2dlLaq20HJNL0wm7FdDONpqNduxBLZyLfiTjJsCHtiK4AGyE1ZPOqeLgy9kWVsW%2BaQdJ5%2BxddmSAkR51jb4KwRdAOIRthvEbdpMwWyvVCuUnK6W4mVVzSlC4to0KZLZTrhXIz8SaK4YK9RZt%2BUD0%2FjuszAb8MoILx6GJGQKJ6EuOfNY%2BD%2F%2FckDu90Zt%2FgyVCz4MlY%2BsyTceeOLHGAH%2FdkZRA4MmyVzmhGV5RCNKM3GPbYJWLDzHmUaTJSbQ0YTfACA6JVnnijVjPEy%2Bszb6rGJHCYBWZ8wpbBHq4IorleeTzCS%2FwEHmUQhS1m0sR5vbpY%2FTKIkYIVkS8mErlFc5IdnpF%2BVI0mYy0TCtUkbaiYwpGRlgmFIZT%2BTfYctpMrXcS1s3PDy5sfKp4l%2Baq1zZ4KgN9ISXSdugIv2Uq8PI9AMpgZ2W58SI1yB3S4XhE9PSlcxoJWo0c1GmVYkNR89ATuI1Hd53AnG4eVxlfXHu1xV8Zhx0Q9GbqeAzKOO8MxOjaYq6M3MuEU%2B70MZtJ5zGRbBTCMxEB6ZY%2BGCwOxda56oEhOFQPFDZ3XV0%2FNtQnBN5HURzKL0o0hKPBDB9cfSk9SO1nlzOQd6clVCl%2BlyG8qfP1Khf%2BmxIO5lHgwrXroFdO0A6FWnIhcyDpu6RqqTDtKEg%2FumsyjWWtiF1OZedRrDHZVVZkHjENTJeMUU48L0xVSD8hQGiz69twjc56eSz6Io46TD%2FYzqQfzhouuRAau0pV%2BMPVIfODf1ONrUo8rwo3flXo06n9U6uGISuH4JY9YRbyqCPFhqE%2BYfSZcj3jCxCkxxNi8n4h8ylOAsizhY1Zdr7Tq0CpV926vRsZJX3OhlnyXQWWjZKpMwTM4UX7HFt%2FmWUcNfw9TU03YWBe3q3nQTjR%2FRR7ai4uRQVQAzX%2FCdJqFrD1MjYrRbontMN9iOuCF0OX4oVFrnMUP9c%2FFDx86toSopVF%2FM3jg0BvHljgqaKDzNsXg4cJ0JcED9XPBA%2FrC4AFdCTOX88K%2FwcPf4OFaBExS5T8ieABTYi8j4DdkUB9CwGatRTFvIiDTeBsB6bI2RQS8MN05AjbrccL2%2BxGQ%2FUIEZD%2BDgOgvAv5FwPcgYJ3KIyAb5wLfjoDXfYNA5n73Nwjx71cd8kGheFb30dPyL%2FwEoerTgjc%2FQSDj%2FchJ4LMr2t6r41qqW%2FwU4fzz1sjxsGefumbcFEcRr1R%2BIceA28LzfOxT2LO8O8P9J9xG6h7y0ojcBE3GyHwva6OC75gT44vL6iZnjelns%2FEdReaz2fQTgzilJ84jvdWLkeIjOf5lzfxP5viGrezwx8pebev4MCCA7S8%2F3bVfqAR6rwB3VTH88PIxRoZ3Yf0XYDvDFqLb5nl020Ln4M5%2BS3SLb41Rxa08zb11K19xKU9VfAxfGeVW3MdX9yoc1v3G%2B3i2iDBEspiYvY8%2Fu46PNajqOr4ZX9n%2Fpuv4dNdDjXvvdXwpElWfNv7h1%2FHojQDjD7yOR%2BcxyRddx3M0m0Ow25Lo9Kvu4zkqf42O6MIQ197HNxpU9UCfuI8HzRWDTAuSkV1cVKMQ3nPko4OLvBfaI6ZRQP2Igy%2F6OKBUnao%2FDqj47Pi3PEj77gC%2FcW5MVUb3IwF%2BX7Xx207Hpctj%2B8ponk6j%2BSv8Zeosk0mL7i%2FLTekpzIW3aAX42L4mPpAAwbFWU3Le7EjliqUBd5Ur%2F9TRTqPSq33hg7TERf19kPZ7H6QJ%2FNETl52TMO%2BsVfwwJnz2NMEP08IHP7INfKzH%2BDFNoD7H9V%2F2IM0eDmbBiudOAshQgr7K4EGX7Im3Ws7M%2F3Wz699g3k8KMqkVwns67QuwW4utgLjpytqaK%2BYpI5kWlr4l9VvMI%2ByEuJxsV9bRfAw6lLDUqRF%2FD7uk7KDOA2lQj8ElTuOHX8nOHWFlZn7n7mCluM2DKS8XW%2BBpM7MWujJYBMISj6FTyqBdHwUt0Bd5p5zGO4l5sEf0bD6iJ%2FzsdHccIcGEFdOy9bKeAX8CSETkH0DKJqXOh%2BFqZ1vFuEJ3QQcf5y0W6n2pm9WV2VYGT7Jabuqwf56w1BrDfiSFEX4cxdMnkAYNstcF60VboUX4ZA0k9q%2BynFAiL3AjfrLHEpLsJ9CxxU4G1MZ6I0SPqBqxlB6NNv046LDQN5ZSfdi7P6g9D6R8vxH65k4IaEpi2nX8sAv0HnihddlStpJBRztitI8hfQ062jcR1i%2BVP5pgfwbYhi8jsw52ZgPPUH7Y4EdcK6uF57QkiKtGsOOq9QLjt4GvmakOwD74I2gLC%2BufmOFOrz2tZPwNaMtB6t9zMPZWso7w8wJjH%2Fcr5AGfAsjgKZxfeHa08fNDuC7Q353Spa0VT2%2BlAcj4TjelgWAqfXO9eva0xDa74wN5oAZjRWOLfazFerjm0RrL8fxhGvCig81hGTsi2Aas77DiJybQsTbB2sI60IEnX2EedNANTcI2BZr%2BGLSx7YRyyayzIKdxhD99ATAF9MWc6WK%2F5a9C7ZudDvtwhT38JA40cV32%2FzN7DB82hrLXEmwZbDCeUGBBlgjYU3iwF%2FHEC7rEmx7Gm3l%2F4YE1gv5N9KQ%2B4hlbHsHMeH%2B5NfAcwNp8kZ9tQO%2BN6UZujpiwPn7IlyJIt2VP17Ay%2FOCQUbZKf%2BJEmPRtlowxT6%2FEvF7bGz2%2FbIfY1sFOxNPWAT6s2O%2FIJ7B5BLqLvJ0A%2FcD2t1IAq1p2DsALNWLARgdYXq2dHLR02Z5tV%2BhoyuCfJutQztR4%2FcROAPGU%2Fn0goAUFY7%2BCfuuSFfqHECGrZJq3AfyYsnNQAWvwHk%2BeQ3TkEqRcc7zILxjQZU3ot4APrK8YqRcHQNxA6XYsBfRgeA%2FyC7D%2B3%2BX3S8c7lO4O%2Fl2wp%2BFfAYL%2FfOFzzEuHaL%2FtYjh%2BYxlfizRaZ4nn9zzNhGL6l8tEGVb6d%2FQwd%2F8H';

/**
 * Generic test helper function
 *
 * @param {Array} elements
 * @param {Array} asserts
 *
 */
const dialogInputTest = (elements, asserts) => {
    elements.map((element) => {
        describe(`${element.name}`, function () {
            asserts.map((assert) => {
                it(`${assert.id} ${element.name}'s ${assert.description}`, () => {
                    // Setup
                    navigateTo.dataDialog(element.element, element.name);
                    cy.get(`input[data-cy="${assert.attributeValue}"]`).each((input) => {
                        cy.wrap(input).clear({ force: true }).type(assert.testValue);
                    });
                    cy.get('Button').contains('Apply').click();

                    // Assert
                    navigateTo.dataDialog(element.element, element.name);
                    cy.get(`input[data-cy="${assert.attributeValue}"]`).each((input) => {
                        cy.wrap(input).should(`${assert.should}`, assert.expected);
                    });
                    cy.get('Button').contains('Apply').click();
                });
            });
        });
    });
};

/**
 * TEST
 */
context('Datadialog test', () => {
    before(() => {
        cy.openDiagram(diagram);
    });

    // Array of elements to test
    const elementTypes = [
        { name: 'Feeder', element: 'text' },
        // { name: 'Generator1', element: 'div' },
        { name: 'Line1', element: 'div' },
        { name: 'Transformer1', element: 'div' },
        // { name: 'Load1', element: 'div' },
        { name: 'Bus1', element: 'div' },
    ];

    // Array of assertions per elementType
    const assertTypes = [
        {
            id: '(ID 2.19)',
            description: 'numeric inputs must accept numbers as scientific notation',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '1e300',
            expected: '1e300',
        },
        {
            id: '(ID 2.20)',
            description: 'numeric inputs should ignore special characters',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: ';',
            expected: '',
        },
        {
            id: '(ID 2.20)',
            description: 'numeric inputs should ignore letters',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: 'i',
            expected: '',
        },
        {
            id: '(ID 2.17)',
            description: 'numeric inputs should ignore negative values',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '-1',
            expected: '',
        },
        {
            id: '(ID 2.18)',
            description: 'numeric inputs should accept commas as decimal point',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '1,1',
            expected: '1.1',
        },
        {
            id: '(ID 2.18)',
            description: 'numeric inputs should accept commas as decimal point',
            attributeValue: 'numeric-input',
            should: 'have.value',
            testValue: '0.9',
            expected: '0.9',
        },
    ];

    // Run dialogInputTest tests
    dialogInputTest(elementTypes, assertTypes);
});
