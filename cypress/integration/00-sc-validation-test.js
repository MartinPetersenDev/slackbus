/// <reference types="Cypress" />
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test validates  label results for loadflow and shortcircuit against known results
 */

'use strict';

import chai from 'chai';
import sinonChai from 'sinon-chai';
import { navigateTo } from '../support/pageObjectModels/Navigation';
import { onProjectSettings } from '../support/pageObjectModels/ProjectSettings';
chai.use(sinonChai);

context('Shortcircuit Validation', () => {
    describe(`IEC 60909-4 shortcircuit test`, function () {
        it(`Shortcircuit currents should match IEC 60909-4 Chapter 3 within 1.2%`, () => {
            cy.openDiagram(
                'R7VxZd6LKFv41eUwvKAbl0SEqaTVpNTHh5SwERJTBCzjgrz%2B7oJidMmj6rGt3TKxd06498VXV1jumYW3brryc9RxVM%2B8QpW7vmOYdQlUkwG9MCCICTxGC7hpqRKJTwtDYaYRIEerKUDUvohGS7zimbyy9XG%2FFsW1N8XM02XWdTb7v1DHzsy5lXSsRhopslqljQ%2FVnhErzQlrR0Qx95scLrkQVlhw3JlN7M1l1NhkS83DHNFzH8aN3zmSOF4AoU56ADMN2ZG2yqYieEpGmsulpKb3ryOrUjMfNVHozZ%2FMKgkrWV6ir2bq5v2Y4c1xfMVxlZfh7Gzw7G83dW9NYua5m7%2B%2FVdTwv1mSmypK3hM2x7NqGref1D9XxEkVfc2XfcGwyCOIoKlH%2BWnN1zVa0rmHFXNPafSWdvql5imsscf%2Bo2ndXhIWl62DR92WLCKSpWQ7QH7aytTRJI9XwlqYcvNiGH8%2BfDv6PsfjHMmxgtqykhBgzCw1zhNgPUqOwtg3NBDdqRUV4Q4wjV0sZRVNOZR8N3TrQ4bGq%2FC%2FwhN%2F1OUu369Oh989qd8%2BQ5fhBbBeaCq5AirZjw5%2F6zLewadLwVtsa%2Fhu8p35xpPSeKzXxsqm4EMQF23eDt3gIXMj0wsW0W1iK%2B3m%2B7Po17NApM5qtFihlQRDZeM7KVbQjqyftYBZd84%2B0I0rHoslMQMTc1hxLA66hgauZYKvrfBSRieXoSTuUVTLR08Fo0AfOEH%2BHGBpL5MWG2rCCQhSFXYF6jaYZmrKyqL8M78EpvMTe6V9x%2FAyWhK2WpqngySE1tf5wGkx6IT3J8CFpKiu%2B4%2BZWddAzxAWTDWMNx3ueGVmKuKCzxeEiKjEsz8b%2B%2F3Z%2FP8hHhHVd9rQSY76T4%2BmopWdVRjwjY%2FUQp0P5GFYYOusgRN%2BAUNvFinh2PIOEkInj%2B44FDUIN1UHkuuusbLXhmERADM%2FXUauVGaNmGjru6ztLoMreMnpkTY2tBgzXwylrMZWKKfBelX35jqlFRdTy1vodqm%2FBF1HjudNHUlBnJ%2BPtStlRhtwZUErTWXcZlVEDjukF3FqxlHVvXtv0GsJOtRRD7Mz8SZvbPdkzTx5z7vPw0VE7g82TUV1DL6ZrK7uuJQRSUN0%2BjRZcl4naiUYdSW%2BPO3ksrJ6H4rY7f4Cx1KXUGThQRj27zkIbRxqbttz5I4jz90Dp6NBGWkpvamPC6IJoUJvu%2FEXvjmqr%2FrCGegZ%2BibrCDLhJ%2B0UQrcfZO%2FJtxRLoifXHf0eCN2FEXuw82u%2BGHvRHL57YFOn%2BcKP35i%2F075FnPM%2B3C2ks7bo7tirBS7Fe52qjzgJPG%2BCHI38ZeTyg5CZl9Oc1Q2zPTHmsOmpSlpaT9gb461tS%2BP8d%2BO5DvBZcaUQZ4g7k18QyBF7bwnJiD0zFoE3Vel2pnR7w3fImDXENvGze3waO2Ib1Lyi914D1Neo9sfm%2BEZuZeSxuPbFeDs%2FBDIIJ8s3umF6q7ddg0oan5BuW6QNey2aCzBWmd8evSBpzFNZ93Ec0qmAfz536TG3rugQrHGFJJ5zUdDyDZJneBOomlrCShumMIK15yAW8YKad2nlcy%2BjFn4xNCrQPlgAcQb%2FeCFbTqa%2FBMoAu%2BtKbZIJUdmBNcR8Da0Rptyi5UV8AvS82e5tQEg8bHaSu93Y%2FKJFNyA1wAlz8FRIBDe1%2Bjo%2F3H5NHFylYO1F0gncQ%2BZaOYUfPE64OPxghoAb%2BzTXxD1OfGqaZibXT8F%2BIFFxnoe2Lwppq%2BPIkjPM4tk4d22%2FJlmFijFFzDdkkRLILoeljiAKHdG17FAOQ2gp5RJEdECOQx90m3U%2FwhDTL7CRi2gdAQwEo7sMQf%2B5QCQbYsGk7Czl4q0naq2YHReTw5xRUNvCm43mWPMAzOHkP2DC8kJ8iXH%2FOoec%2FuVK4lLOBADoKBMKnOX4uh1g1g3xlVyEmwlFlQ0zMrWjBof0ibLyokZTZQpkvlKuknOK%2BpIpLmtJ7y6hQZgtlvlCuJq6lGi4YTqT%2Fjeb5McjJoB8F%2FAFvAg%2FCIwJxCOApNY%2BR0EdQVMF%2FDzv6tTybFgquTfMl1%2BbKno1o9quufd6Wjvvslu4zG7qkEPVC19vQ%2FWZ%2BV%2BtrwZq%2FjB4a%2FWEwUXYo3s5ef0NHuj5j709NBfF5U0k2OPEQ0YJIr4IZJGzss4zzTKHySVOgP2MK9H%2FTFPjrmALDoU%2BZAshDDjLNyPPl4DzJuPE81Wp2uJPteZYvWGLEwZl2uQ99tOhLwo9WbBBnwAx2L8jINDwPZaATiOeoT%2FKXgB9CpUlVKjf48W3wIxHoKfiRNLwW%2FEjOv4nDchx1DvygUSHefM%2FOoktnrISmLHwueVdpwE8jlFpa2dVsHfiLTy%2Fp6PDSKsaFrmF%2FJS5kzgnZ4ikheSJMXaz7c%2Fy4dHuwb4H5aNQl%2BiSrJb1iTgZkYuoXkJgqUdNbjioQ6oBKqTQbq%2FQtQ6UqNKG2c4GqnivJ1sTXrOVd5sjVkrcpiY4xytGoVT0atcrP7UzkilwmvkfDsCAf5FTZmyWFgrvtx6b8pwAJewKQnBEMLuX9Jy8q%2BDPBjHAmmCGx5B4COB%2BfTJB4cs%2BWTyro2IC%2FCII4isvHrkphT3QABJUG4oUisObzA0WS%2BjKa4ovBVjiOport42OfvWiq1LsgT2c69bQP7gvOCNktdFE49oFTn2%2BCY8xX4Jhwg2M3OPaVc1624PHoWnDsvCMAmpwBrGVzRYz6FTSihjkVd4V0h4zlw%2FL9gsmf0qNlqCruXnc1z9hltJT3Amxd8soHM4kUUVRWRzPXGh64oDCBlDPapsJ%2FcTvC%2FJ7EiA9rtXBuw5R1mpwCZrXKfFypJS3uPcxgS%2Fq53ple9YcPctCZ2Ofbz%2FS%2BpDH%2Bkxr7jqO3K6bVnKuxve2479bYt5yYMTx9FOMV2yOBKljLl0%2FMRhFEO5b1g%2BdDVMgLzgBKmg7TpiG8ohavtbOAXYrqRq5se1PHteIsoYPIKoV9IxRDO5Lck00fslFBcsMC%2FtMI9CNhLk3%2FYX4xDI14Nn2Gyp5EtsDp3WC8wSfd0w1%2BHPf3HAocdV50FBR%2BY65QAlf%2BT3KFuB5TzBX6sz2RK8T1ghrVG4o4V6A9MCW7h7Nt7FLGDfTojXr6qFHbQa%2Ft0xDnvIh0%2BGo5H8v0OZ5zAn1aKwVJIDVKEO3HtdpuUdJQ959GNa%2B3g9dosTyUm%2FI%2B3nqHMnVYTmw%2BBCRLZvU0rAU%2FmBfCgORQxMsDSPcHM3Zo0GfQN8Jsqi1whUexZJAjcLqaIM7MrB7029sQqaQcDmngYGAq1naJpYMt82BGlx33CbO6Amncp8QOWCWWQGdB%2FmYs2hB1uf26lNAMj0sd11jC7ynbaL96ILeNgl70d%2BwT814AveYayDrURecR57mF9qHYmKuPSGWvLcxAt6EdSDCG%2FFbfSUOYL8mT63PpfHo8X5Dwc65tfCyHLogy18AnsNYfKP0JZ7HNa9jLQfIX949EJntylX5%2B71lKMmLL%2BxSBKW9T2I%2BD3jPOlUb0d4AWnrkiaKE%2FAVp45jRoqbBchaGF%2BGMwR0EL%2Bi7QwtxAyw203EDLDbTcQMsNtPx3QAvLV34QtHRfL3obFg5%2F5esw9gT%2BOQoiuKMg4nYddrsO%2B1h20r6rkytmJ6GMlbD%2FreSkU25cSk7as75CMCI9z8hNSvIpcrlJNEOoudykSnxZlstNovlL5CbtjVnHr%2FCvnJtU%2FdSNzak7tr8kN%2BnYLc4FcpOQELtFnH7DloLJt6Um0fnUJJ6q5oc4NzWpUoiALLpQahIqRFoinIOpSYX2DJtrfyI16T4WfZwlxnH5VV0oWanLZKJ0GJGpvy92c8XYfSojqRS7cysrRG3m3KjNsbGKs1GbTjJUMlGb5mOrzAbtShLKLx%2B06fIXX%2Fxg1L7MPftfErWPReOTd%2FQ0fWbY%2FqtjMve5mPwtISz06vsnCDYzTVYvudssznTljSd3IuodjwfHv5%2FjtvO87TyP40Wh%2BLHcqyVi7sUtbMZKuPDpnvrlXwRd%2BCJ0OeXEJehSXFwBvbBnohd6z6dhaLZa%2FjAMUykBF5blrrbZpNENt%2FzkbjPGI6eBC%2FNzwIWmiputaiHKnItcYKQ8BroqdGkxl4QrrfjL564HUfgvQZTjN%2Bw3iHKDKCe%2BOQTxeWe%2BHkaBYvqlo1FsSL%2B5lXn4Fw%3D%3D'
            );

            navigateTo.projectSettings();
            onProjectSettings.onLoadflow().checkAll().onShortcircuit().checkAll().apply();

            /**
             * Baseline
             */
            // Check elements are present, and labels are empty / at default
            cy.get('g').contains('Net').parent().should('contain', 'Un = 20000 V');
            cy.get('g').contains('Q').parent().should('not.contain', 'U = 20000.0 / 11547.0 V').and('not.contain', 'Ik');
            cy.get('g').contains('F1').parent().should('not.contain', 'Ik =');

            /**
             * Run calculation
             *  */
            navigateTo.calculateNetwork();
            // cy.wait(7000); // could propably be made async and wait for calculation result

            /**
             * Assert labels
             */
            cy.get('g')
                .contains('Q')
                .should('contain', 'U = 20000.0 / 11547.0 V')
                .and('contain', "Ik''3 = 10.000 kA")
                .and('contain', "Ik''2 = 8.660 kA")
                .and('contain', "Ik''1 = 0.000 kA");

            cy.get('g').contains('F1').should('contain', "Ik''3 = 34.750 kA").and('contain', "Ik''2 = 30.094 kA").and('contain', "Ik''1 = 35.284 kA");

            cy.get('g').contains('F2').should('contain', "Ik''3 = 34.238 kA").and('contain', "Ik''2 = 29.651 kA").and('contain', "Ik''1 = 34.581 kA");

            cy.get('g').contains('F3').should('contain', "Ik''3 = 6.949 kA").and('contain', "Ik''2 = 6.018 kA").and('contain', "Ik''1 = 4.828 kA");
        });
    });
});
