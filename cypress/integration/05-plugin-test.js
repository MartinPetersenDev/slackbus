/// <reference types="Cypress" />
/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

/**
 * This test checks if plugin and library is loaded
 */

'use strict';

const EXPECT_BUILD_VERSION = '@@CI_COMMIT_SHORT_SHA';

context('Plugin load', () => {
    beforeEach(() => {
        cy.openEmptyNoSplash();
    });

    describe('Loading of slackbus plugin', function () {
        //it('About dialog should contain build version', () => {
        //   cy.get('a').should('have.class', 'geItem').contains('SlackBUS').click();
        //   cy.get('td').contains('About SlackBUS').click();
        //   cy.get('[id="build-version"]').contains(EXPECT_BUILD_VERSION);
        //});

        it('Plugin should add slabusnet-lib.xml on load', () => {
            cy.get('a').contains('slackbusnet-lib.xml');
        });
    });
});
