/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

// Imports
var assert = require('assert');
const networkjs = require('../../src/sdk/network');
const linkjs = require('../../src/sdk/link');
const calcjs = require('../../src/engine/calculateNet');
const mathjs = require('mathjs');
const nodejs = require('../../src/sdk/node');
const Complex = require('complex.js');
const utils = require('../../src/sdk/utils');

// eslint-disable-next-line no-undef
describe('Integration test - Shortcircuit Calculation ', function () {
    // Setup IEC 60909-4 Chapter 3 shortcircuit test

    // Create constwork
    let network = {
        name: 'Network',
        links: {
            posSeq: [
                {
                    id: 'J8cqys9KBj41GBfSs_uz-1',
                    name: 'Net',
                    zm: { mathjs: 'Complex', re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    z0: { mathjs: 'Complex', re: 250, im: 250 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 1,
                    from: 0,
                    fromZeroSeq: 0,
                    to: 1,
                    toZeroSeq: 1,
                    sBase: 100,
                    vBase: 20000,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-3',
                    name: 'T1',
                    zm: { mathjs: 'Complex', re: 0.0016774055380355353, im: 0.0062831082459858445 },
                    z0: { mathjs: 'Complex', re: 0.0016774055380355353, im: 0.0062831082459858445 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 2.3099014526270802e-14,
                    Q: -6.158642795274259e-15,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 3.944304526105059e-30,
                    Ib: 2.3905927155512146e-14,
                    dU: 1.5546415853955e-14,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9748942661084339,
                    from: 1,
                    fromZeroSeq: 0,
                    to: 2,
                    toZeroSeq: 2,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'J8cqys9KBj41GBfSs_uz-8',
                    name: 'L1',
                    zm: { mathjs: 'Complex', re: 0.00024062499999999998, im: 0.00024687499999999997 },
                    z0: { mathjs: 'Complex', re: 0.000890625, im: 0.000446875 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 1.3202442837803062e-12,
                    Q: 6.308073853749107e-14,
                    Qlimit: 0,
                    Ploss: 4.0389678347315804e-28,
                    Qloss: 4.291403324402304e-28,
                    Ib: 1.321750410792071e-12,
                    dU: 4.5566404889231685e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 2,
                    fromZeroSeq: 2,
                    to: 3,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-2',
                    name: 'T2',
                    zm: { mathjs: 'Complex', re: 0.002945626811781762, im: 0.009811882910045049 },
                    z0: { mathjs: 'Complex', re: 0.002945626811781762, im: 0.009811882910045049 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 5.189864956521024e-15,
                    Q: 4.371356887599221e-14,
                    Qlimit: 0,
                    Ploss: 5.5220263365470826e-30,
                    Qloss: 1.8932661725304283e-29,
                    Ib: 4.402057248767944e-14,
                    dU: 4.5096871898026096e-14,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9750861806219077,
                    from: 1,
                    fromZeroSeq: 0,
                    to: 4,
                    toZeroSeq: 4,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-9',
                    name: 'L2',
                    zm: { mathjs: 'Complex', re: 0.00026, im: 0.00008499999999999999 },
                    z0: { mathjs: 'Complex', re: 0.0011, im: 0.000103125 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 1.211329431506053e-13,
                    Q: 9.362886200822134e-13,
                    Qlimit: 0,
                    Ploss: 2.271919407036514e-28,
                    Qloss: 0,
                    Ib: 9.44091928792839e-13,
                    dU: 2.582484047891325e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 4,
                    to: 4,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-10',
                    name: 'L3',
                    zm: { mathjs: 'Complex', re: 0.0033875, im: 0.0010875 },
                    z0: { mathjs: 'Complex', re: 0.0101625, im: 0.00485 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 8.245875986271199e-14,
                    Q: 4.941571890689596e-14,
                    Qlimit: 0,
                    Ploss: 2.524354896707238e-29,
                    Qloss: 1.262177448353619e-29,
                    Ib: 9.613199443047958e-14,
                    dU: 3.420166759026648e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 3,
                    to: 5,
                    toZeroSeq: 5,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-12',
                    name: 'L4',
                    zm: { mathjs: 'Complex', re: 0.011562499999999998, im: 0.00928125 },
                    z0: { mathjs: 'Complex', re: 0.023124999999999996, im: 0.027843749999999997 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 8.397671266235933e-15,
                    Q: 1.3462242574269955e-14,
                    Qlimit: 0,
                    Ploss: 3.1554436208840472e-30,
                    Qloss: 1.5777218104420236e-30,
                    Ib: 1.5866721710052495e-14,
                    dU: 2.3525217950297037e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 5,
                    fromZeroSeq: 5,
                    to: 6,
                    toZeroSeq: 6,
                    sBase: 100,
                    vBase: 400,
                },
            ],
            zeroSeq: [
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-3',
                    name: 'T1',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.0016774055380355353, im: 0.0062831082459858445 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9748942661084339,
                    from: 0,
                    fromZeroSeq: 0,
                    to: 2,
                    toZeroSeq: 2,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-2',
                    name: 'T2',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.002945626811781762, im: 0.009811882910045049 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9750861806219077,
                    from: 0,
                    fromZeroSeq: 0,
                    to: 4,
                    toZeroSeq: 4,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'J8cqys9KBj41GBfSs_uz-1',
                    name: 'Net',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 250, im: 250 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 1,
                    from: 0,
                    fromZeroSeq: 0,
                    to: 1,
                    toZeroSeq: 1,
                    sBase: 100,
                    vBase: 20000,
                },
                {
                    id: 'J8cqys9KBj41GBfSs_uz-8',
                    name: 'L1',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.000890625, im: 0.000446875 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 2,
                    fromZeroSeq: 2,
                    to: 3,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-9',
                    name: 'L2',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.0011, im: 0.000103125 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 4,
                    to: 4,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-10',
                    name: 'L3',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.0101625, im: 0.00485 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 3,
                    to: 5,
                    toZeroSeq: 5,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-12',
                    name: 'L4',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 0.023124999999999996, im: 0.027843749999999997 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 5,
                    fromZeroSeq: 5,
                    to: 6,
                    toZeroSeq: 6,
                    sBase: 100,
                    vBase: 400,
                },
            ],
            posSeqForYbus: [
                {
                    id: 'J8cqys9KBj41GBfSs_uz-1',
                    name: 'Net',
                    zm: { mathjs: 'Complex', re: 1000000000, im: 1000000000 },
                    z0: { mathjs: 'Complex', re: 250, im: 250 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 0,
                    Q: 0,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 0,
                    Ib: 0,
                    dU: 0,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 1,
                    from: 0,
                    fromZeroSeq: 0,
                    to: 1,
                    toZeroSeq: 1,
                    sBase: 100,
                    vBase: 20000,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-3',
                    name: 'T1',
                    zm: { mathjs: 'Complex', re: 0.0016774055380355353, im: 0.0062831082459858445 },
                    z0: { mathjs: 'Complex', re: 0.0016774055380355353, im: 0.0062831082459858445 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 2.3099014526270802e-14,
                    Q: -6.158642795274259e-15,
                    Qlimit: 0,
                    Ploss: 0,
                    Qloss: 3.944304526105059e-30,
                    Ib: 2.3905927155512146e-14,
                    dU: 1.5546415853955e-14,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9748942661084339,
                    from: 1,
                    fromZeroSeq: 0,
                    to: 2,
                    toZeroSeq: 2,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'J8cqys9KBj41GBfSs_uz-8',
                    name: 'L1',
                    zm: { mathjs: 'Complex', re: 0.00024062499999999998, im: 0.00024687499999999997 },
                    z0: { mathjs: 'Complex', re: 0.000890625, im: 0.000446875 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 1.3202442837803062e-12,
                    Q: 6.308073853749107e-14,
                    Qlimit: 0,
                    Ploss: 4.0389678347315804e-28,
                    Qloss: 4.291403324402304e-28,
                    Ib: 1.321750410792071e-12,
                    dU: 4.5566404889231685e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 2,
                    fromZeroSeq: 2,
                    to: 3,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-2',
                    name: 'T2',
                    zm: { mathjs: 'Complex', re: 0.002945626811781762, im: 0.009811882910045049 },
                    z0: { mathjs: 'Complex', re: 0.002945626811781762, im: 0.009811882910045049 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 5.189864956521024e-15,
                    Q: 4.371356887599221e-14,
                    Qlimit: 0,
                    Ploss: 5.5220263365470826e-30,
                    Qloss: 1.8932661725304283e-29,
                    Ib: 4.402057248767944e-14,
                    dU: 4.5096871898026096e-14,
                    singlePhase: false,
                    Rfault: 1,
                    Kt: 0.9750861806219077,
                    from: 1,
                    fromZeroSeq: 0,
                    to: 4,
                    toZeroSeq: 4,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-9',
                    name: 'L2',
                    zm: { mathjs: 'Complex', re: 0.00026, im: 0.00008499999999999999 },
                    z0: { mathjs: 'Complex', re: 0.0011, im: 0.000103125 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 1.211329431506053e-13,
                    Q: 9.362886200822134e-13,
                    Qlimit: 0,
                    Ploss: 2.271919407036514e-28,
                    Qloss: 0,
                    Ib: 9.44091928792839e-13,
                    dU: 2.582484047891325e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 4,
                    to: 4,
                    toZeroSeq: 3,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-10',
                    name: 'L3',
                    zm: { mathjs: 'Complex', re: 0.0033875, im: 0.0010875 },
                    z0: { mathjs: 'Complex', re: 0.0101625, im: 0.00485 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 8.245875986271199e-14,
                    Q: 4.941571890689596e-14,
                    Qlimit: 0,
                    Ploss: 2.524354896707238e-29,
                    Qloss: 1.262177448353619e-29,
                    Ib: 9.613199443047958e-14,
                    dU: 3.420166759026648e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 3,
                    fromZeroSeq: 3,
                    to: 5,
                    toZeroSeq: 5,
                    sBase: 100,
                    vBase: 400,
                },
                {
                    id: 'K3K8Bv9mjUTECNSybcz2-12',
                    name: 'L4',
                    zm: { mathjs: 'Complex', re: 0.011562499999999998, im: 0.00928125 },
                    z0: { mathjs: 'Complex', re: 0.023124999999999996, im: 0.027843749999999997 },
                    shunt: { mathjs: 'Complex', re: 0, im: 0 },
                    U: 0,
                    P: 8.397671266235933e-15,
                    Q: 1.3462242574269955e-14,
                    Qlimit: 0,
                    Ploss: 3.1554436208840472e-30,
                    Qloss: 1.5777218104420236e-30,
                    Ib: 1.5866721710052495e-14,
                    dU: 2.3525217950297037e-14,
                    singlePhase: false,
                    Rfault: 1.56,
                    Kt: 1,
                    from: 5,
                    fromZeroSeq: 5,
                    to: 6,
                    toZeroSeq: 6,
                    sBase: 100,
                    vBase: 400,
                },
            ],
        },
        nodes: [
            {
                node: 1,
                id: 'J8cqys9KBj41GBfSs_uz-2',
                sc: {
                    ik3: 3464.0999999999995,
                    ik2: 2999.9986012496734,
                    ik1: 0.009333796535305406,
                    ik2EE: 0.004666903133785461,
                    ikmin: 0.008485269577550369,
                    ikmax: 3464.0999999999995,
                    cMax: 1.1,
                    cMin: 1,
                },
                lf: {
                    type: 2,
                    P: 4.999937837983408e-10,
                    Q: -5.0000862505762e-10,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 4.99954703947874e-10,
                    Qgen: 5.000654684764809e-10,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: -4.999937837983408e-10,
                    Qmismatch: 5.0000862505762e-10,
                    U: { mathjs: 'Complex', re: 1, im: 0 },
                    Uln: { mathjs: 'Complex', re: 0.5773502691896258, im: 0 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 20000,
                iBase: 0.002886751345948129,
                zBase: 4000000,
                name: 'Q',
                type: 1,
                Pgen: 1e-10,
            },
            {
                node: 2,
                id: 'J8cqys9KBj41GBfSs_uz-6',
                sc: {
                    ik3: 240.75586186382225,
                    ik2: 208.50069248408718,
                    ik1: 244.45759864807232,
                    ik2EE: 260.0124269695449,
                    ikmin: 188.01402710886168,
                    ikmax: 244.45759864807232,
                    cMax: 1.05,
                    cMin: 0.95,
                },
                lf: {
                    type: 2,
                    P: 1.3189025184389698e-12,
                    Q: 1.38819325788968e-13,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 0,
                    Qgen: 0,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: -1.3189025184389698e-12,
                    Qmismatch: -1.38819325788968e-13,
                    U: { mathjs: 'Complex', re: 1, im: 1.554312234475219e-16 },
                    Uln: { mathjs: 'Complex', re: 0.5773502691896258, im: 8.973825869789965e-17 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 400,
                iBase: 0.14433756729740643,
                zBase: 1600,
                name: 'F1',
            },
            {
                node: 3,
                id: 'J8cqys9KBj41GBfSs_uz-9',
                sc: {
                    ik3: 237.20970201241474,
                    ik2: 205.42962796688784,
                    ik1: 239.5867497883436,
                    ik2EE: 253.30412447732996,
                    ikmin: 185.01465972824053,
                    ikmax: 239.5867497883436,
                    cMax: 1.05,
                    cMin: 0.95,
                },
                lf: {
                    type: 2,
                    P: -7.38964445190504e-13,
                    Q: -1.8474111129762612e-13,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 0,
                    Qgen: 0,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: 7.38964445190504e-13,
                    Qmismatch: 1.8474111129762612e-13,
                    U: { mathjs: 'Complex', re: 0.9999999999999997, im: -1.554312234475219e-16 },
                    Uln: { mathjs: 'Complex', re: 0.5773502691896256, im: -8.973825869789965e-17 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 400,
                iBase: 0.14433756729740643,
                zBase: 1600,
                name: 'F2',
            },
            {
                node: 4,
                id: 'K3K8Bv9mjUTECNSybcz2-5',
                sc: {
                    ik3: 235.56097854629434,
                    ik2: 204.00179156141203,
                    ik1: 236.2786027803555,
                    ik2EE: 247.93175774771635,
                    ikmin: 183.2176921726019,
                    ikmax: 236.2786027803555,
                    cMax: 1.05,
                    cMin: 0.95,
                },
                lf: {
                    type: 2,
                    P: -4.547473508864639e-13,
                    Q: 2.2737367544323198e-13,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 0,
                    Qgen: 0,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: 4.547473508864639e-13,
                    Qmismatch: -2.2737367544323198e-13,
                    U: { mathjs: 'Complex', re: 0.9999999999999996, im: 7.771561172376095e-17 },
                    Uln: { mathjs: 'Complex', re: 0.5773502691896255, im: 4.486912934894983e-17 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 400,
                iBase: 0.14433756729740643,
                zBase: 1600,
                name: 'LV2',
            },
            {
                node: 5,
                id: 'K3K8Bv9mjUTECNSybcz2-11',
                sc: {
                    ik3: 148.7031746369605,
                    ik2: 128.78072685900162,
                    ik1: 109.88559450038503,
                    ik2EE: 90.80318722077519,
                    ikmin: 77.98684944736797,
                    ikmax: 148.7031746369605,
                    cMax: 1.05,
                    cMin: 0.95,
                },
                lf: {
                    type: 2,
                    P: -2.1316282072802996e-14,
                    Q: -3.5527136788005016e-14,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 0,
                    Qgen: 0,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: 2.1316282072802996e-14,
                    Qmismatch: 3.5527136788005016e-14,
                    U: { mathjs: 'Complex', re: 1, im: -2.3314683517128283e-16 },
                    Uln: { mathjs: 'Complex', re: 0.5773502691896258, im: -1.3460738804684946e-16 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 400,
                iBase: 0.14433756729740643,
                zBase: 1600,
                name: 'Cable-Overhead',
            },
            {
                node: 6,
                id: 'K3K8Bv9mjUTECNSybcz2-13',
                sc: {
                    ik3: 48.14380421466341,
                    ik2: 41.69375748472284,
                    ik1: 33.45003891400606,
                    ik2EE: 26.830095611308508,
                    ikmin: 23.29774282740579,
                    ikmax: 48.14380421466341,
                    cMax: 1.05,
                    cMin: 0.95,
                },
                lf: {
                    type: 2,
                    P: 7.105427357601005e-15,
                    Q: -7.105427357601001e-15,
                    Psch: 0,
                    Qsch: 0,
                    Pgen: 0,
                    Qgen: 0,
                    Pload: 0,
                    Qload: 0,
                    Umismatch: 0,
                    Pmismatch: -7.105427357601005e-15,
                    Qmismatch: 7.105427357601001e-15,
                    U: { mathjs: 'Complex', re: 1.0000000000000002, im: -3.108624468950438e-16 },
                    Uln: { mathjs: 'Complex', re: 0.577350269189626, im: -1.794765173957993e-16 },
                    uFactor: 1,
                    QlimitMax: 0,
                    QlimitMin: 0,
                    singlePhase: false,
                },
                Un: 1,
                sBase: 100,
                vBase: 400,
                iBase: 0.14433756729740643,
                zBase: 1600,
                name: 'F3',
            },
        ],
        matrix: {
            zBusPosSeq: [
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                ],
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.0011740248794715404, im: 0.004200273069259087 },
                    { re: 0.0010870309147891376, im: 0.004103282937846361 },
                    { re: 0.000990185280346276, im: 0.0040671139813280004 },
                    { re: 0.0010870309147891376, im: 0.004103282937846361 },
                    { re: 0.0010870309147891376, im: 0.004103282937846361 },
                ],
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.0010870309147891378, im: 0.004103282937846361 },
                    { re: 0.0012341233405576033, im: 0.004250942886018089 },
                    { re: 0.0011318624886945516, im: 0.0042156159894722635 },
                    { re: 0.0012341233405576033, im: 0.004250942886018089 },
                    { re: 0.0012341233405576033, im: 0.004250942886018089 },
                ],
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.0009901852803462757, im: 0.0040671139813280004 },
                    { re: 0.0011318624886945514, im: 0.0042156159894722635 },
                    { re: 0.0012861047439233454, im: 0.004267873768454093 },
                    { re: 0.0011318624886945514, im: 0.0042156159894722635 },
                    { re: 0.0011318624886945514, im: 0.0042156159894722635 },
                ],
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.0010870309147891378, im: 0.004103282937846361 },
                    { re: 0.0012341233405576033, im: 0.004250942886018089 },
                    { re: 0.0011318624886945516, im: 0.0042156159894722635 },
                    { re: 0.004621623340557603, im: 0.005338442886018089 },
                    { re: 0.004621623340557603, im: 0.005338442886018089 },
                ],
                [
                    { re: 0.00003159668916113816, im: 0.00031596689161138194 },
                    { re: 0.0010870309147891378, im: 0.004103282937846361 },
                    { re: 0.0012341233405576033, im: 0.004250942886018089 },
                    { re: 0.0011318624886945516, im: 0.0042156159894722635 },
                    { re: 0.004621623340557603, im: 0.005338442886018089 },
                    { re: 0.0161841233405576, im: 0.014619692886018089 },
                ],
            ],
            zBusZeroSeq: [
                [
                    { re: 250, im: 250 },
                    { re: 0, im: 0 },
                    { re: 0, im: 0 },
                    { re: 0, im: 0 },
                    { re: 0, im: 0 },
                    { re: 0, im: 0 },
                ],
                [
                    { re: 0, im: 0 },
                    { re: 0.0013395827951507414, im: 0.003946188785138622 },
                    { re: 0.001037424170629955, im: 0.003747198093768713 },
                    { re: 0.0006451574382352375, im: 0.0036632618999478408 },
                    { re: 0.001037424170629955, im: 0.003747198093768713 },
                    { re: 0.001037424170629955, im: 0.003747198093768713 },
                ],
                [
                    { re: 0, im: 0 },
                    { re: 0.001037424170629955, im: 0.0037471980937687126 },
                    { re: 0.0015723529504931902, im: 0.004009467289120482 },
                    { re: 0.0011305676813289773, im: 0.003961944745225018 },
                    { re: 0.0015723529504931902, im: 0.004009467289120482 },
                    { re: 0.0015723529504931902, im: 0.004009467289120482 },
                ],
                [
                    { re: 0, im: 0 },
                    { re: 0.0006451574382352377, im: 0.003663261899947841 },
                    { re: 0.0011305676813289775, im: 0.003961944745225018 },
                    { re: 0.0017522840893031222, im: 0.0040751007214332845 },
                    { re: 0.0011305676813289775, im: 0.003961944745225018 },
                    { re: 0.0011305676813289775, im: 0.003961944745225018 },
                ],
                [
                    { re: 0, im: 0 },
                    { re: 0.001037424170629955, im: 0.0037471980937687126 },
                    { re: 0.0015723529504931902, im: 0.004009467289120482 },
                    { re: 0.0011305676813289773, im: 0.003961944745225018 },
                    { re: 0.01173485295049319, im: 0.008859467289120482 },
                    { re: 0.01173485295049319, im: 0.008859467289120482 },
                ],
                [
                    { re: 0, im: 0 },
                    { re: 0.001037424170629955, im: 0.0037471980937687126 },
                    { re: 0.0015723529504931902, im: 0.004009467289120482 },
                    { re: 0.0011305676813289773, im: 0.003961944745225018 },
                    { re: 0.01173485295049319, im: 0.008859467289120482 },
                    { re: 0.03485985295049319, im: 0.03670321728912048 },
                ],
            ],
            zBusForYBus: [
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000, im: 1000000000 },
                ],
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000.0011424, im: 1000000000.0038843 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                    { re: 1000000000.0009586, im: 1000000000.0037512 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                ],
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                    { re: 1000000000.0012026, im: 1000000000.003935 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                    { re: 1000000000.0012026, im: 1000000000.003935 },
                    { re: 1000000000.0012026, im: 1000000000.003935 },
                ],
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000.0009586, im: 1000000000.0037512 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                    { re: 1000000000.0012546, im: 1000000000.0039519 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                ],
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                    { re: 1000000000.0012026, im: 1000000000.003935 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                    { re: 1000000000.00459, im: 1000000000.0050225 },
                    { re: 1000000000.00459, im: 1000000000.0050225 },
                ],
                [
                    { re: 1000000000, im: 1000000000 },
                    { re: 1000000000.0010555, im: 1000000000.0037873 },
                    { re: 1000000000.0012026, im: 1000000000.003935 },
                    { re: 1000000000.0011003, im: 1000000000.0038997 },
                    { re: 1000000000.00459, im: 1000000000.0050225 },
                    { re: 1000000000.0161525, im: 1000000000.0143038 },
                ],
            ],
            yBus: {
                mathjs: 'DenseMatrix',
                data: [
                    [
                        { mathjs: 'Complex', re: 67.72879323561547, im: -242.05939703341377 },
                        { mathjs: 'Complex', re: -39.622944854150234, im: 148.61244744734122 },
                        { mathjs: 'Complex', re: -0.052460353332452095, im: -0.07791376006819946 },
                        { mathjs: 'Complex', re: -28.053388027632774, im: 93.52486334564078 },
                        { mathjs: 'Complex', re: -9.285013264718227e-16, im: 2.9809676243674124e-16 },
                        { mathjs: 'Complex', re: 0, im: 0 },
                    ],
                    [
                        { mathjs: 'Complex', re: -39.62294485415014, im: 148.6124474473411 },
                        { mathjs: 'Complex', re: 2063.4907255981907, im: -2226.4974276831513 },
                        { mathjs: 'Complex', re: -2024.002600927825, im: 2078.4626879137695 },
                        { mathjs: 'Complex', re: 0.13482018378429667, im: -0.5777076779594265 },
                        { mathjs: 'Complex', re: -2.851965539735136e-14, im: 1.325310170386301e-14 },
                        { mathjs: 'Complex', re: -9.08502918170102e-31, im: 1.9624636226743584e-31 },
                    ],
                    [
                        { mathjs: 'Complex', re: -0.05246035333269515, im: -0.07791376006808501 },
                        { mathjs: 'Complex', re: -2024.0026009278295, im: 2078.462687913769 },
                        { mathjs: 'Complex', re: 5766.922947391971, im: -3299.6277181460487 },
                        { mathjs: 'Complex', re: -3475.245599524472, im: 1135.322402025599 },
                        { mathjs: 'Complex', re: -267.62228658633677, im: 85.92054196674938 },
                        { mathjs: 'Complex', re: 0, im: 0 },
                    ],
                    [
                        { mathjs: 'Complex', re: -28.05338802763263, im: 93.52486334564074 },
                        { mathjs: 'Complex', re: 0.13482018378922261, im: -0.5777076779590791 },
                        { mathjs: 'Complex', re: -3475.245599524477, im: 1135.322402025598 },
                        { mathjs: 'Complex', re: 3503.16416736832, im: -1228.2695576932801 },
                        { mathjs: 'Complex', re: 0, im: 0 },
                        { mathjs: 'Complex', re: 0, im: 0 },
                    ],
                    [
                        { mathjs: 'Complex', re: 5.446964599883029e-16, im: -4.74280470148566e-16 },
                        { mathjs: 'Complex', re: -3.449023245500995e-14, im: -5.311685326014862e-15 },
                        { mathjs: 'Complex', re: -267.6222865863367, im: 85.92054196674941 },
                        { mathjs: 'Complex', re: -5.668760170329847e-14, im: -2.8034117072219284e-14 },
                        { mathjs: 'Complex', re: 320.21893862176546, im: -128.1402643954027 },
                        { mathjs: 'Complex', re: -52.59665203542871, im: 42.21972242865334 },
                    ],
                    [
                        { mathjs: 'Complex', re: 2.8390716192815686e-32, im: -6.13269882085737e-33 },
                        { mathjs: 'Complex', re: -1.5568067077992095e-30, im: 7.166446193494254e-31 },
                        { mathjs: 'Complex', re: -3.7630386316630443e-16, im: -4.687933082457501e-16 },
                        { mathjs: 'Complex', re: 0, im: 0 },
                        { mathjs: 'Complex', re: -52.59665203542871, im: 42.21972242865334 },
                        { mathjs: 'Complex', re: 52.59665203542871, im: -42.21972242865334 },
                    ],
                ],
                size: [6, 6],
            },
            connectivity: {
                mathjs: 'DenseMatrix',
                data: [
                    [0, 1, 0, 1, 0, 0],
                    [1, 0, 1, 0, 0, 0],
                    [0, 1, 0, 1, 1, 0],
                    [1, 0, 1, 0, 0, 0],
                    [0, 0, 1, 0, 0, 1],
                    [0, 0, 0, 0, 1, 0],
                ],
                size: [6, 6],
            },
        },
        messages: [
            { source: 'callAPI', message: 'Using API type', value: 'local', type: 'message' },
            { source: 'Loadflow', message: 'Calculation ended after 0.004s and 1 iterations', value: null, type: 'message' },
        ],
        options: {
            calcIk1: false,
            calcIk2: false,
            calcIk3: true,
            calcIk2EE: false,
            calcLoadflow: false,
            cMax: 1.05,
            cMin: 0.95,
            maxLoadflowIterations: 250,
            gsAccelerationFactor: 1.5,
            respectQLimits: true,
            convergenceLimit: 0.00001,
            slackbus: {
                voltages: ['20000', '400'],
                calcIsc: false,
                calcLoadflow: false,
                showDescription: true,
                showVoltage: true,
                showAngle: false,
                showShortcircuit: true,
                showPower: false,
                showCurrent: true,
                showLosses: false,
                maxVoltageWarning: 10,
                displayUnits: 2,
                showIkMinMax: true,
            },
        },
        sBase: 100,
    };
    network = calcjs.calculateNet(network);
    const results = utils.getResultsActualUnits(network);

    const roundTo = 3;
    const tolerance = 1.2;

    // eslint-disable-next-line no-undef
    describe('(ID 1.1) Shortcircuit currents should match IEC 60909-4 within 1.2%', function () {
        // eslint-disable-next-line no-undef
        it(`Ik3 at node Q matches IEC 60909-4`, function () {
            const result = results.nodes[0].sc.ik3;
            const expected = 10;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik3 at node F1 matches IEC 60909-4`, function () {
            const result = results.nodes[1].sc.ik3;
            const expected = 34.62;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik1 at node F1 matches IEC 60909-4`, function () {
            const result = results.nodes[1].sc.ik1;
            const expected = 35.64;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik3 at node F2 matches IEC 60909-4`, function () {
            const result = results.nodes[2].sc.ik3;
            const expected = 34.12;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik1 at node F2 matches IEC 60909-4`, function () {
            const result = results.nodes[2].sc.ik1;
            const expected = 34.98;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik3 at node F3 matches IEC 60909-4`, function () {
            const result = results.nodes[5].sc.ik3;
            const expected = 6.95;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });

        // eslint-disable-next-line no-undef
        it(`Ik1 at node F3 matches IEC 60909-4`, function () {
            const result = results.nodes[5].sc.ik1;
            const expected = 4.83;
            assert.equal(utils.withinTolerance(result, expected, tolerance), true);
        });
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Loadflow Calculation ', function () {
    // Setup loadflow test
    // Reference network - Loadflow example 2
    // Create network
    let network = new networkjs.Network('net');
    network.options.calcIk1 = false;
    network.options.calcIk2 = false;
    network.options.calcIk3 = false;
    network.options.calcIk2EE = false;
    network.options.cMax = 1.0;
    network.options.calcLoadflow = true;
    network.options.gsAccelerationFactor = 1.4;
    network.options.maxLoadflowIterations = 50;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;

    network.sBase = 1;

    const bus1 = nodejs.NodeFactory('bus1');
    bus1.setBase(network.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('bus2');
    bus2.setBase(network.sBase, 1).setNode(2);

    const bus3 = nodejs.NodeFactory('bus3');
    bus3.setBase(network.sBase, 1).setNode(3).setNodeType(nodejs.typeEnum.PV);

    const G1 = linkjs.LinkFactory('G1');
    G1.setBase(network.sBase, 1).setFrom(0).setTo(bus1.node);

    const G3 = linkjs.GeneratorFactory('G3');
    G3.setBase(network.sBase, 1).setFrom(0).setTo(bus3.node).setSn(2.5).setPf(0.8);
    bus3.addGeneratorPower(G3).setUFactor(1.04);

    const line12 = linkjs.LinkFactory('L12');
    line12.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus2.node).setZm(mathjs.complex(0.02, 0.04)).setShunt(mathjs.complex(0, 0.0000056386));

    const line13 = linkjs.LinkFactory('L13');
    line13.setBase(network.sBase, 1).setFrom(bus1.node).setTo(bus3.node).setZm(mathjs.complex(0.01, 0.03)).setShunt(mathjs.complex(0, 0.0000056386));

    const line23 = linkjs.LinkFactory('L23');
    line23
        .setBase(network.sBase, 1)
        .setFrom(bus2.node)
        .setTo(bus3.node)
        .setZm(mathjs.complex(0.0125, 0.025))
        .setShunt(mathjs.complex(0, 0.0000056386));

    // load2.P = 4.0; // 400 MW / 100 => 4.0
    // load2.Q = 2.5; // 250 MVAR / 100 => 2.5
    const load2 = linkjs.LoadFactory('Load2');
    load2.setP(4.0).setQ(2.5);
    bus2.addLoadPower(load2);

    // Add elements to network in sorted order
    network.addNode(bus1);
    network.addNode(bus2);
    network.addNode(bus3);

    // Add impedance objects
    network.addLinkImpedance(G1);
    network.addLinkImpedance(G3);
    network.addLinkImpedance(line12);
    network.addLinkImpedance(line13);
    network.addLinkImpedance(line23);

    network = calcjs.calculateNet(network);

    // eslint-disable-next-line no-undef
    describe('(ID 1.4) Generated y-bus should pass with known reference network', function () {
        // Expected y-bus
        //
        // Rounded
        //      20-50i	-10+20i	-10+30i
        // Y = -10+20i	 26-52i	-16+32i
        //     -10+30i	-16+32i	 26-62i

        const expected = [
            [mathjs.complex('19.99972-50.00159i'), mathjs.complex('-9.99993+19.99999i'), mathjs.complex('-9.99978+30.00160i')],
            [mathjs.complex('-9.99993+19.99999i'), mathjs.complex('26.00019-52.00004i'), mathjs.complex('-16.00026+32.000054i')],
            [mathjs.complex('-9.99978+30.00160i'), mathjs.complex('-16.00026+32.000054i'), mathjs.complex(' 26.00003-62.00165i')],
        ];

        // Set number of decimals for rounding
        const roundTo = 4;

        // Assert
        let res;
        let exp;
        let _pass = false;

        const BreakException = {};
        try {
            network.matrix.yBus.forEach((value, index, matrix) => {
                // res = mathjs.complex(value.re, value.im);
                res = mathjs.round(value, roundTo);
                exp = mathjs.round(expected[index[0]][index[1]], roundTo);
                _pass = mathjs.equal(res, exp);
                if (!_pass) {
                    console.log(`Generated y-bus value ${res} should equal ${exp} when rounded to ${roundTo} decimals`);
                    throw BreakException;
                }
            });
        } catch (error) {
            if (error !== BreakException) throw error;
        }

        // eslint-disable-next-line no-undef
        it(`returned y-bus which when rounded of to ${roundTo} decimals should equal expected reference y-bus`, function () {
            assert.equal(_pass, 1);
        });
    });
    // eslint-disable-next-line no-undef
    describe('(ID 1.3) Gauss-Seidel loadflow should pass with known reference network', function () {
        /*
        Expected load-flow
        Voltage 2 = 0.97168 angle -2.6948
        Voltage 3 = 1.04 angle -0.498
        Slack S = 2.1842 + 1.4085i
        G3 S = 2.0 + 1.46177i
        L12 Ploss = 0.08
        L12 Qloss = 0.168
        */
        // Create network

        // Set number of decimals for rounding
        const roundTo = 3;
        const roundToShort = 1;

        // Assert
        let _pass = true;

        // Voltage 2
        if (
            // eslint-disable-next-line prettier/prettier
            !(network.nodes[1].lf.U.abs().toFixed(roundTo) === (0.97168).toFixed(roundTo) &&
                ((network.nodes[1].lf.U.arg() * 180) / Math.PI).toFixed(roundToShort) === (-2.6948).toFixed(roundToShort)
            )
        ) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow voltage ${network.nodes[1].lf.U.abs().toFixed(
                    roundTo + 1
                )} rounded to ${roundTo} should be ${(0.97168).toFixed(roundTo)}`
            );
            console.log(
                `[Gauss-Seidel loadflow] Loadflow voltage angle ${((network.nodes[1].lf.U.arg() * 180) / Math.PI).toFixed(
                    roundToShort + 1
                )} rounded to ${roundToShort} should be ${(-2.6948).toFixed(roundToShort)}`
            );
            _pass = false;
        }

        // Voltage 3
        if (
            // eslint-disable-next-line prettier/prettier
            !(network.nodes[2].lf.U.abs()).toFixed(roundTo) === (1.04).toFixed(roundTo) &&
            ((network.nodes[2].lf.U.arg() * 180) / Math.PI).toFixed(roundTo) === (-0.498).toFixed(roundTo)
        ) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow voltage ${network.nodes[2].lf.U.abs().toFixed(
                    roundTo
                )} rounded to ${roundTo} should be ${(1.04).toFixed(roundTo)}`
            );
            console.log(
                `[Gauss-Seidel loadflow] Loadflow voltage angle ${((network.nodes[2].lf.U.arg() * 180) / Math.PI).toFixed(
                    roundTo
                )} rounded to ${roundToShort} should be ${(-0.498).toFixed(roundTo)}`
            );
            _pass = false;
        }

        // Slack P
        if (!(network.nodes[0].lf.Pgen.toFixed(roundTo) === (2.1842).toFixed(roundTo))) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow slackbus P ${network.nodes[0].lf.Pgen.toFixed(
                    roundTo
                )} rounded to ${roundToShort} should be ${(2.1842).toFixed(roundTo)}`
            );
            _pass = false;
        }

        // Slack Q
        if (!(network.nodes[0].lf.Qgen.toFixed(roundTo) === (1.4085).toFixed(roundTo))) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow slackbus Q ${network.nodes[0].lf.Qgen.toFixed(
                    roundTo
                )} rounded to ${roundToShort} should be ${(1.4085).toFixed(roundTo)}`
            );
            _pass = false;
        }

        // G3 P
        if (!(network.nodes[2].lf.Pgen.toFixed(roundTo) === (2.0).toFixed(roundTo))) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow slackbus P ${network.nodes[2].lf.Pgen.toFixed(
                    roundTo
                )} rounded to ${roundToShort} should be ${(2.0).toFixed(roundTo)}`
            );
            _pass = false;
        }

        // G3 Q
        if (!(network.nodes[2].lf.Qgen.toFixed(roundTo) === (1.46177).toFixed(roundTo))) {
            console.log(
                `[Gauss-Seidel loadflow] Loadflow slackbus Q ${network.nodes[2].lf.Qgen.toFixed(
                    roundTo
                )} rounded to ${roundToShort} should be ${(1.46177).toFixed(roundTo)}`
            );
            _pass = false;
        }

        // Links
        network.links.posSeq.forEach((link) => {
            if (link.from === 1 && link.to === 2) {
                if (!(link.Ploss.toFixed(roundTo) === (0.08393).toFixed(roundTo))) {
                    console.log(
                        `[Gauss-Seidel loadflow] Loadflow L12 Ploss ${link.Ploss.toFixed(
                            roundTo
                        )} rounded to ${roundTo} should be ${(0.08393).toFixed(roundTo)}`
                    );
                    _pass = false;
                }
                if (!(link.Qloss.toFixed(roundTo) === (0.16787).toFixed(roundTo))) {
                    console.log(
                        `[Gauss-Seidel loadflow] Loadflow L12 Qloss ${link.Qloss.toFixed(
                            roundTo
                        )} rounded to ${roundTo} should be ${(0.16787).toFixed(roundTo)}`
                    );
                    _pass = false;
                }
            }
        });
        // eslint-disable-next-line no-undef
        it(`Calculated load-flow when rounded of to ${roundTo} decimals should equal expected reference`, function () {
            assert.equal(_pass, true);
        });
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Loadflow Parallel Links', function () {
    // Setup loadflow test
    // Reference network - Parallel Lines example
    // Create network
    let network = networkjs.Network('Example Network');

    network.options.calcIk1 = false;
    network.options.calcIk2 = false;
    network.options.calcIk3 = true;
    network.options.calcIk2EE = false;
    network.options.calcLoadflow = true;
    network.options.cMax = 1.0;
    network.options.maxLoadflowIterations = 500;
    network.options.gsAccelerationFactor = 1.5;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;

    network.sBase = 100; // MVa
    const vBase = 10; // kV

    const bus1 = nodejs.NodeFactory('MV');
    bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('LV');
    bus2.setBase(network.sBase, vBase).setNode(2);

    const bus3 = nodejs.NodeFactory('A');
    bus3.setBase(network.sBase, vBase).setNode(3);

    const feeder = linkjs.LinkFactory('Feeder');
    const zk = 10 ** 2 / 500;
    const angle = Math.atan(10);
    const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
    let zm = mathjs.complex(new Complex((vBase, network.sBase, strZm)));
    feeder.setFrom(0).setTo(bus1.node).setBase(network.sBase, vBase).setZm(zm);

    const TR = linkjs.LinkFactory('TR');
    const Un1 = 10;
    const Un2 = 0.4;
    const Un = Math.min(Un1, Un2);
    const ztr = ((4 / 100) * Un ** 2) / 0.5;
    const ang = Math.atan(6);
    const strZtr = `${Math.cos(ang) * ztr}+${Math.sin(ang) * ztr}i`;
    TR.setFrom(bus1.node)
        .setTo(bus2.node)
        .setBase(network.sBase, vBase)
        .setZm(new Complex(utils.convertToPerUnit(vBase, network.sBase, strZtr)));

    const L13A = linkjs.LinkFactory('L13A');
    zm = mathjs.complex(new Complex(utils.convertToPerUnit(Un2, network.sBase, '0.0195+0.012i'))); // 0.15 km * 0.13+0.08i ohm/km
    L13A.setFrom(bus2.node).setTo(bus3.node).setBase(network.sBase, Un2).setZm(zm);

    const L13B = linkjs.LinkFactory('L13B');
    zm = mathjs.complex(new Complex(utils.convertToPerUnit(Un2, network.sBase, '0.0195+0.012i'))); // 0.15 km * 0.13+0.08i ohm/km
    L13B.setFrom(bus2.node).setTo(bus3.node).setBase(network.sBase, Un2).setZm(zm);

    const load3 = linkjs.LoadFactory('Load3');
    load3.setP(0.1 / network.sBase).setQ(0.01 / network.sBase);
    bus3.addLoadPower(load3);

    // Add elements to network in sorted order
    network.addNode(bus1);
    network.addNode(bus2);
    network.addNode(bus3);

    // Add impedance objects
    network.addLinkImpedance(feeder);
    network.addLinkImpedance(TR);
    network.addLinkImpedance(L13A);
    network.addLinkImpedance(L13B);

    network = calcjs.calculateNet(network);
    const results = utils.getResultsActualUnits(network);

    // eslint-disable-next-line no-undef
    describe('(ID 1.5) Gauss-Seidel loadflow should pass with parallel lines', function () {
        /*
        Load at bus3 is P=0.1 MVA Q=0.01 MVAr transportet through L13A and L13B in parallel, eg.
        L13A.P = load3.P/2 Qline = load3P/Q (accounting for losses)
        L13B.P = load3.P/2 Qline = load3P/Q (accounting for losses)
        */

        // Set number of decimals for rounding
        const roundTo = 4;

        // Assert
        // eslint-disable-next-line no-undef
        it(`TR P should equal expected load3.P + losses`, function () {
            // assert.equal(
            //     (network.links.posSeqForYbus[1].lf.P * network.sBase).toFixed(roundTo),
            //     ((load3.P + network.links.posSeqForYbus[2].lf.Ploss + network.links.posSeqForYbus[3].lf.Ploss) * network.sBase).toFixed(roundTo)
            // );
            assert.equal(
                results.links[1].P.toFixed(roundTo),
                (load3.P * network.sBase + results.links[2].Ploss + results.links[3].Ploss).toFixed(roundTo)
            );
        });
        // eslint-disable-next-line no-undef
        it(`TR Q should equal expected load3.Q + losses`, function () {
            assert.equal(
                results.links[1].Q.toFixed(roundTo),
                (load3.Q * network.sBase + results.links[2].Qloss + results.links[3].Qloss).toFixed(roundTo)
            );
        });
        // eslint-disable-next-line no-undef
        it(`${network.links.posSeqForYbus[2].name}.P should equal load3.P/2 + losses`, function () {
            // assert.equal(
            //     (network.links.posSeqForYbus[2].lf.P * network.sBase).toFixed(roundTo),
            //     ((load3.P / 2 + network.links.posSeqForYbus[2].lf.Ploss) * network.sBase).toFixed(roundTo)
            // );
            assert.equal(results.links[2].P.toFixed(roundTo), ((load3.P * network.sBase) / 2 + results.links[2].Ploss).toFixed(roundTo));
        });
        // eslint-disable-next-line no-undef
        it(`${network.links.posSeqForYbus[2].name}.Q should equal load3.Q/2 + losses`, function () {
            assert.equal(results.links[2].Q.toFixed(roundTo), ((load3.Q * network.sBase) / 2 + results.links[2].Qloss).toFixed(roundTo));
        });
        // eslint-disable-next-line no-undef
        it(`${network.links.posSeqForYbus[3].name}.P should equal load3.P/2 + losses`, function () {
            // assert.equal(
            //     (network.links.posSeqForYbus[3].lf.P * network.sBase).toFixed(roundTo),
            //     ((load3.P / 2 + network.links.posSeqForYbus[3].lf.Ploss) * network.sBase).toFixed(roundTo)
            // );
            assert.equal(results.links[3].P.toFixed(roundTo), ((load3.P * network.sBase) / 2 + results.links[3].Ploss).toFixed(roundTo));
        });
        // eslint-disable-next-line no-undef
        it(`${network.links.posSeqForYbus[3].name}.Q should equal load3.Q/2 + losses`, function () {
            // assert.equal(
            //     (network.links.posSeqForYbus[3].lf.Q * network.sBase).toFixed(roundTo),
            //     ((load3.Q / 2 + network.links.posSeqForYbus[3].lf.Qloss) * network.sBase).toFixed(roundTo)
            // );
            assert.equal(results.links[3].Q.toFixed(roundTo), ((load3.Q * network.sBase) / 2 + results.links[3].Qloss).toFixed(roundTo));
        });
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Shunt Capacitance ', function () {
    // Setup loadflow test
    // Reference network - Parallel Lines example
    // Create network
    let network = networkjs.Network('Example Network');

    network.options.calcIk1 = false;
    network.options.calcIk2 = false;
    network.options.calcIk3 = true;
    network.options.calcIk2EE = false;
    network.options.calcLoadflow = true;
    network.options.cMax = 1.0;
    network.options.maxLoadflowIterations = 500;
    network.options.gsAccelerationFactor = 1.5;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;
    network.sBase = 100; // MVa
    const vBase = 10; // kV

    const bus1 = nodejs.NodeFactory('Net');
    bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('Bus2');
    bus2.setBase(network.sBase, vBase).setNode(2);

    const bus3 = nodejs.NodeFactory('Bus3');
    bus3.setBase(network.sBase, vBase).setNode(3);

    const feeder = linkjs.LinkFactory('Feeder');
    const zk = 10 ** 2 / 500;
    const angle = Math.atan(10);
    const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
    let zm = mathjs.complex(new Complex((vBase, network.sBase, strZm)));
    feeder.setFrom(0).setTo(bus1.node).setBase(network.sBase, vBase).setZm(zm);

    const L12 = linkjs.LinkFactory('L12');
    zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '1.875+1.2i'))); // 15 km * 0.125+0.08i ohm/km
    L12.setFrom(bus1.node).setTo(bus2.node).setBase(network.sBase, vBase).setZm(zm);

    const L13 = linkjs.LinkFactory('L13');
    zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '1.875+1.2i'))); // 15 km * 0.125+0.08i ohm/km
    const shunt = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '0+0.001425i'))); // 15 km * 0.000095i ohm/km
    L13.setFrom(bus1.node).setTo(bus3.node).setBase(network.sBase, vBase).setZm(zm).setShunt(shunt);

    const load2 = linkjs.LoadFactory('Load2');
    load2.setP(1.6 / network.sBase).setQ(0.2 / network.sBase);
    bus2.addLoadPower(load2);

    const load3 = linkjs.LoadFactory('Load3');
    load3.setP(1.6 / network.sBase).setQ(0.2 / network.sBase);
    bus3.addLoadPower(load3);

    // Add elements to network in sorted order
    network.addNode(bus1);
    network.addNode(bus2);
    network.addNode(bus3);

    // Add impedance objects
    network.addLinkImpedance(feeder);
    network.addLinkImpedance(L12);
    network.addLinkImpedance(L13);

    network = calcjs.calculateNet(network);

    // Assert
    // eslint-disable-next-line no-undef
    it(`(ID 1.6) Loadflow calculations must take into account lines with no shunt capacitance`, function () {
        assert.equal((network.nodes[1].lf.U.abs() * network.nodes[1].vBase).toFixed(3), 9.663);
    });
    // eslint-disable-next-line no-undef
    it(`(ID 1.6) Loadflow calculations must take into account lines with shunt capacitance`, function () {
        assert.equal((network.nodes[2].lf.U.abs() * network.nodes[2].vBase).toFixed(3), 9.672);
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Generator Q-limits ', function () {
    // Setup loadflow test
    // Reference network - Generator-Qlimits example
    // Create network
    let networkNoQLimits = networkjs.Network('Example Network');
    networkNoQLimits.options.calcIk1 = false;
    networkNoQLimits.options.calcIk2 = false;
    networkNoQLimits.options.calcIk3 = false;
    networkNoQLimits.options.calcIk2EE = false;
    networkNoQLimits.options.cMax = 1.0;
    networkNoQLimits.options.calcLoadflow = true;
    networkNoQLimits.options.respectQLimits = false;
    networkNoQLimits.options.gsAccelerationFactor = 1.4;
    networkNoQLimits.options.maxLoadflowIterations = 50;
    networkNoQLimits.options.convergenceLimit = 1e-7;
    networkNoQLimits.sBase = 1; // MVA

    let networkWithQLimits = networkjs.Network('Example Network');
    networkWithQLimits.options.calcIk1 = false;
    networkWithQLimits.options.calcIk2 = false;
    networkWithQLimits.options.calcIk3 = false;
    networkWithQLimits.options.calcIk2EE = false;
    networkWithQLimits.options.cMax = 1.0;
    networkWithQLimits.options.calcLoadflow = true;
    networkWithQLimits.options.respectQLimits = true;
    networkWithQLimits.options.gsAccelerationFactor = 1.4;
    networkWithQLimits.options.maxLoadflowIterations = 50;
    networkWithQLimits.options.convergenceLimit = 1e-7;
    networkWithQLimits.sBase = 1; // MVA

    const bus1A = nodejs.NodeFactory('bus1A');
    bus1A.setBase(networkNoQLimits.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

    const bus1B = nodejs.NodeFactory('bus1B');
    bus1B.setBase(networkNoQLimits.sBase, 1).setNode(1).setUFactor(1.05).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('bus2');
    bus2.setBase(networkNoQLimits.sBase, 1).setNode(2);

    const bus3A = nodejs.NodeFactory('bus3A');
    bus3A.setBase(networkNoQLimits.sBase, 1).setNode(3).setNodeType(nodejs.typeEnum.PV);

    const bus3B = nodejs.NodeFactory('bus3B');
    bus3B.setBase(networkNoQLimits.sBase, 1).setNode(3).setNodeType(nodejs.typeEnum.PV);

    const G1 = linkjs.LinkFactory('G1');
    G1.setBase(networkNoQLimits.sBase, 1).setFrom(0).setTo(bus1A.node);

    const G3A = linkjs.GeneratorFactory('G3A');
    G3A.setBase(networkNoQLimits.sBase, 1).setFrom(0).setTo(bus3A.node).setSn(2.5).setPf(0.8);
    bus3A.addGeneratorPower(G3A).setUFactor(1.04);

    const G3B = linkjs.GeneratorFactory('G3B');
    G3B.setBase(networkNoQLimits.sBase, 1).setFrom(0).setTo(bus3B.node).setSn(2.5).setPf(0.8);
    bus3B.addGeneratorPower(G3B).setUFactor(1.04);

    const line12 = linkjs.LinkFactory('L12');
    line12
        .setBase(networkNoQLimits.sBase, 1)
        .setFrom(bus1A.node)
        .setTo(bus2.node)
        .setZm(mathjs.complex(0.02, 0.04))
        .setShunt(mathjs.complex(0, 0.0000056386));

    const line13 = linkjs.LinkFactory('L13');
    line13
        .setBase(networkNoQLimits.sBase, 1)
        .setFrom(bus1A.node)
        .setTo(bus3A.node)
        .setZm(mathjs.complex(0.01, 0.03))
        .setShunt(mathjs.complex(0, 0.0000056386));

    const line23 = linkjs.LinkFactory('L23');
    line23
        .setBase(networkNoQLimits.sBase, 1)
        .setFrom(bus2.node)
        .setTo(bus3A.node)
        .setZm(mathjs.complex(0.0125, 0.025))
        .setShunt(mathjs.complex(0, 0.0000056386));

    // load2.P = 4.0 pu
    // load2.Q = 3.5 pu
    const load2 = linkjs.LoadFactory('Load2');
    load2.setP(4.0).setQ(3.5);
    bus2.addLoadPower(load2);

    // Add elements to network in sorted order
    networkNoQLimits.addNode(bus1A);
    networkNoQLimits.addNode(bus2);
    networkNoQLimits.addNode(bus3A);
    networkNoQLimits.addLinkImpedance(G1);
    networkNoQLimits.addLinkImpedance(G3A);
    networkNoQLimits.addLinkImpedance(line12);
    networkNoQLimits.addLinkImpedance(line13);
    networkNoQLimits.addLinkImpedance(line23);

    networkWithQLimits.addNode(bus1B);
    networkWithQLimits.addNode(bus2);
    networkWithQLimits.addNode(bus3B);
    networkWithQLimits.addLinkImpedance(G1);
    networkWithQLimits.addLinkImpedance(G3B);
    networkWithQLimits.addLinkImpedance(line12);
    networkWithQLimits.addLinkImpedance(line13);
    networkWithQLimits.addLinkImpedance(line23);

    // Assert without Q limits

    networkNoQLimits = calcjs.calculateNet(networkNoQLimits);

    // eslint-disable-next-line no-undef
    describe('(ID 1.7) Gauss-Seidel loadflow respectQLimits=false should pass with reference network ', function () {
        /*
        No Q-limits && Load 4+3.5i
        bus1 Pgen 2.24 Qgen 1.826
        bus3 Pgen 2 Qgen 2.159
        */

        // Set number of decimals for rounding
        const roundTo = 3;

        // Assert
        // eslint-disable-next-line no-undef
        it(`Pgen at Bus1 should equal expected 2.24`, function () {
            assert.equal((networkNoQLimits.nodes[0].lf.Pgen * networkNoQLimits.nodes[0].sBase).toFixed(roundTo), 2.242);
        });
        // eslint-disable-next-line no-undef
        it(`Qgen at Bus1 should equal expected 1.826`, function () {
            assert.equal((networkNoQLimits.nodes[0].lf.Qgen * networkNoQLimits.nodes[0].sBase).toFixed(roundTo), 1.826);
        });
        // eslint-disable-next-line no-undef
        it(`Pgen at Bus3 should equal expected 2`, function () {
            assert.equal((networkNoQLimits.nodes[2].lf.Pgen * networkNoQLimits.nodes[2].sBase).toFixed(roundTo), 2);
        });
        // eslint-disable-next-line no-undef
        it(`Qgen at Bus3 should equal expected 2.159`, function () {
            assert.equal((networkNoQLimits.nodes[2].lf.Qgen * networkNoQLimits.nodes[2].sBase).toFixed(roundTo), 2.159);
        });
    });

    // Assert with Q limits
    networkWithQLimits = calcjs.calculateNet(networkWithQLimits);

    // eslint-disable-next-line no-undef
    describe('(ID 1.7) Gauss-Seidel loadflow respectQLimits=true should pass with reference network ', function () {
        /*
        Respect Q-limits && Load 4+3.5i
        bus1 Pgen 2 Qgen 1.914
        bus3 Pgen 2 Qgen 1.5
        */

        // Set number of decimals for rounding
        const roundTo = 3;

        // Assert
        // eslint-disable-next-line no-undef
        it(`Pgen at Bus1 should equal expected 1.999`, function () {
            assert.equal((networkWithQLimits.nodes[0].lf.Pgen * networkWithQLimits.nodes[0].sBase).toFixed(roundTo), 1.999);
        });
        // eslint-disable-next-line no-undef
        it(`Qgen at Bus1 should equal expected 1.914`, function () {
            assert.equal((networkWithQLimits.nodes[0].lf.Qgen * networkWithQLimits.nodes[0].sBase).toFixed(roundTo), 1.914);
        });
        // eslint-disable-next-line no-undef
        it(`Pgen at Bus3 should equal expected 2`, function () {
            assert.equal((networkWithQLimits.nodes[2].lf.Pgen * networkWithQLimits.nodes[2].sBase).toFixed(roundTo), 2);
        });
        // eslint-disable-next-line no-undef
        it(`Qgen at Bus3 should equal expected 1.5`, function () {
            assert.equal((networkWithQLimits.nodes[2].lf.Qgen * networkWithQLimits.nodes[2].sBase).toFixed(roundTo), 1.5);
        });
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Loadflow Calculation Single Phase', function () {
    // Setup loadflow test
    // Single_Phase_Example
    // Create network
    let network = networkjs.Network('Example Network');
    network.options.calcIk1 = false;
    network.options.calcIk2 = false;
    network.options.calcIk3 = false;
    network.options.calcIk2EE = false;
    network.options.calcLoadflow = true;
    network.options.cMax = 1.0;
    network.options.maxLoadflowIterations = 500;
    network.options.gsAccelerationFactor = 1.5;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;
    network.sBase = 100; // MVA
    const vBase = 0.4; // kV

    const bus1 = nodejs.NodeFactory('bus1');
    bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('bus2');
    bus2.setBase(network.sBase, vBase).setNode(2).setSinglePhase(true);

    const bus3 = nodejs.NodeFactory('bus3');
    bus3.setBase(network.sBase, vBase).setNode(3);

    const line12 = linkjs.LinkFactory('L12');
    line12
        .setBase(network.sBase, vBase)
        .setSinglePhase(true)
        .setFrom(bus1.node)
        .setTo(bus2.node)
        .setZm(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3}+${0.1 * 30e-3}i`)) // 0,030 km * 12.1+0.1i
        .setZ0(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3 * 4}+${0.1 * 30e-3 * 4}i`)); // 4*Zm

    const load2 = linkjs.LoadFactory('Load');
    load2.setP(0.0023 / network.sBase).setQ(0);
    bus2.addLoadPower(load2);

    const line13 = linkjs.LinkFactory('L13');
    line13
        .setBase(network.sBase, vBase)
        .setFrom(bus1.node)
        .setTo(bus3.node)
        .setZm(utils.convertToPerUnit(vBase, network.sBase, `${12.1 * 30e-3}+${0.1 * 30e-3}i`)); // 0,030 km * 12.1+0.1i

    const load3 = linkjs.LoadFactory('Load');
    load3.setP(0.0023 / network.sBase).setQ(0);
    bus3.addLoadPower(load3);

    const feeder = linkjs.LinkFactory('Feeder');
    feeder
        .setBase(network.sBase, vBase)
        .setFrom(0)
        .setTo(bus1.node)
        .setZm(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'))
        .setZ0(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'));
    network.addLinkImpedance(feeder);

    // Add elements to network in sorted order
    network.addNode(bus1);
    network.addNode(bus2);
    network.addNode(bus3);

    // Add impedance objects
    network.addLinkImpedance(line12);
    network.addLinkImpedance(line13);

    network = calcjs.calculateNet(network);
    const results = utils.getResultsActualUnits(network);

    // eslint-disable-next-line no-undef
    describe('(ID 1.12) Loadflow calculation must work with 3-phase and 1-phase cables', function () {
        // Expected
        //
        // Bus2 load 2.3 kW @ 3-phase 400V => I=3.3198 A
        // Bus3 load 2.3 kW @ 1-phase 400V/SQRT(3) ~= 231V =>I=9.9593 A

        // Assert
        let Ib = 0;

        it(`2.3kW load @ 231V must yield phase current ~10A`, function () {
            Ib = results.links[1].Ib;
            assert.equal(Ib.toFixed(0), 10);
        });

        it(`2.3kW load @ 400V must yield line current ~3.3A`, function () {
            Ib = results.links[2].Ib;
            assert.equal(Ib.toFixed(1), 3.3);
        });

        it(`2.3kW load @ 231V must yield voltage drop ~3.2%`, function () {
            assert.equal(results.links[1].dU.toFixed(1), 3.2);
        });

        it(`2.3kW load @ 400V must yield voltage drop ~0.6%`, function () {
            assert.equal(results.links[2].dU.toFixed(1), 0.5);
        });

        it(`Bus2 Uln voltage voltage must be ~ 224V `, function () {
            assert.equal((results.nodes[1].lf.Uln * 1000).toFixed(0), 224);
        });

        it(`Bus3 U voltage voltage must be ~ 398V`, function () {
            assert.equal((results.nodes[2].lf.U * 1000).toFixed(0), 398);
        });
    });
});

// eslint-disable-next-line no-undef
describe('Integration test - Loadflow Calculation Single Phase', function () {
    // Setup loadflow test
    // Single_Phase_Example
    // Create network
    let network = networkjs.Network('Example Network');
    network.options.calcIk1 = false;
    network.options.calcIk2 = false;
    network.options.calcIk3 = false;
    network.options.calcIk2EE = false;
    network.options.calcLoadflow = true;
    network.options.cMax = 1.0;
    network.options.maxLoadflowIterations = 500;
    network.options.gsAccelerationFactor = 1.5;
    network.options.respectQLimits = true;
    network.options.convergenceLimit = 1e-7;
    network.sBase = 100; // MVA
    const vBase = 0.4; // kV

    const bus1 = nodejs.NodeFactory('bus1');
    bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

    const bus2 = nodejs.NodeFactory('bus2');
    bus2.setBase(network.sBase, vBase).setNode(2);

    const bus3 = nodejs.NodeFactory('bus3');
    bus3.setBase(network.sBase, vBase).setNode(3);

    const line12 = linkjs.LinkFactory('L12');
    line12
        .setBase(network.sBase, vBase)
        .setSinglePhase(true)
        .setFrom(bus1.node)
        .setTo(bus2.node)
        .setZm(utils.convertToPerUnit(vBase, network.sBase, '0,18+0,0015i')); // 0,015 km * 12.1+0.1i

    const load2 = linkjs.LoadFactory('Load');
    load2.setP(0.0023 / network.sBase).setQ(0);
    bus2.addLoadPower(load2);

    const line13 = linkjs.LinkFactory('L12');
    line13.setBase(network.sBase, vBase).setFrom(bus1.node).setTo(bus3.node).setZm(utils.convertToPerUnit(vBase, network.sBase, '0,18+0,0015i')); // 0,015 km * 12.1+0.1i

    const load3 = linkjs.LoadFactory('Load');
    load3.setP(0.0023 / network.sBase).setQ(0);
    bus3.addLoadPower(load3);

    const feeder = linkjs.LinkFactory('Feeder');
    feeder.setBase(network.sBase, vBase).setFrom(0).setTo(bus1.node).setZm(mathjs.complex('0.03980148760839952 + 0.39801487608399566i'));
    network.addLinkImpedance(feeder);

    // Add elements to network in sorted order
    network.addNode(bus1);
    network.addNode(bus2);
    network.addNode(bus3);

    // Add impedance objects
    network.addLinkImpedance(line12);
    network.addLinkImpedance(line13);

    network = calcjs.calculateNet(network);

    // eslint-disable-next-line no-undef
    describe('(ID 1.12) Loadflow calculation must work with 3-phase and 1-phase cables', function () {
        // Expected
        //
        // Bus2 load 2.3 kW @ 3-phase 400V => I=3.3198 A
        // Bus3 load 2.3 kW @ 1-phase 400V/SQRT(3) ~= 231V =>I=9.9593 A

        // Assert
        const lineCurrent =
            ((Math.sqrt(network.links.posSeqForYbus[1].P ** 2 + network.links.posSeqForYbus[1].Q ** 2) * network.nodes[1].sBase) /
                network.links.posSeqForYbus[1].vBase /
                Math.sqrt(3)) *
            1000;
        const phaseCurrent =
            ((Math.sqrt(network.links.posSeqForYbus[2].P ** 2 + network.links.posSeqForYbus[2].Q ** 2) * network.nodes[1].sBase) /
                network.links.posSeqForYbus[2].vBase /
                Math.sqrt(3)) *
            1000 *
            3;

        it(`2.3kW load @ 400V must yield line current ~3.3A`, function () {
            assert.equal(lineCurrent % 3.3 < 0.1, true);
        });

        it(`2.3kW load @ 231V must yield phase current ~9.9A`, function () {
            assert.equal(phaseCurrent % 9.9 < 0.1, true);
        });
    });
});
