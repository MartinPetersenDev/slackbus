/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

// Import Class under test
const cut = require('');

// Config template string
const strTestType = 'xxx Unit test';
let strDescr = '';
let strShould = '';
const strExpected = 'expected';
const strWhen = 'when called';
const strFound = ', got result ';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Class */
    strDescr = 'Class ';
    strShould = 'should contain ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}${result}`, function () {
            assert.strictEqual(result, true);
        });
    });
    /** Methods */
    strDescr = 'Method xxx() ';
    strShould = 'should do ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}${result}`, function () {
            assert.strictEqual(result, true);
        });
    });
});
