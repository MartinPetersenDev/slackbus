/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

// Import Class under test
const shortcircuit = require('../../src/engine/shortcircuit');
const mathjs = require('mathjs');

// Config template string
const strTestType = 'Shortcircuit Unit test';
let strDescr = '';
let strShould = '';
let strExpected = '';
const strWhen = '';
// const strFound = ', got result ';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Methods */
    strDescr = 'Method ';

    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert

        strShould = 'calculateIsc3() should throw error when called with impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc3(10, 1, 1);
            }, Error);
        });

        strShould = 'calculateIsc2() should throw error when called with impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc2(10, 1, 1);
            }, Error);
        });

        strShould = 'calculateIsc2EE() should throw error when called with impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc2EE(10, mathjs.complex(1, 2), 10, 1);
            }, Error);
        });

        strShould = 'calculateIsc2EE() should throw error when called with z0 impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc2EE(10, 10, mathjs.complex(1, 2), 1);
            }, Error);
        });

        strShould = 'calculateIsc1() should throw error when called with impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc1(10, 10, mathjs.complex(1, 2), 1);
            }, Error);
        });

        strShould = 'calculateIsc1() should throw error when called with z_0 impedance with typeof number';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                shortcircuit.calculateIsc1(10, mathjs.complex(1, 2), 10, 1);
            }, Error);
        });
    });
});
