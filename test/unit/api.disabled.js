/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');
var sinon = require('sinon');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

// Config template string
const strTestType = 'API Unit test';
let strDescr = '';
let strShould = '';
const strExpected = '';
const strWhen = '';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    strDescr = '(ID 1.10) RESTful API should';
    strShould = ' produce same calculation result as local API';

    // RESTful API is async, so promise will be passed to it()
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, () => {
        // Stub console.log for this test .. restore it when promise is resolved
        console.log = sinon.stub();
        // Setup network
        const networkjs = require('../../src/sdk/network');
        const nodejs = require('../../src/sdk/node');
        const linkjs = require('../../src/sdk/link');
        const mathjs = require('mathjs');
        const calcjs = require('../../src/engine/calculateNet');
        const Complex = require('complex.js');
        const utils = require('../../src/sdk/utils');
        const api = require('../../src/engine/api');
        const networkREST = networkjs.Network('Network for REST API');
        let network = networkjs.Network('Network for local ');

        network.options.calcIk1 = false;
        network.options.calcIk2 = false;
        network.options.calcIk3 = true;
        network.options.calcIk2EE = false;
        network.options.calcLoadflow = true;
        network.options.cFactor = 1.0;
        network.options.maxLoadflowIterations = 500;
        network.options.gsAccelerationFactor = 1.5;
        network.options.respectQLimits = true;
        network.options.convergenceLimit = 1e-7;

        networkREST.options = network.options;

        network.sBase = 100; // MVa
        const vBase = 10; // kV

        const bus1 = nodejs.NodeFactory('Net');
        bus1.setBase(network.sBase, vBase).setNode(1).setUFactor(1.0).setNodeType(nodejs.typeEnum.slackbus);

        const bus2 = nodejs.NodeFactory('Bus2');
        bus2.setBase(network.sBase, vBase).setNode(2);

        const bus3 = nodejs.NodeFactory('Bus3');
        bus3.setBase(network.sBase, vBase).setNode(3);

        const feeder = linkjs.LinkFactory('Feeder');
        const zk = 10 ** 2 / 500;
        const angle = Math.atan(10);
        const strZm = `${Math.cos(angle) * zk}+${Math.sin(angle) * zk}i`;
        let zm = mathjs.complex(new Complex((vBase, network.sBase, strZm)));
        feeder.setFrom(0).setTo(bus1.node).setBase(network.sBase, vBase).setZm(zm);

        const L12 = linkjs.LinkFactory('L12');
        zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '1.875+1.2i'))); // 15 km * 0.125+0.08i ohm/km
        L12.setFrom(bus1.node).setTo(bus2.node).setBase(network.sBase, vBase).setZm(zm);

        const L13 = linkjs.LinkFactory('L13');
        zm = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '1.875+1.2i'))); // 15 km * 0.125+0.08i ohm/km
        const shunt = mathjs.complex(new Complex(utils.convertToPerUnit(vBase, network.sBase, '0+0.001425i'))); // 15 km * 0.000095i ohm/km
        L13.setFrom(bus1.node).setTo(bus3.node).setBase(network.sBase, vBase).setZm(zm).setShunt(shunt);

        const load2 = linkjs.LoadFactory('Load2');
        load2.setP(1.6 / network.sBase).setQ(0.2 / network.sBase);
        bus2.addLoadPower(load2);

        const load3 = linkjs.LoadFactory('Load3');
        load3.setP(1.6 / network.sBase).setQ(0.2 / network.sBase);
        bus3.addLoadPower(load3);

        // Add elements to network in sorted order
        network.addNode(bus1);
        network.addNode(bus2);
        network.addNode(bus3);
        networkREST.addNode(bus1);
        networkREST.addNode(bus2);
        networkREST.addNode(bus3);

        // Add impedance objects
        network.addLinkImpedance(feeder);
        network.addLinkImpedance(L12);
        network.addLinkImpedance(L13);
        networkREST.addLinkImpedance(feeder);
        networkREST.addLinkImpedance(L12);
        networkREST.addLinkImpedance(L13);

        // Calculate with local API
        network = calcjs.calculateNet(network);

        return api.apiCalculateNet(networkREST, 'https://l9im0ttsze.execute-api.eu-central-1.amazonaws.com/dev/calculateNet').then((result) => {
            if (console.log.restore) console.log.restore();
            assert.equal(JSON.stringify(result.matrix), JSON.stringify(network.matrix));
        });
    });
});
