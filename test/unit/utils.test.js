/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');
var sinon = require('sinon');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

// Config template string
const strTestType = 'API utils Unit test';
let strDescr = '';
let strShould = '';
const strExpected = '';
const strWhen = '';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    strDescr = 'resultsToConsole should';
    strShould = ' be typeOf "function"';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(typeof utils.resultsToConsole, typeof 'str'.toString);
    });

    strDescr = 'debugLog should';
    strShould = ' be typeOf "function"';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(typeof utils.debugLog, typeof 'str'.toString);
    });

    strDescr = 'complexMatrixToArray should';
    strShould = ' throw error if not passed Matrix argument';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.throws(function () {
            utils.complexMatrixToArray({ element: 'foo' });
        }, Error);
    });

    strDescr = 'convertNodeUtoComplex should';
    strShould = ' throw error if busbar.U is not a number or Complex';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        const networkjs = require('../../src/sdk/network');
        const nodejs = require('../../src/sdk/node');
        // Create test network
        const network = networkjs.Network('Network for local ');
        const bus1 = nodejs.NodeFactory('Net');
        bus1.lf.U = 'foo';
        network.addNode(bus1);

        assert.throws(function () {
            utils.convertNodeUtoComplex(network);
        }, Error);
    });

    strDescr = 'convertNodeUtoComplex should';
    strShould = ' throw error if busbar.Uln is not a number or Complex';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        const networkjs = require('../../src/sdk/network');
        const nodejs = require('../../src/sdk/node');
        // Create test network
        const network = networkjs.Network('Network for local ');
        const bus1 = nodejs.NodeFactory('Net');
        bus1.lf.Uln = 'foo';
        network.addNode(bus1);

        assert.throws(function () {
            utils.convertNodeUtoComplex(network);
        }, Error);
    });

    strDescr = 'withinTolerance should';
    strShould = ' return false if a=0.89 b=1.0 tolerance=10';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(utils.withinTolerance(0.89, 1.0, 10), false);
    });

    strDescr = 'withinTolerance should';
    strShould = ' return true if a=0.91 b=1.0 tolerance=10';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(utils.withinTolerance(0.91, 1.0, 10), true);
    });

    strDescr = 'withinTolerance should';
    strShould = ' return true if a=1.09 b=1.0 tolerance=10';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(utils.withinTolerance(1.09, 1.0, 10), true);
    });

    strDescr = 'withinTolerance should';
    strShould = ' return false if a=1.11 b=1.0 tolerance=10';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const utils = require('../../src/sdk/utils');
        assert.equal(utils.withinTolerance(1.11, 1.0, 10), false);
    });
});
