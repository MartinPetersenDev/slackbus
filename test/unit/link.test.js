/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

// Import Class under test
const linkjs = require('../../src/sdk/link');

// Config template string
let strTestType = '';
let strDescr = '';
let strShould = '';
let strExpected = '';
const strWhen = '';
const strFound = ', got result ';

strTestType = 'Link Unit test'; // Generator extends Link extends NetElement
// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Class */
    strDescr = 'Class';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const result = linkjs.complex;

        // Assert re
        strShould = '.re should equal ';
        strExpected = '1e9';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${result.re}`, function () {
            assert.strictEqual(result.re, 1e9);
        });

        // Assert im
        strShould = '.im should equal ';
        strExpected = '1e9';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${result.im}`, function () {
            assert.strictEqual(result.im, 1e9);
        });
    });

    /** Methods */
    strDescr = 'Method ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert re
        strShould = 'setRfault() should accept numbers';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}`, function () {
            const line = linkjs.LinkFactory('test');
            line.setRfault(1.5);
            assert.strictEqual(line.Rfault, 1.5);
        });

        // Assert re
        strShould = 'setRfault() should accept strings and convert to numbers';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}`, function () {
            const line = linkjs.LinkFactory('test');
            line.setRfault('1,9');
            assert.strictEqual(line.Rfault, 1.9);
        });
    });
});

strTestType = 'Generator Unit test'; // Generator extends Link extends NetElement
// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Class */
    strDescr = 'Class ';
    strShould = 'should contain';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const generator = linkjs.GeneratorFactory('Generator');
        generator.setFrom(0).setTo(1).setSn('0.2').setPf('0,8').setBase(10, 1);

        // Assert from
        strExpected = ' from field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.from}`, function () {
            assert.strictEqual(typeof generator.from, typeof 1);
        });

        // Assert to
        strExpected = ' to field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.to}`, function () {
            assert.strictEqual(typeof generator.to, typeof 1);
        });

        // Assert fromZeroSeq
        strExpected = ' fromZeroSeq field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.fromZeroSeq}`, function () {
            assert.strictEqual(typeof generator.fromZeroSeq, typeof 1);
        });

        // Assert toZeroSeq
        strExpected = ' toZeroSeq field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.toZeroSeq}`, function () {
            assert.strictEqual(typeof generator.toZeroSeq, typeof 1);
        });

        // Assert Sn
        strExpected = ' Sn field typeof number when constructor called with dot string 0.2';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.Sn}`, function () {
            assert.strictEqual(typeof generator.Sn, typeof 1);
        });

        // Assert Sn value
        strExpected = ' Sn field value = 0.2 when constructor called with dot string 0.2';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.Sn}`, function () {
            assert.strictEqual(generator.Sn, 0.2);
        });

        // Assert pf
        strExpected = ' pf field typeof number when constructor called with comma string 0,8';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.pf}`, function () {
            assert.strictEqual(typeof generator.pf, typeof 1);
        });

        // Assert pf value
        strExpected = ' pf field value = 0.8 when constructor called with comma string 0,8';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.pf}`, function () {
            assert.strictEqual(generator.pf, 0.8);
        });

        // Assert P
        strExpected = ' P field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.P}`, function () {
            assert.strictEqual(typeof generator.P, typeof 1);
        });

        // Assert P value
        strExpected = ' P value = 0.15 when called with Sn=0.2 pf=0.8'; // Sn * Pf = 0.2 * 0.8 = 0.16
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.P.toFixed(8)}`, function () {
            assert.equal(generator.P.toFixed(8), 0.16);
        });

        // Assert Q
        strExpected = ' Q field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof generator.Q}`, function () {
            assert.strictEqual(typeof generator.Q, typeof 1);
        });

        // Assert QlimitMax
        strExpected = ' QlimitMax value = 0.12 when called with Sn=0.2 pf=0.8'; // sin(acos(0.8)) * 0.2 = 0.12
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.QlimitMax.toFixed(8)}`, function () {
            assert.equal(generator.QlimitMax.toFixed(8), 0.12);
        });

        // Assert QlimitMin
        strExpected = ' QlimitMin value = -0.04 when called with Sn=0.2 pf=0.8'; // -1 * QlimitMax / 3
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.QlimitMin.toFixed(8)}`, function () {
            assert.equal(generator.QlimitMin.toFixed(8), -0.04);
        });

        // Assert zm
        strExpected = ' zm field typeof netelm.complex';
        const netelmjs = require('../../src/sdk/link');
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.zm}`, function () {
            assert.deepEqual(generator.zm, netelmjs.complex);
        });

        // Assert z0
        strExpected = ' z0 field typeof netelm.complex';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${generator.z0}`, function () {
            assert.deepEqual(generator.z0, netelmjs.complex);
        });
    });

    /** Methods */
    strDescr = 'Method ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const generator = linkjs.GeneratorFactory('Generator');

        const setUpOK = generator.Sn === 1 && generator.pf === 1; // Default is 1

        if (!setUpOK) {
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        // Assert Sn
        strShould = 'setSn(num) should ';
        strExpected = ' set .Sn ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn(2.0);
            assert.equal(generator.Sn, 2.0);
        });
        strShould = 'typeOf Sn should be ';
        strExpected = '"number" when set as number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn(2.0);
            assert.equal(typeof generator.Sn, typeof 1);
        });
        strShould = 'setSn(str) should ';
        strExpected = 'set .Sn ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn('2.0');
            assert.equal(generator.Sn, 2.0);
        });
        strShould = 'typeOf Sn should be ';
        strExpected = '"number" when set as string with dot';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn('2.0');
            assert.equal(typeof generator.Sn, typeof 1);
        });
        strShould = 'typeOf Sn should be ';
        strExpected = '"number" when set as string with comma';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn('2,0');
            assert.equal(typeof generator.Sn, typeof 1);
        });
        strShould = 'setSn("-2.0") should ';
        strExpected = 'throw error';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                generator.setSn('-2,0');
            }, Error);
        });

        // Assert PF
        strShould = 'setPf(num) should ';
        strExpected = 'set .pf ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf(0.8);
            assert.equal(generator.pf, 0.8);
        });
        strShould = 'typeOf pf should be ';
        strExpected = '"number" when set as number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf(0.8);
            assert.equal(typeof generator.pf, typeof 1);
        });
        strShould = 'setPf(str) should ';
        strExpected = 'set .pf ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf('0.8');
            assert.equal(generator.pf, 0.8);
        });
        strShould = 'typeOf pf should be ';
        strExpected = '"number" when set as string with dot';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf('0.8');
            assert.equal(typeof generator.pf, typeof 1);
        });
        strShould = 'typeOf pf should be ';
        strExpected = '"number" when set as string with comma';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf('0,8');
            assert.equal(typeof generator.pf, typeof 1);
        });
        strShould = 'setPf("-1.01") should ';
        strExpected = 'throw error';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                generator.setPf('-1.01');
            }, Error);
        });
        strShould = 'setPf("1.01") should ';
        strExpected = 'throw error';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                generator.setPf('1.01');
            }, Error);
        });
        strShould = 'setPf(1.0) should ';
        strExpected = 'be allowed';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf(1.0);
            assert.equal(generator.pf, 1.0);
        });
        strShould = 'setPf(-1.0) should ';
        strExpected = 'be allowed';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setPf(-1.0);
            assert.equal(generator.pf, -1.0);
        });
        strShould = 'updatePV() should ';
        strExpected = 'set P to Sn * pf';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            generator.setSn(1.0).setPf(0.8);
            assert.equal(generator.P, 0.8);
        });
    });
});

strTestType = 'Load Unit test'; // Load extends NetElement
// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Methods */
    strDescr = 'Method ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const load = linkjs.LoadFactory('Load');

        const setUpOK = load.P === 0 && load.Q === 0;

        if (!setUpOK) {
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        strShould = 'setP(num) should ';
        strExpected = 'set .P ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP(2).setQ(1);
            assert.equal(load.P, 2);
        });
        strShould = 'typeOf P is ';
        strExpected = '"number" when set as number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP(2).setQ(1);
            assert.equal(typeof load.P, typeof 1);
        });
        strShould = 'setP(str) should ';
        strExpected = 'set .P ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2').setQ(1);
            assert.equal(load.P, 2);
        });
        strShould = 'typeOf P is ';
        strExpected = '"number" when set as string with dot';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2.0').setQ(1);
            assert.equal(typeof load.P, typeof 1);
        });
        strShould = 'typeOf P is ';
        strExpected = '"number" when set as string with comma';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2,0').setQ(1);
            assert.equal(typeof load.P, typeof 1);
        });

        strShould = 'setQ(num) should ';
        strExpected = 'set .Q ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP(2).setQ(1);
            assert.equal(load.Q, 1);
        });
        strShould = 'typeOf Q is ';
        strExpected = '"number" when set as number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP(2).setQ(1);
            assert.equal(typeof load.Q, typeof 1);
        });
        strShould = 'setQ(str) should ';
        strExpected = 'set .Q ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2').setQ('1');
            assert.equal(load.Q, 1);
        });
        strShould = 'typeOf Q is ';
        strExpected = '"number" when set as string with dot';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2').setQ('1.0');
            assert.equal(typeof load.Q, typeof 1);
        });
        strShould = 'typeOf Q is ';
        strExpected = '"number" when set as string with comma';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            load.setP('2').setQ('1,0');
            assert.equal(typeof load.Q, typeof 1);
        });
    });
});
