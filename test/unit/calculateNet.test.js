/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

// Import Class under test
const calcjs = require('../../src/engine/calculateNet');
const netjs = require('../../src/sdk/network');
const linkjs = require('../../src/sdk/link');
const node = require('../../src/sdk/node');
const mathjs = require('mathjs');

// Config template string
const strTestType = 'calculateNet Unit test';
let strDescr = '';
let strShould = '';
let strExpected = 'expected';
const strWhen = '';
// const strFound = ', got result ';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Methods */
    // network.sBase = 100;
    const vBase = 10;

    strDescr = 'Method calculateNet() ';

    strShould = 'called with options.calcLoadFlow + calcIk[...] and U as typeOf number should ';
    strExpected = 'convert U to typeOf Complex';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;
        network.sBase = 100;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.nodes[0].lf.U), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow + calcIsc[...] and U as typeOf Complex should ';
    strExpected = 'convert U to typeOf Complex';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));
        bus1.U = mathjs.complex(1.05, 0);
        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.nodes[0].lf.U), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow and U as typeOf string should ';
    strExpected = 'throw Error';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));
        bus1.U = '1.05+0i';

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = false;
        network.options.calcIk2 = false;
        network.options.calcIk2EE = false;
        network.options.calcIk3 = false;

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                calcjs.calculateNet(network);
            }, Error);
        });
    });

    strShould = 'called with options.calcIk3 and U as typeOf string should ';
    strExpected = 'throw Error';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));
        bus1.U = '1.05+0i';

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = false;
        network.options.calcIk2 = false;
        network.options.calcIk2EE = false;
        network.options.calcIk3 = true;

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                calcjs.calculateNet(network);
            }, Error);
        });
    });

    strShould = 'called with options.calcIk3 and U as typeOf string should ';
    strExpected = 'throw Error';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));
        bus1.U = '1.05+0i';

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = true;
        network.options.calcIk2 = false;
        network.options.calcIk2EE = false;
        network.options.calcIk3 = false;

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                calcjs.calculateNet(network);
            }, Error);
        });
    });

    strShould = 'called with options.calcIk2EE and U as typeOf string should ';
    strExpected = 'throw Error';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));
        bus1.U = '1.05+0i';

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = false;
        network.options.calcIk2 = false;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = false;

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                calcjs.calculateNet(network);
            }, Error);
        });
    });

    strShould = 'called with options.calcLoadFlow and links.posSeq as string should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm('0.1+0.2j');

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeq[1].zm), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow and links.posSeq as number should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(0.1);

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeq[1].zm), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow and links.posSeq as Complex should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2));

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeq[1].zm), 'Complex');
    });

    /** * */
    strShould = 'called with options.calcLoadFlow and links.shunt as string should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm('0.1+0.2j').setShunt('0.0+0.02j');

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeqForYbus[1].shunt), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow and links.shunt as number should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(0.1).setShunt(0.02);

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeqForYbus[1].shunt), 'Complex');
    });

    strShould = 'called with options.calcLoadFlow and links.shunt as Complex should ';
    strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZm(mathjs.complex(0.1, 0.2)).setShunt(mathjs.complex(0, 0.02));

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = true;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.posSeqForYbus[1].shunt), 'Complex');
    });
    /** * */
    strShould = 'called with options.shortCircuit and links.zeroSeq as string should ';
    // strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZ0('0.1+0.2j');

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.zeroSeq[1].z0), 'Complex');
    });

    strShould = 'called with options.shortCircuit and links.zeroSeq as number should ';
    // strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZ0(0.1);

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.zeroSeq[1].z0), 'Complex');
    });

    strShould = 'called with options.shortCircuit and links.zeroSeq as Complex should ';
    // strExpected = 'convert links to typeOf Complex ';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
        const network = netjs.Network('Test-network');
        const bus1 = node.NodeFactory('Test-node');
        bus1.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const bus2 = node.NodeFactory('Test-node');
        bus2.setNode(1).setUFactor(1.05).setBase(network.sBase, vBase);
        const source = linkjs.LinkFactory('Source');
        source.setFrom(0).setTo(1).setBase(network.sBase, vBase);
        const link = linkjs.LinkFactory('L12');
        link.setFrom(1).setTo(2).setBase(network.sBase, vBase).setZ0(mathjs.complex(0.1, 0.2));

        network.addNode(bus1);
        network.addNode(bus2);
        network.addLinkImpedance(source);
        network.addLinkImpedance(link);
        network.options.calcLoadflow = false;
        network.options.calcIk1 = true;
        network.options.calcIk2 = true;
        network.options.calcIk2EE = true;
        network.options.calcIk3 = true;

        const result = calcjs.calculateNet(network);

        assert.strictEqual(mathjs.typeOf(result.links.zeroSeq[1].z0), 'Complex');
    });
});
