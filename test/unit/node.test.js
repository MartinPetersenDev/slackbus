/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');

// Import Class under test
const nodejs = require('../../src/sdk/node');

// Config template string
const strTestType = 'Node Unit test';
let strDescr = '';
let strShould = '';
let strExpected = '';
const strWhen = '';
let strFound = ', got result ';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Class */
    strDescr = 'Class ';
    strShould = 'should contain';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const node = nodejs.NodeFactory('Net');
        node.setBase(10, 20).setNode(1).setUFactor(1.1);

        // Assert node
        strExpected = ' node field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.node}`, function () {
            assert.strictEqual(typeof node.node, typeof 1);
        });

        // Assert i_sc
        strExpected = ' sc object typeof object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.sc}`, function () {
            assert.strictEqual(typeof node.sc, 'object');
        });

        // Assert i_sc.i3
        strExpected = ' sc.ik3 object typeof object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.sc.ik3}`, function () {
            assert.strictEqual(typeof node.sc.ik3, typeof 1);
        });

        // Assert i_sc.i2
        strExpected = ' sc.ik2 object typeof object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.sc.ik2}`, function () {
            assert.strictEqual(typeof node.sc.ik2, typeof 1);
        });

        // Assert i_sc.i2EE
        strExpected = ' sc.ik2EE object typeof object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.sc.ik2EE}`, function () {
            assert.strictEqual(typeof node.sc.ik2EE, typeof 1);
        });

        // Assert i_sc.i1
        strExpected = ' sc.ik1 object typeof object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.sc.ik1}`, function () {
            assert.strictEqual(typeof node.sc.ik1, typeof 1);
        });

        // Assert type
        strExpected = ' type field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.type}`, function () {
            assert.strictEqual(typeof node.lf.type, typeof 1);
        });

        // Assert Psch
        strExpected = ' Psch field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Psch}`, function () {
            assert.strictEqual(typeof node.lf.Psch, typeof 1);
        });

        // Assert Qsch
        strExpected = ' Qsch field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Qsch}`, function () {
            assert.strictEqual(typeof node.lf.Qsch, typeof 1);
        });

        // Assert Pgen
        strExpected = ' Pgen field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Pgen}`, function () {
            assert.strictEqual(typeof node.lf.Pgen, typeof 1);
        });

        // Assert Qgen
        strExpected = ' Pgen field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Qgen}`, function () {
            assert.strictEqual(typeof node.lf.Qgen, typeof 1);
        });

        // Assert Pload
        strExpected = ' Pload field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Pload}`, function () {
            assert.strictEqual(typeof node.lf.Pload, typeof 1);
        });

        // Assert Qload
        strExpected = ' Qload field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Qload}`, function () {
            assert.strictEqual(typeof node.lf.Qload, typeof 1);
        });

        // Assert Umismatch
        strExpected = ' Umismatch field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Umismatch}`, function () {
            assert.strictEqual(typeof node.lf.Umismatch, typeof 1);
        });

        // Assert Pmismatch
        strExpected = ' Pmismatch field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Pmismatch}`, function () {
            assert.strictEqual(typeof node.lf.Pmismatch, typeof 1);
        });

        // Assert Qmismatch
        strExpected = ' Qmismatch field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.Qmismatch}`, function () {
            assert.strictEqual(typeof node.lf.Qmismatch, typeof 1);
        });

        // Assert U
        strExpected = ' U field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.U}`, function () {
            assert.strictEqual(typeof node.lf.U, typeof 1);
        });

        // Assert U value
        strExpected = ' U=1.0';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${node.lf.U}`, function () {
            assert.strictEqual(node.lf.U, 1.0);
        });

        // Assert Un
        strExpected = ' Un field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.Un}`, function () {
            assert.strictEqual(typeof node.Un, typeof 1);
        });

        // Assert Un value
        strExpected = ' Un=U';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${node.Un}`, function () {
            assert.strictEqual(node.Un, node.lf.U);
        });

        // Assert uFactor
        strExpected = ' uFactor field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.uFactor}`, function () {
            assert.strictEqual(typeof node.lf.uFactor, typeof 1);
        });

        // Assert QlimitMax
        strExpected = ' QlimitMax field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.QlimitMax}`, function () {
            assert.strictEqual(typeof node.lf.QlimitMax, typeof 1);
        });

        // Assert QlimitMin
        strExpected = ' QlimitMin field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.lf.QlimitMin}`, function () {
            assert.strictEqual(typeof node.lf.QlimitMin, typeof 1);
        });

        // Assert iBase
        strExpected = ' iBase field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.iBase}`, function () {
            assert.strictEqual(typeof node.iBase, typeof 1);
        });

        // Assert iBase value
        strExpected = ' iBase = 0.28867513'; // 10 / (sqrt(3)*20) = 0.28867513
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${node.iBase.toFixed(8)}`, function () {
            assert.equal(node.iBase.toFixed(8), 0.28867513);
        });

        // Assert zBase
        strExpected = ' zBase field typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof node.zBase}`, function () {
            assert.strictEqual(typeof node.zBase, typeof 1);
        });

        // Assert zBase value
        strExpected = ' zBase = 40'; // 20^2 / 10 = 40
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${node.zBase.toFixed(8)}`, function () {
            assert.equal(node.zBase.toFixed(8), 40);
        });
    });

    /** Methods */
    strDescr = 'Method ';

    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert

        const node = new nodejs.NodeFactory('Node');
        node.setNode(1).setBase(10, 20).setUFactor(2);

        const setUpOK = true;

        if (!setUpOK) {
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        strFound = ', found = ';

        strShould = 'default type should be ';
        strExpected = '2 (PQ)';

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}${node.lf.type}`, function () {
            assert.strictEqual(node.lf.type, nodejs.typeEnum.PQ);
        });

        strShould = 'setNodeType(1) should set type to ';
        strExpected = '1 (Slackbus)';
        // Call method
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(1);
            assert.strictEqual(node.lf.type, 1);
        });

        strShould = 'setNodeType(1)should set U = '; // 1 * 2 = 2
        strExpected = 'Un * uFactor';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.strictEqual(node.lf.U, 2);
        });

        strShould = 'setNodeType(2) should set U = '; // = 1
        strExpected = 'Un';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(2);
            assert.strictEqual(node.lf.U, 1);
        });

        strShould = 'setNodeType(3) should set U = '; // 1 * 2 = 2
        strExpected = 'Un * uFactor';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(3);
            assert.strictEqual(node.lf.U, 2);
        });

        strShould = 'getNodeTypeName() for type 1, should return string = ';
        strExpected = 'slackbus';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(1);
            assert.strictEqual(node.getNodeTypeName(), 'slackbus');
        });

        strShould = 'getNodeTypeName() for type 2, should return string = ';
        strExpected = 'PQ';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(2);
            assert.strictEqual(node.getNodeTypeName(), 'PQ');
        });

        strShould = 'getNodeTypeName() for type 3, should return string = ';
        strExpected = 'PV';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setNodeType(3);
            assert.strictEqual(node.getNodeTypeName(), 'PV');
        });

        strShould = 'setNodeType(4) should throw error';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                node.setNodeType(4);
            }, Error);
        });

        strShould = 'getNodeTypeName() for type 4, should throw error';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                node.lf.type = 4;
                node.getNodeType();
            }, Error);
        });

        const linkjs = require('../../src/sdk/link');
        const load = new linkjs.LoadFactory('Load');
        load.setP(10).setQ(20);

        strShould = 'addLoadPower(load) should set ';
        strExpected = 'node.lf.Pload = 10';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.addLoadPower(load);
            assert.strictEqual(node.lf.Pload, 10);
        });
        strShould = 'addLoadPower(load) should set ';
        strExpected = 'node.lf.Qload = 20';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            // bus.addLoadPower(load);
            assert.strictEqual(node.lf.Qload, 20);
        });

        strShould = 'addLoadPower(load) with negative P, should throw error';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                load.P = -1;
                node.addLoadPower(load);
            }, Error);
        });

        const generator = linkjs.GeneratorFactory('Generator');
        generator.setFrom(0).setTo(1).setSn(10).setPf(0.8);

        strShould = 'addGeneratorPower(generator) Sn=10 pf=0.8 should set ';
        strExpected = 'node.lf.Pgen = 8';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.addGeneratorPower(generator);
            assert.strictEqual(node.lf.Pgen, 8);
        });

        strShould = 'addGeneratorPower(generator) with negative P, should throw error';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                generator.P = -1;
                node.addGeneratorPower(generator);
            }, Error);
        });

        const oldId = node.id;

        strShould = 'setId(42), should set ID to 42';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            node.setId(42);
            assert.equal(node.id, 42);
        });

        strShould = 'id=42 should not equal old ID';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.notEqual(node.id, oldId);
        });

        strShould = 'setCFactor(1.1, 1.0) should set .cmax and .cmin';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            const _node = nodejs.NodeFactory('test node');
            _node.setCFactor(1.1, 1.0);
            assert.strictEqual(_node.sc.cMax, 1.1);
            assert.strictEqual(_node.sc.cMin, 1.0);
        });

        strShould = 'setCFactor(1.05, 0.95) should set .cmax and .cmin';
        strExpected = '';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            const _node = nodejs.NodeFactory('test node');
            _node.setCFactor(1.05, 0.95);
            assert.strictEqual(_node.sc.cMax, 1.05);
            assert.strictEqual(_node.sc.cMin, 0.95);
        });
    });
});
