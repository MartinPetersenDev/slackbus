/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');
const utils = require('../../src/sdk/utils');

// Import Class under test
const networkjs = require('../../src/sdk/network');

// Config template string
const strTestType = 'Network Unit test';
let strDescr = '';
let strShould = '';
let strExpected;
const strWhen = '';
let strFound = ', got result ';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    /** Network class */
    strDescr = 'Class ';
    strShould = 'should contain ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        const network = networkjs.Network('Test-network');

        // Assert Z_m array
        strExpected = 'posSeq array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.links.posSeq)}`, function () {
            assert.strictEqual(Array.isArray(network.links.posSeq), true);
        });

        // Assert Z_0 array
        strExpected = 'zeroSeq array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.links.zeroSeq)}`, function () {
            assert.strictEqual(Array.isArray(network.links.zeroSeq), true);
        });

        // Assert z_mYbus array
        strExpected = 'posSeqForYbus array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.links.posSeqForYbus)}`, function () {
            assert.strictEqual(Array.isArray(network.links.posSeqForYbus), true);
        });

        // Assert busbars array
        strExpected = 'busbars array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.nodes)}`, function () {
            assert.strictEqual(Array.isArray(network.nodes), true);
        });

        // Assert messages array
        strExpected = 'messages array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.messages)}`, function () {
            assert.strictEqual(Array.isArray(network.messages), true);
        });

        // Assert options object
        strExpected = 'options object';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options}`, function () {
            assert.strictEqual(typeof network.options, typeof { a: 1 });
        });

        // Assert options object
        strExpected = 'options object, and should not be an array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${Array.isArray(network.options)}`, function () {
            assert.notEqual(Array.isArray(network.options), Array.isArray([1]));
        });

        // Assert options, calcIsc1
        strExpected = 'options object with calcIsc1 typeof boolean';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.calcIk1}`, function () {
            assert.strictEqual(typeof network.options.calcIk1, typeof false);
        });

        // Assert options, calcIsc2
        strExpected = 'options object with calcIsc2 typeof boolean';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.calcIk2}`, function () {
            assert.strictEqual(typeof network.options.calcIk2, typeof false);
        });

        // Assert options, calcIsc2EE
        strExpected = 'options object with calcIsc2EE typeof boolean';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.calcIk2EE}`, function () {
            assert.strictEqual(typeof network.options.calcIk2EE, typeof false);
        });

        // Assert options, calcIsc3
        strExpected = 'options object with calcIsc3 typeof boolean';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.calcIk3}`, function () {
            assert.strictEqual(typeof network.options.calcIk3, typeof false);
        });

        // Assert options, calcLoadflow
        strExpected = 'options object with calcLoadflow typeof boolean';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.calcLoadflow}`, function () {
            assert.strictEqual(typeof network.options.calcLoadflow, typeof false);
        });

        // Assert options, cMax
        strExpected = 'options object with cMax typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.cMax}`, function () {
            assert.strictEqual(typeof network.options.cMax, typeof 1);
        });

        // Assert options, cMin
        strExpected = 'options object with cMin typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.cMin}`, function () {
            assert.strictEqual(typeof network.options.cMin, typeof 1);
        });

        // Assert options, maxLoadflowIterations
        strExpected = 'options object with maxLoadflowIterations typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.maxLoadflowIterations}`, function () {
            assert.strictEqual(typeof network.options.maxLoadflowIterations, typeof 1);
        });

        // Assert options, gsAccelerationFactor
        strExpected = 'options object with gsAccelerationFactor typeof number';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.gsAccelerationFactor}`, function () {
            assert.strictEqual(typeof network.options.gsAccelerationFactor, typeof 1);
        });

        // Assert options, gsAccelerationFactor
        strExpected = 'options object with gsAccelerationFactor default = 1.5';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.options.gsAccelerationFactor}`, function () {
            assert.strictEqual(network.options.gsAccelerationFactor, 1.5);
        });

        // Assert options, convergenceLimit
        strExpected = 'options object with convergenceLimit typeof number, default 1e-5';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.convergenceLimit}`, function () {
            assert.strictEqual(typeof network.options.convergenceLimit, typeof 1);
            assert.strict(network.options.convergenceLimit, 1e-5);
        });

        // Assert options, convergenceLimit default value
        strExpected = 'options object with convergenceLimit default = 1e-5';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.options.convergenceLimit}`, function () {
            assert.strictEqual(network.options.convergenceLimit, 1e-5);
        });

        // Assert options, respectQLimits
        strExpected = 'options object with respectQLimits typeof boolean';

        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${typeof network.options.respectQLimits}`, function () {
            assert.strictEqual(typeof network.options.respectQLimits, typeof false);
        });
    });

    /** Network methods */
    strDescr = 'Method ';
    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert method clear
        strShould = 'clear() should ';

        const network = networkjs.Network('Test-network');
        network.links.posSeq.push(1);
        network.links.zeroSeq.push(1);
        network.links.posSeqForYbus.push(1);
        network.nodes.push(1);
        network.messages.push(1);

        const setUpOK =
            network.links.posSeq.length === 1 &&
            network.links.zeroSeq.length === 1 &&
            network.links.posSeqForYbus.length === 1 &&
            network.nodes.length === 1 &&
            network.messages.length === 1;

        if (!setUpOK) {
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        // Call method
        network.clear();

        strFound = ', got length ';
        strExpected = 'clear links.posSeq array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeq.length}`, function () {
            assert.equal(network.links.posSeq.length, 0);
        });
        strExpected = 'clear links.zeroSeq array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.zeroSeq.length}`, function () {
            assert.strictEqual(network.links.zeroSeq.length, 0);
        });
        strExpected = 'clear links.posSeqForYbus array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeqForYbus.length}`, function () {
            assert.strictEqual(network.links.posSeqForYbus.length, 0);
        });
        strExpected = 'clear network.nodes array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.nodes.length}`, function () {
            assert.strictEqual(network.nodes.length, 0);
        });
        strExpected = 'clear network.messages array';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.messages.length}`, function () {
            assert.strictEqual(network.messages.length, 0);
        });
    });

    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert method addNode
        strShould = 'addNode() should ';
        strFound = ', got result ';

        const network = networkjs.Network('Test-network');

        const node = require('../../src/sdk/node');

        const bus1 = node.NodeFactory('Bus1');
        bus1.setBase(1, 1).setNode(1).setUFactor(1);

        const bus2 = node.NodeFactory('Bus2');
        bus2.setBase(1, 1).setNode(2).setUFactor(1);

        const bus3 = node.NodeFactory('Bus3');
        bus3.setBase(1, 1).setNode(3).setUFactor(1);

        // Add in unsorted order
        network.addNode(bus3);
        network.addNode(bus1);
        network.addNode(bus2);

        const setUpOK = network.nodes.length === 3;

        if (!setUpOK) {
            console.log(network.nodes);
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        // Call method

        strExpected = 'add nodes';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.nodes.length === 3}`, function () {
            assert.strictEqual(network.nodes.length, 3);
        });

        strExpected = 'sort bus1 first';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.nodes[0].node}`, function () {
            assert.strictEqual(network.nodes[0].node, 1);
        });
        strExpected = 'sort bus2 second';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.nodes[1].node}`, function () {
            assert.strictEqual(network.nodes[1].node, 2);
        });
        strExpected = 'sort bus3 third';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.nodes[2].node}`, function () {
            assert.strictEqual(network.nodes[2].node, 3);
        });
    });

    // eslint-disable-next-line no-undef
    describe(`${strDescr.trim()}`, function () {
        // Assert method addLinkImpedance
        strShould = 'addLinkImpedance() should ';

        const network = networkjs.Network('Test-network');
        const linkjs = require('../../src/sdk/link');
        const mathjs = require('mathjs');
        const node = require('../../src/sdk/node');

        // Mock busbars ... not relevant for test, but addLinkImpedance will fail,
        // if index of link.to and link.from is not found in connectivity matrix
        const bus1 = node.NodeFactory('Bus1');
        bus1.setNode(1);
        const bus2 = node.NodeFactory('Bus2');
        bus2.setNode(2);

        const lineZm12 = linkjs.LinkFactory('zm');
        lineZm12.setFrom(bus1.node).setTo(bus2.node).setBase(1, 1).setZm(mathjs.complex(1, 2)).setZ0(mathjs.complex(1, 2));

        const lineZm21 = linkjs.LinkFactory('zm');
        lineZm21.setFrom(bus2.node).setTo(bus1.node).setBase(1, 1).setZm(mathjs.complex(1, 2)).setZ0(mathjs.complex(1, 2));

        const lineRefm01 = linkjs.LinkFactory('ref');
        lineRefm01.setFrom(0).setTo(bus1.node).setBase(1, 1).setZm(mathjs.complex(1, 2)).setZ0(mathjs.complex(1, 2));

        const lineRefm10 = linkjs.LinkFactory('ref');
        lineRefm10.setFrom(bus1.node).setTo(0).setBase(1, 1).setZm(mathjs.complex(1, 2)).setZ0(mathjs.complex(1, 2));

        // Add elements to network in sorted order
        network.addNode(bus1);
        network.addNode(bus2);

        // Add in unsorted order
        network.addLinkImpedance(lineZm21);
        // network.addLinkImpedance(lineZ021);
        network.addLinkImpedance(lineZm12);
        // network.addLinkImpedance(lineZ012);
        // network.addLinkImpedance(lineRef010);
        network.addLinkImpedance(lineRefm10);
        // network.addLinkImpedance(lineRef001);
        network.addLinkImpedance(lineRefm01);

        const setUpOK = network.nodes.length === 2 && network.links.posSeq.length === 4 && network.links.zeroSeq.length === 4;

        if (!setUpOK) {
            throw new Error(`The test setup for "${strShould}" has somehow failed ... aborting test.`);
        }

        // Call method
        strExpected = 'add all links to z_m';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeq.length}`, function () {
            assert.strictEqual(network.links.posSeq.length, 4);
        });

        strExpected = 'add all links to z_0';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.zeroSeq.length}`, function () {
            assert.strictEqual(network.links.zeroSeq.length, 4);
        });

        strExpected = 'sort posSeq links with ref (from=0) first';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeq[0].from}`, function () {
            assert.strictEqual(network.links.posSeq[0].from, 0);
        });

        strExpected = 'sort zeroSeq links with ref (from=0) first';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.zeroSeq[0].from}`, function () {
            assert.strictEqual(network.links.zeroSeq[0].from, 0);
        });

        strExpected = 'sort posSeqForYbus links with ref (from=0) first';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeqForYbus[0].from}`, function () {
            assert.strictEqual(network.links.posSeqForYbus[0].from, 0);
        });

        strExpected = 'add impedance.re=1e9 for z_mYbus ref nodes (from=0)';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeqForYbus[0].zm.re}`, function () {
            assert.strictEqual(network.links.posSeqForYbus[0].zm.re, 1e9);
        });

        strExpected = 'add impedance.im=1e9 for z_mYbus ref nodes (from=0)';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeqForYbus[0].zm.im}`, function () {
            assert.strictEqual(network.links.posSeqForYbus[0].zm.im, 1e9);
        });

        strExpected = 'not have impedance.re=1e9 for z_m ref nodes (from=0)';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeq[0].zm.re}`, function () {
            assert.strictEqual(network.links.posSeq[0].zm.re, 1);
        });
        strExpected = 'not have impedance.im=1e9 for z_m ref nodes (from=0)';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${network.links.posSeq[0].zm.im}`, function () {
            assert.strictEqual(network.links.posSeq[0].zm.im, 2);
        });

        strExpected = 'update connectivity matrix for parallel connections with same impedance';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}`, function () {
            const lineZm12B = linkjs.LinkFactory('zm');
            lineZm12B.setFrom(bus1.node).setTo(bus2.node).setBase(1, 1).setZm(mathjs.complex(1, 2));
            network.addLinkImpedance(lineZm12B);
            assert.equal(network.matrix.connectivity.subset(mathjs.index(bus1.node - 1, bus2.node - 1)), 3);
        });

        strExpected = 'throw error if adding parallel connections with different impedance';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}`, function () {
            const lineZm12C = linkjs.LinkFactory('zm');
            lineZm12C.setFrom(bus1.node).setTo(bus2.node).setBase(1, 1).setZm(mathjs.complex(1.1, 2.2));

            assert.throws(function () {
                network.addLinkImpedance(lineZm12C);
            }, Error);
        });
    });

    // eslint-disable-next-line no-undef
    describe(`(ID 1.8, 1.9) ${strDescr.trim()}`, function () {
        // Assert method addNode
        strShould = 'convertToPerUnit() should ';
        strFound = ', got result ';

        // Call method

        strExpected = 'convert "2+4i" at voltage=2 and sBase=10 to 5+10i';
        // eslint-disable-next-line no-undef
        it(`${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${utils.convertToPerUnit(2, 10, '2+4i')}`, function () {
            assert.strictEqual(utils.convertToPerUnit(2, 10, '2+4i'), '5 + 10i');
        });

        strExpected = 'convert "2 +4 i" with spaces, at voltage=2 and sBase=10 to 5+10i';
        // eslint-disable-next-line no-undef
        it(`(ID 1.8) ${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${utils.convertToPerUnit(2, 10, '2 +4 i')}`, function () {
            assert.strictEqual(utils.convertToPerUnit(2, 10, '2 +4 i'), '5 + 10i');
        });

        strExpected = 'convert "2+4j" at voltage=2 and sBase=10 to 5+10i';
        // eslint-disable-next-line no-undef
        it(`(ID 1.8) ${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${utils.convertToPerUnit(2, 10, '2+4j')}`, function () {
            assert.strictEqual(utils.convertToPerUnit(2, 10, '2+4j'), '5 + 10i');
        });

        strExpected = 'convert "2,1+4,2j" at voltage=2 and sBase=10 to 5.25+10.5i';
        // eslint-disable-next-line no-undef
        it(`(ID 1.8) ${strDescr}${strShould}${strExpected}${strWhen}${strFound}= ${utils.convertToPerUnit(2, 10, '2,1+4,2j')}`, function () {
            assert.strictEqual(utils.convertToPerUnit(2, 10, '2,1+4,2j'), '5.25 + 10.5i');
        });

        strExpected = 'throw error for invalid string';
        // eslint-disable-next-line no-undef
        it(`(ID 1.9) ${strDescr}${strShould}${strExpected}${strWhen}`, function () {
            assert.throws(function () {
                utils.convertToPerUnit(2, 10, 'invalid-string');
            }, Error);
        });
    });
});
