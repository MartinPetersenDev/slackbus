/**
 * Copyright (c) 2020 Martin Petersen / @MartinPetersenDev
 */

var assert = require('assert');
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

// Config template string
const strTestType = 'Dummy';
let strDescr = '';
let strShould = '';

// eslint-disable-next-line no-undef
describe(`${strTestType.trim()}`, function () {
    strDescr = 'Test should';
    strShould = ' pass';
    // eslint-disable-next-line no-undef
    it(`${strDescr}${strShould}`, function () {
        assert.strictEqual(1, 1);
    });
});
